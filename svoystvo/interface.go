package svoystvo

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindSvoystvoByName(name string) (*entity.Svoystvo, error)
	FindSvoystvoByNameForSite(name string) (*entity.Svoystvo, error)
	FindSvoystvoByNameTranslitForSite(name string) (*entity.Svoystvo, error)
	FindSvoystvoBySinonim(name string) (*entity.Svoystvo, error)

	FindByQuery(query bson.M) ([]entity.Svoystvo, error)
}

// Writer bookmark writer
type Writer interface {
	UpdateSvoystvo(svoystvo *entity.Svoystvo) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

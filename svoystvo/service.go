package svoystvo

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindSvoystvoByName(name string) (*entity.Svoystvo, error) {
	return s.repo.FindSvoystvoByName(name)
}

func (s *Service) FindSvoystvoByNameForSite(name string) (*entity.Svoystvo, error) {
	return s.repo.FindSvoystvoByNameForSite(name)
}

func (s *Service) FindSvoystvoByNameTranslitForSite(name string) (*entity.Svoystvo, error) {
	return s.repo.FindSvoystvoByNameTranslitForSite(name)
}

func (s *Service) UpdateSvoystvo(svoystvo *entity.Svoystvo) error {
	return s.repo.UpdateSvoystvo(svoystvo)
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.Svoystvo, error) {
	return s.repo.FindByQuery(query)
}

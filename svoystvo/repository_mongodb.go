package svoystvo

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindSvoystvoByName(name string) (*entity.Svoystvo, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}

	result := entity.Svoystvo{}

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"name": name}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindSvoystvoByNameForSite(name string) (*entity.Svoystvo, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}

	result := entity.Svoystvo{}

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"nameForSite": name}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindSvoystvoByNameTranslitForSite(name string) (*entity.Svoystvo, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}

	result := entity.Svoystvo{}

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"nameTranslitForSite": name}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindSvoystvoBySinonim(name string) (*entity.Svoystvo, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}

	result := entity.Svoystvo{}

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"sinonims": primitive.Regex{name, "i"}}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

// Обновляет свойство или добавляет новое с уникальными значениями из коллекции
func (c *MongoConnection) UpdateSvoystvo(svoystvo *entity.Svoystvo) (err error) {
	if svoystvo.Name == "" {
		return entity.ErrInputValueNil
	}

	query := bson.M{"name": svoystvo.Name}
	update := bson.M{"$set": svoystvo}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return nil
	default:
		log.Println("ERROR Update " + err.Error())
		return err
	}
}

func (c *MongoConnection) FindByQuery(query bson.M) ([]entity.Svoystvo, error) {
	resultArray := []entity.Svoystvo{}
	// ищем
	cursor, err := c.collection.Find(context.TODO(), query)

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

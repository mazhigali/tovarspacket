package utils

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
)

// Reader interface
type Reader interface {
	CloseConnection()
	GetCounter() (entity.Counter, error)
	GetCounterByName(name string) (entity.Counter, error)
}

// Writer bookmark writer
type Writer interface {
	UpdateCounter(counter *entity.Counter) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

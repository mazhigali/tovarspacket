package utils

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}
func (s *Service) GetCounter() (entity.Counter, error) {
	return s.repo.GetCounter()
}

func (s *Service) GetCounterByName(name string) (entity.Counter, error) {
	return s.repo.GetCounterByName(name)
}

func (s *Service) UpdateCounter(counter *entity.Counter) error {
	return s.repo.UpdateCounter(counter)
}

module gitlab.com/mazhigali/tovarspacket

go 1.24

require (
	gitlab.com/mazhigali/apiyamarket v0.0.0-20211206062903-2d614e8d6786
	gitlab.com/mazhigali/comparetextpacket v0.0.0-20240212131256-9378c9ed019c
	gitlab.com/mazhigali/usefullPacket v0.0.0-20240429041931-8ae3d666c7cb
	go.mongodb.org/mongo-driver v1.15.0
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/mxmCherry/translit v1.0.0 // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/nyaruka/phonenumbers v1.3.4 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.31.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

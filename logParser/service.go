package logParser

import (
	"gitlab.com/mazhigali/tovarspacket/entity"

	"go.mongodb.org/mongo-driver/bson"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByUrl(url string) (*entity.LogParser, error) {
	return s.repo.FindByUrl(url)
}

func (s *Service) FindAll() ([]entity.LogParser, error) {
	return s.repo.FindAll()
}

func (s *Service) FindAllBySite(siteId string) ([]entity.LogParser, error) {
	return s.repo.FindAllBySite(siteId)
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.LogParser, error) {
	return s.repo.FindByQuery(query)
}

func (s *Service) UpdateLog(url, siteId string) error {
	return s.repo.UpdateLog(url, siteId)
}

func (s *Service) Remove(url string) error {
	return s.repo.Remove(url)
}

func (s *Service) RemoveAllBySiteID(siteId string) error {
	return s.repo.RemoveAllBySiteID(siteId)
}

func (s *Service) RemoveAll() error {
	return s.repo.RemoveAll()
}

package logParser

import (
	"gitlab.com/mazhigali/tovarspacket/entity"

	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByUrl(url string) (*entity.LogParser, error)
	FindAll() ([]entity.LogParser, error)
	FindAllBySite(siteId string) ([]entity.LogParser, error)
	FindByQuery(query bson.M) ([]entity.LogParser, error)
}

// Writer tovar writer
type Writer interface {
	UpdateLog(url, siteId string) error
	Remove(url string) error
	RemoveAll() error
	RemoveAllBySiteID(siteId string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

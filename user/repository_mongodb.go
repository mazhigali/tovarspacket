package user

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindUserById(id string) (entity.User, error) {
	// создаем новый документ
	result := entity.User{}

	id = strings.TrimSpace(id)
	if id == "" {
		return result, entity.ErrInputValueNil
	}

	idObject, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return result, err
	}

	filter := bson.M{"_id": idObject}

	err = c.collection.FindOne(context.TODO(), filter).Decode(&result)

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return result, entity.ErrNotFound
	default:
		return result, err
	}
}

func (c *MongoConnection) FindUserByEmail(email string) (entity.User, error) {
	// создаем новый документ
	result := entity.User{}

	email = strings.TrimSpace(email)
	if email == "" {
		return result, entity.ErrInputValueNil
	}

	//searchRegex := "^" + regexp.QuoteMeta(userName) + "$"
	// ищем
	//err := c.collection.FindOne(context.TODO(), bson.M{"userName": primitive.Regex{searchRegex, "i"}}).Decode(&result)
	filter := bson.M{"email": email}

	err := c.collection.FindOne(context.TODO(), filter).Decode(&result)

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return result, entity.ErrNotFound
	default:
		return result, err
	}
}

func (c *MongoConnection) FindUserByActivationCode(activationCode string) (entity.User, error) {
	result := entity.User{}

	activationCode = strings.TrimSpace(activationCode)
	if activationCode == "" {
		return result, entity.ErrInputValueNil
	}

	filter := bson.M{"activationCode": activationCode}

	err := c.collection.FindOne(context.TODO(), filter).Decode(&result)

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return result, entity.ErrNotFound
	default:
		return result, err
	}
}

func (c *MongoConnection) FindUsersByQuery(query bson.M) ([]entity.User, error) {
	// создаем новый документ
	results := []entity.User{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &results); err != nil {
		return nil, err
	}
	return results, nil
}

func (c *MongoConnection) UpdateUser(user *entity.User) error {
	if user.Email == "" {
		return errors.New("User Email is nil")
	}

	user.UpdatedAt = time.Now()

	// Specify the Upsert option to insert a new document if a document matching
	// the filter isn't found.
	opts := options.Update().SetUpsert(true)
	filter := bson.M{"email": user.Email}
	update := bson.M{"$set": user}

	result, err := c.collection.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		return err
	}

	if result.MatchedCount != 0 {
		log.Println("matched and replaced an existing user")
	}
	if result.UpsertedCount != 0 {
		log.Printf("inserted a new user with ID %v\n", result.UpsertedID)
	}
	return nil
}

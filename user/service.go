package user

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}
func (s *Service) FindUserById(id string) (entity.User, error) {
	return s.repo.FindUserById(id)
}

func (s *Service) FindUserByEmail(email string) (entity.User, error) {
	return s.repo.FindUserByEmail(email)
}
func (s *Service) FindUserByActivationCode(activationCode string) (entity.User, error) {
	return s.repo.FindUserByActivationCode(activationCode)
}

func (s *Service) FindUsersByQuery(query bson.M) ([]entity.User, error) {
	return s.repo.FindUsersByQuery(query)
}

func (s *Service) UpdateUser(user *entity.User) error {
	return s.repo.UpdateUser(user)
}

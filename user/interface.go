package user

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindUsersByQuery(query bson.M) ([]entity.User, error)
	FindUserById(id string) (entity.User, error)
	FindUserByEmail(email string) (entity.User, error)
	FindUserByActivationCode(activationCode string) (entity.User, error)
}

// Writer bookmark writer
type Writer interface {
	UpdateUser(user *entity.User) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

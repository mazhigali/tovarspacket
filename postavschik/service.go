package postavschik

import (
	"gitlab.com/mazhigali/tovarspacket/entity"

	"go.mongodb.org/mongo-driver/bson"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByNameTranslit(name string) (*entity.Organization, error) {
	return s.repo.FindByNameTranslit(name)
}

func (s *Service) FindById(id string) (*entity.Organization, error) {
	return s.repo.FindById(id)
}

func (s *Service) FindLog(name string) (*entity.LogPostavschik, error) {
	return s.repo.FindLog(name)
}

func (s *Service) FindAll() ([]entity.Organization, error) {
	return s.repo.FindAll()
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.Organization, error) {
	return s.repo.FindByQuery(query)
}

func (s *Service) UpdateByNameTranslit(p *entity.Organization) error {
	return s.repo.UpdateByNameTranslit(p)
}

func (s *Service) UpdateLog(l *entity.LogPostavschik) error {
	return s.repo.UpdateLog(l)
}

func (s *Service) Remove(p *entity.Organization) error {
	return s.repo.Remove(p)
}

package postavschik

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindById(id string) (*entity.Organization, error)
	FindByNameTranslit(name string) (*entity.Organization, error)
	FindAll() ([]entity.Organization, error)
	FindByQuery(query bson.M) ([]entity.Organization, error)
	FindLog(namePostavschik string) (*entity.LogPostavschik, error)
}

// Writer tovar writer
type Writer interface {
	UpdateByNameTranslit(p *entity.Organization) error
	UpdateLog(l *entity.LogPostavschik) error
	Remove(p *entity.Organization) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

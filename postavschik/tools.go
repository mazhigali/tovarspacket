package postavschik

import (
	"errors"

	"strings"

	compareTextPacket "gitlab.com/mazhigali/comparetextpacket"
	"gitlab.com/mazhigali/tovarspacket/entity"
)

// получает правило из массива правил поставщика если совпадает имя листа.
// если имя листа не совпадает то выдает дефолтное правило
func GetRuleParsing(p *entity.Organization, whatFor, listSheet string) (entity.RuleParsing, error) {
	for _, r := range p.RulesParsing {
		if r.ListXls == listSheet && r.WhatFor == whatFor {
			return r, nil
		}
	}

	//на этом этапе не нашелось совпадение листа и поэтому ищем в дефолтных правилах
	for _, r := range p.RulesParsing {
		if r.ListXls == "Default" && r.WhatFor == whatFor {
			return r, nil
		}
	}
	//если дошло до сюда то правило не нашлось
	var res entity.RuleParsing
	err := errors.New("Not Found Default RULE PARSING")
	return res, err
}

func GetRuleCheck(p *entity.Organization, whatFor string) (entity.CheckRule, error) {
	for _, r := range p.CheckRules {
		if r.WhatFor == whatFor {
			return r, nil
		}
	}

	//если дошло до сюда то правило не нашлось
	var res entity.CheckRule
	err := errors.New("Not Found Default CHECK RULE FOR PARSING")
	return res, err
}

// определяет какую скидку взять из массива правил скидок
func GetSkidka(skidki []entity.Skidka, tovar *entity.Tovar, listXlsName string) float64 {
	var skidkaFloat float64
	for _, skidka := range skidki {
		//если true то скидка действует на весь ассортимент поставщка
		if skidka.AllTovarsPostavschik == true {
			skidkaFloat = *skidka.Skidka
		}

		//берем лист экселя
		if len(listXlsName) > 0 {
			if listXlsName == skidka.ListXls {
				skidkaFloat = *skidka.Skidka
			}
		}
		//берем бренд если другие поля пустые
		if len(skidka.Brand) > 0 {
			if compareTextPacket.CleanStringAndUpperCase(skidka.Brand) == compareTextPacket.CleanStringAndUpperCase(tovar.Proizvoditel) && skidka.Category == "" && skidka.SlovoInName == "" {
				skidkaFloat = *skidka.Skidka
			}
		}

		//берем категорию вместе с брендом
		if len(skidka.Category) > 0 {
			if len(compareTextPacket.SearchPartOfStringInSlice(skidka.Category, tovar.Categories)) > 0 && compareTextPacket.CleanStringAndUpperCase(skidka.Brand) == compareTextPacket.CleanStringAndUpperCase(tovar.Proizvoditel) {
				skidkaFloat = *skidka.Skidka
			}
		}

		if len(skidka.Seriya) > 0 {
			if compareTextPacket.CleanStringAndUpperCase(skidka.Seriya) == compareTextPacket.CleanStringAndUpperCase(tovar.Seriya) {
				skidkaFloat = *skidka.Skidka
			}
		}

		if len(skidka.Model) > 0 {
			if compareTextPacket.CleanStringAndUpperCase(skidka.Model) == compareTextPacket.CleanStringAndUpperCase(tovar.Model) {
				skidkaFloat = *skidka.Skidka
			}
		}

		//берем совпадение в имени вместе с брендом
		if len(skidka.SlovoInName) > 0 {
			if strings.Contains(compareTextPacket.CleanStringAndUpperCase(tovar.Name), compareTextPacket.CleanStringAndUpperCase(skidka.SlovoInName)) == true && compareTextPacket.CleanStringAndUpperCase(skidka.Brand) == compareTextPacket.CleanStringAndUpperCase(tovar.Proizvoditel) {
				skidkaFloat = *skidka.Skidka
			}
		}
		//берем Артикулы. если артикул товара есть в массиве артикулов то берется эта скидка
		if len(skidka.Articuls) > 0 {
			if len(compareTextPacket.SearchStringInSlice(tovar.Articul, skidka.Articuls)) > 0 {
				skidkaFloat = *skidka.Skidka
			}
		}

		//берем айдишники. если айди товара есть в массиве артикулов то берется эта скидка
		if len(skidka.Ids) > 0 {
			if len(compareTextPacket.SearchStringInSlice(tovar.Id.Hex(), skidka.Ids)) > 0 {
				skidkaFloat = *skidka.Skidka
			}
		}

	}

	return skidkaFloat
}

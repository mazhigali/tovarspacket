package postavschik

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindByNameTranslit(name string) (*entity.Organization, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}

	result := entity.Organization{}

	err := c.collection.FindOne(context.TODO(), bson.M{"nameTranslit": name}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}

}

func (c *MongoConnection) FindById(id string) (*entity.Organization, error) {

	if id == "" {
		return nil, entity.ErrInputValueNil
	}

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	result := entity.Organization{}

	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectId}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindLog(namePostavschik string) (*entity.LogPostavschik, error) {
	collectionLog := c.client.Database(c.dbname).Collection("logPostavschik")
	if collectionLog == nil {
		return nil, errors.New("Collection could not be created, maybe need to create it manually")
	}

	result := entity.LogPostavschik{}

	err := collectionLog.FindOne(context.TODO(), bson.M{"namePostavschik": namePostavschik}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindAll() ([]entity.Organization, error) {
	resultArray := []entity.Organization{}

	cursor, err := c.collection.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByQuery(query bson.M) ([]entity.Organization, error) {
	resultArray := []entity.Organization{}

	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) UpdateByNameTranslit(p *entity.Organization) (err error) {
	if p.NameTranslit == "" {
		return entity.ErrInputValueNil
	}

	query := bson.M{"nameTranslit": p.NameTranslit}
	update := bson.M{"$set": p}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return nil
	default:
		log.Println("ERROR Update " + err.Error())
		return err
	}
}

func (c *MongoConnection) UpdateLog(l *entity.LogPostavschik) (err error) {
	collectionLog := c.client.Database(c.dbname).Collection("logPostavschik")
	if collectionLog == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	l.TimeCreated = time.Now()
	query := bson.M{"namePostavschik": l.NamePostavschik}
	update := bson.M{"$set": l}

	_, err = collectionLog.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return nil
	default:
		log.Println("ERROR Update " + err.Error())
		return err
	}
}

func (c *MongoConnection) Remove(p *entity.Organization) (err error) {
	if p.NameTranslit == "" {
		return entity.ErrInputValueNil
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"nameTranslit": p.NameTranslit})
	if err != nil {
		return errors.New("ERROR REMOVE" + err.Error())
	}

	return err
}

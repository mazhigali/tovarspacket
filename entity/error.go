package entity

import "errors"

//ErrNotFound not found
var ErrInputValueNil = errors.New("Input value is empty")
var ErrNoMongoId = errors.New("Not input MongoID")
var ErrNotFound = errors.New("Not found")
var ErrNoDataFromMarket = errors.New("No Data from Market")

var ErrDublicateInSlice = errors.New("Dublicate: Element is already in slice")

//ErrCannotBeDeleted bookmark cannot be deleted
var ErrCannotBeDeleted = errors.New("Cannot Be Deleted")

//Err postavshik
var ErrPostavschikChangedFormat = errors.New("Changed Format Postavschik")

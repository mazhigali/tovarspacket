package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID    primitive.ObjectID `bson:"_id" `
	Email string             `bson:"email,omitempty" json:"email,omitempty" mapstructure:"email"` //unique

	HashedPassword []byte `json:"-" bson:"hashedPassword,omitempty" ` // password is the client-given password

	Role string `bson:"role,omitempty" json:"role,omitempty" mapstructure:"role"` //retailer || provider || admin || user

	LoggedAt     time.Time `bson:"loggedAt,omitempty" json:"loggedAt,omitempty" ` //последний раз залогинен
	RefreshToken string    `json:"-" bson:"refreshToken" `                        //for jwt auth

	Activated bool `json:"activated,omitempty" bson:"activated,omitempty" mapstructure:"activated"` // is user activated

	ActivationCode string `json:"activationCode,omitempty" bson:"activationCode,omitempty" mapstructure:"activationCode"` //код активации для ссылки в письме
	FIO            string `bson:"fio,omitempty" json:"fio,omitempty" mapstructure:"fio"`
	TelMob         string `bson:"telMob,omitempty" json:"telMob,omitempty" mapstructure:"telMob"`
	AvatarUrlLocal string `json:"avatarUrlLocal,omitempty" bson:"avatarUrlLocal,omitempty" mapstructure:"avatarUrlLocal"`

	TarifActive   bool      `json:"tarifActive,omitempty" bson:"tarifActive,omitempty" mapstructure:"tarifActive"`       //тариф активен
	TarifPaidTill time.Time `json:"tarifPaidTill,omitempty" bson:"tarifPaidTill,omitempty" mapstructure:"tarifPaidTill"` //дата окончания оплаченного тарифа
	PaymentsIds   []string  `json:"paymentsIds,omitempty" bson:"paymentsIds,omitempty" mapstructure:"paymentsIds"`       //айдишники платежей

	CreatedAt time.Time `bson:"createdAt,omitempty" json:"createdAt,omitempty" `
	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty" `

	AtsNum string `json:"atsNum,omitempty" bson:"atsNum,omitempty" mapstructure:"atsNum"`
}

package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Brand struct {
	Id                primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Name              string             `bson:"name" json:"name,omitempty"`
	NameTranslit      string             `bson:"nameTranslit" json:"nameTranslit,omitempty"` //название в урле
	IdVnutr           string             `bson:"idVnutr,omitempty" json:"idVnutr,omitempty"` //айдишник коллекции
	URL               string             `bson:"url" json:"url,omitempty"`
	Proizvoditel      string             `bson:"proizvoditel,omitempty" json:"proizvoditel,omitempty"`
	Opisanie          string             `bson:"opisanie,omitempty" json:"opisanie,omitempty"`
	OpisanieShort     string             `bson:"opisanieShort,omitempty" json:"opisanieShort,omitempty"`
	StranaIzgotovitel string             `bson:"stranaIzgotovitel,omitempty" json:"stranaIzgotovitel,omitempty"`
	ImageUrl          string             `bson:"imageUrl,omitempty" json:"imageUrl,omitempty"`
	ImageUrlLocal     string             `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty"`         //ссылка на изображение на сервере
	CertImageUrlLocal string             `bson:"certImageUrlLocal,omitempty" json:"certImageUrlLocal,omitempty"` //ссылка на изображение сертификата на сервере
	ImagesMore        []string           `bson:"imagesmore,omitempty" json:"imagesMore,omitempty"`               // ссылка на изображения локальные
	Videos            []string           `bson:"videos,omitempty" json:"videos,omitempty"`
	Documents         []string           `bson:"documents,omitempty" json:"documents,omitempty"`
	Otzyvy            []string           `bson:"otzyvy,omitempty" json:"otzyvy,omitempty"`
	UpdatedTime       time.Time          `bson:"updatedTime" json:"updatedTime,omitempty"` //время когда был обновлен
}

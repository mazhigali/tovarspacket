package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// в отдельной коллекции сущность Organization
type Organization struct {
	ID            primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name          string             `json:"name,omitempty" bson:"name,omitempty"`                 // обычное название для себя - required
	NameTranslit  string             `bson:"nameTranslit,omitempty" json:"nameTranslit,omitempty"` //название в урле и для загрузки - uniq, required
	Type          string             `json:"type,omitempty" bson:"type,omitempty"`                 // "наш склад", "наша компания" , "поставщик", "водитель" , "покупатель", "партнер"
	ContactIds    []string           `json:"contactIds,omitempty" bson:"contactIds"`               //контактные лица
	Rekvizity     []Rekvizity        `json:"rekvizity,omitempty" bson:"rekvizity,omitempty"`
	RekvizityMain *Rekvizity         `json:"rekvizityMain,omitempty" bson:"rekvizityMain,omitempty"`
	Comments      []Comment          `json:"comments,omitempty" bson:"comments,omitempty"`

	Voditel *Voditel `json:"voditel,omitempty" bson:"voditel,omitempty"`

	EmailFolderName     string          `json:"emailFolderName,omitempty" bson:"emailFolderName,omitempty"` //имя папки где искать прайс
	EmailMatchNameFiles []string        `json:"emailMatchNameFiles" bson:"emailMatchNameFiles,omitempty"`   //скачивает прайс если совпадает
	LichnyKabinets      []LichnyKabinet `json:"lichnyKabinets,omitempty" bson:"lichnyKabinets,omitempty"`   //это для поставщиков

	Skidki        []Skidka       `json:"skidki,omitempty" bson:"skidki,omitempty"`
	Nacenki       []Skidka       `json:"nacenki,omitempty" bson:"nacenki,omitempty"`
	RulesParsing  []RuleParsing  `json:"rulesParsing,omitempty" bson:"rulesParsing,omitempty"`
	CheckRules    []CheckRule    `json:"checkRules,omitempty" bson:"checkRules,omitempty"`
	PricePostavka *PricePostavka `json:"pricePostavka,omitempty" bson:"pricePostavka,omitempty"` //загруженный прайс
}

type Voditel struct {
	Fio       string `json:"fio,omitempty" bson:"fio,omitempty"`
	Passport  string `json:"passport,omitempty" bson:"passport,omitempty"`
	Telephone string `json:"telephone,omitempty" bson:"telephone,omitempty"`
}

type Car struct {
	NomerCar string `json:"nomerCar,omitempty" bson:"nomerCar,omitempty"`
	MarkaCar string `json:"markaCar,omitemptya" bson:"markaCar,omitempty"`
}

type Rekvizity struct {
	YurType           string          `json:"yurType,omitempty" bson:"yurType,omitempty"` //физлицо || ООО || ИП || самозанятый
	INN               string          `json:"inn,omitempty" bson:"inn,omitempty"`         //unique
	Name              string          `json:"name,omitempty" bson:"name,omitempty"`
	KPP               string          `json:"kpp,omitempty" bson:"kpp,omitempty"`
	OGRN              string          `json:"ogrn,omitempty" bson:"ogrn,omitempty"`
	AddressYur        string          `json:"addressYur,omitempty" bson:"addressYur,omitempty"`
	AddressPochta     string          `json:"addressPochta,omitempty" bson:"addressPochta,omitempty"`
	AddressDostavka   string          `json:"addressDostavka,omitempty" bson:"addressDostavka,omitempty"`
	GenDirFio         string          `json:"genDirFio,omitempty" bson:"genDirFio,omitempty"`
	Phone             string          `json:"phone,omitempty" bson:"phone,omitempty"`
	Email             string          `json:"email,omitempty" bson:"email,omitempty"`
	BankRekvizity     []BankRekvizity `json:"bankRekvizity,omitempty" bson:"bankRekvizity,omitempty"`
	MainBankRekvizity BankRekvizity   `json:"mainBankRekvizity,omitempty" bson:"mainBankRekvizity,omitempty"`
}

type BankRekvizity struct {
	BankName        string   `json:"bankName,omitempty" bson:"bankName,omitempty"`
	BIK             string   `json:"bik,omitempty" bson:"bik,omitempty"`
	KorSchet        string   `json:"korSchet,omitempty" bson:"korSchet,omitempty"`
	RasschetnySchet string   `json:"rasschetnySchet,omitempty" bson:"rasschetnySchet,omitempty"` //uniq
	KoshelekIds     []string `json:"koshelekIds,omitempty" bson:"koshelekIds,omitempty"`
}

type LichnyKabinet struct {
	Link     string `json:"link" bson:"link"`
	Login    string `json:"login" bson:"login"`
	Password string `json:"password" bson:"password"`
	Comment  string `json:"comment" bson:"comment"`
}

// в отдельной коллекции сущность LogPostavschik
type LogPostavschik struct {
	NamePostavschik         string       `json:"namePostavschik,omitempty" bson:"namePostavschik,omitempty"` //имя поставщика
	ParsingLogSkladFound    []ParsingLog `json:"parsingLogSkladFound,omitempty" bson:"parsingLogSkladFound,omitempty"`
	ParsingLogSkladNotFound []ParsingLog `json:"parsingLogSkladNotFound,omitempty" bson:"parsingLogSkladNotFound,omitempty"`
	ParsingLogPriceFound    []ParsingLog `json:"parsingLogPriceFound,omitempty" bson:"parsingLogPriceFound,omitempty"`
	ParsingLogPriceNotFound []ParsingLog `json:"parsingLogPriceNotFound,omitempty" bson:"parsingLogPriceNotFound,omitempty"`
	ParsingLogSPFound       []ParsingLog `json:"parsingLogSPFound,omitempty" bson:"parsingLogSPFound,omitempty"`
	ParsingLogSPNotFound    []ParsingLog `json:"parsingLogSPNotFound,omitempty" bson:"parsingLogSPNotFound,omitempty"`
	TimeCreated             time.Time    `json:"timeCreated" bson:"timeCreated"`
}

type Skidka struct {
	AllTovarsPostavschik bool     `json:"allTovarsPostavschik" bson:"allTovarsPostavschik"` //если TRUE то тогда скидка действует на любой товар поставщика
	ListXls              string   `json:"listXls,omitempty" bson:"listXls"`
	Brand                string   `json:"brand,omitempty" bson:"brand"`
	Category             string   `json:"category,omitempty" bson:"category"`
	Seriya               string   `json:"seriya,omitempty" bson:"seriya"`
	Model                string   `json:"model,omitempty" bson:"model"`
	SlovoInName          string   `json:"slovoInName,omitempty" bson:"slovoInName"`
	Articuls             []string `json:"articuls,omitempty" bson:"articuls"`
	Ids                  []string `json:"ids" bson:"ids"` //TODO айдишники товаров
	Skidka               *float64 `json:"skidka,omitempty" bson:"skidka"`
	//TODO еще в расчет скидки - там не всегда скидка иногда просто курсовая разница/ еще в расчет скидки - там не всегда скидка иногда просто курсовая разница
}

type RuleParsing struct { //999 означает что значение не задано
	WhatFor             string `json:"whatFor" bson:"whatFor"` //"All" - for all | "Sklad" - for sklad | "Price" for Price
	ListXls             string `json:"listXls" bson:"listXls"` // "Default" если для всех листов
	StolbecNumName      int    `json:"stolbecNumName" bson:"stolbecNumName"`
	StolbecNumArticul   int    `json:"stolbecNumArticul" bson:"stolbecNumArticul"`
	StolbecNumBrand     int    `json:"stolbecNumBrand" bson:"stolbecNumBrand"`
	StolbecNumOstatok   int    `json:"stolbecNumOstatok" bson:"stolbecNumOstatok"`
	StolbecNumPriceRozn int    `json:"stolbecNumPriceRozn" bson:"stolbecNumPriceRozn"`
	StolbecNumPriceOpt  int    `json:"stolbecNumPriceOpt" bson:"stolbecNumPriceOpt"`

	StolbecSootvetstviePostavschik int `json:"stolbecSootvetstviePostavschik" bson:"stolbecSootvetstviePostavschik"` //столбец для записи соответствия 999 значит что не задан столбец соответствия

	RawBeginNum     int `json:"rawBeginNum" bson:"rawBeginNum"`         // с какого ряда начинать прасинг
	StolbecCheckNum int `json:"stolbecCheckNum" bson:"stolbecCheckNum"` //проверить заполненность ячейки
}

type CheckRule struct {
	WhatFor   string `json:"whatFor" bson:"whatFor"` //"All" - for all | "Sklad" - for sklad | "Price" for Price
	RowNum    int    `json:"rowNum" bson:"rowNum"`
	ColumnNum int    `json:"columnNum" bson:"columnNum"`
	Value     string `json:"value" bson:"value"`
}

type ParsingLog struct {
	NameInFile      string  `json:"nameInFile" bson:"nameInFile"`
	ArticulInFile   string  `json:"articulInFile" bson:"articulInFile"`
	BrandInFile     string  `json:"brandInFile" bson:"brandInFile"`
	SkladInFile     string  `json:"skladInFile" bson:"skladInFile"`
	PriceRoznInFile string  `json:"priceRoznInFile" bson:"priceRoznInFile"`
	PriceOptInFile  string  `json:"priceOptInFile" bson:"priceOptInFile"`
	ArticulInDb     string  `json:"articulInDb" bson:"articulInDb"`
	IdVnutrInDb     string  `json:"idVnutrInDb" bson:"idVnutrInDb"`
	NameInDb        string  `json:"nameInDb" bson:"nameInDb"`
	BrandInDb       string  `json:"brandInDb" bson:"brandInDb"`
	SkladInDb       float64 `json:"skladInDb" bson:"skladInDb"`
	PriceRoznInDb   float64 `json:"priceRoznInDb" bson:"priceRoznInDb"`
	PriceOptInDb    float64 `json:"priceOptInDb" bson:"priceOptInDb"`
	HowToSearch     string  `json:"howToSearch" bson:"howToSearch"` //Mongo, elastic
}

type PricePostavka struct {
	Tovars         []Tovar   `json:"tovars" bson:"tovars"`
	TimeCreated    time.Time `json:"timeCreated" bson:"timeCreated"`
	PriceFileLocal string    `json:"priceFileLocal" bson:"priceFileLocal"`
}

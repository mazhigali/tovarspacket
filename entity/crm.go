package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Информация о контактных лицах внутри клиентских организаций. Включает в себя имена, должности, контактные данные и другую сопутствующую информацию.
type Contact struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name      string             `json:"name,omitempty" bson:"name,omitempty"`
	Email     string             `json:"email,omitempty" bson:"email,omitempty"`         //unique
	Phone     string             `json:"phone,omitempty" bson:"phone,omitempty"`         //unique
	PhoneE164 string             `json:"phoneE164,omitempty" bson:"phoneE164,omitempty"` //unique, формат e164 +79787071137
	DopPhones []string           `json:"dopPhones,omitempty" bson:"dopPhones,omitempty"`
	Comment   string             `json:"comment,omitempty" bson:"comment,omitempty"`
	CreatedAt time.Time          `bson:"createdAt" json:"createdAt,omitempty"`
	UpdatedAt time.Time          `bson:"updatedTime" json:"updatedTime,omitempty"`
}

// Сущность, представляющая клиентов. Включает в себя информацию о контактных данных клиента, истории покупок, предпочтениях и т.д.
type Customer struct {
	ID                 primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name               string             `json:"name,omitempty" bson:"name,omitempty"`                             // обычное название для себя
	ContactMainId      string             `json:"contactMainId,omitempty" bson:"contactMainId,omitempty"`           //основное контактное лицо
	ContactIds         []string           `json:"contactIds,omitempty" bson:"contactIds,omitempty"`                 //контактные лица
	ContactMainForSite *Contact           `json:"contactMainForSite,omitempty" bson:"contactMainForSite,omitempty"` //основное контактное лицо
	ContactsForSite    []Contact          `json:"contactsForSite,omitempty" bson:"contactsForSite,omitempty"`       //контакты для сайта
	UpdatedAt          time.Time          `bson:"updatedTime" json:"updatedTime,omitempty"`
	CreatedAt          time.Time          `bson:"createdAt" json:"createdAt,omitempty"`
	ManagerId          string             `json:"managerId,omitempty" bson:"managerId,omitempty"`
	Comment            string             `json:"comment,omitempty" bson:"comment,omitempty"`
	//OrderIds  []string    `json:"orderIds,omitempty" bson:"orderIds,omitempty"`
	YurType             string        `json:"yurType,omitempty" bson:"yurType,omitempty"` //физлицо || организация
	OrganizationId      string        `json:"organizationId,omitempty" bson:"organizationId,omitempty"`
	OrganizationForSite *Organization `json:"organizationForSite,omitempty" bson:"organizationForSite,omitempty"` //Организация для сайта
}

type Task struct {
	ID                primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Title             string             `json:"title,omitempty" bson:"title,omitempty"`
	Body              string             `json:"body,omitempty" bson:"body,omitempty"`
	DeadlineStr       string             `json:"deadlineStr,omitempty" bson:"deadlineStr,omitempty"`
	Deadline          time.Time          `json:"deadline,omitempty" bson:"deadline,omitempty"` //дедлайн
	Status            string             `json:"status,omitempty" bson:"status,omitempty"`
	PostavManagerId   string             `json:"postavManagerId,omitempty" bson:"postavManagerId,omitempty"`
	OtvetstvManagerId string             `json:"otvetstvManagerId,omitempty" bson:"otvetstvManagerId,omitempty"`
	OrderId           string             `json:"orderId,omitempty" bson:"orderId,omitempty"`

	UpdatedAt time.Time `bson:"updatedTime" json:"updatedTime,omitempty"`
	CreatedAt time.Time `bson:"createdAt" json:"createdAt,omitempty"`
}

type Call struct {
	ID            primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	AtsId         string             `json:"atsId,omitempty" bson:"atsId,omitempty"`                 //постоянный ID внешнего звонка в АТС
	CallIDWithRec string             `json:"callIDWithRec,omitempty" bson:"callIDWithRec,omitempty"` //уникальный id звонка с записью разговора
	CallStart     time.Time          `json:"callStart,omitempty" bson:"callStart,omitempty"`         //время начала звонка
	CallerNumber  string             `json:"callerNumber,omitempty" bson:"callerNumber,omitempty"`   //номер звонящего

	NumberCallTo string `json:"numberCallTo,omitempty" bson:"numberCallTo,omitempty"` //номер на который позвонили
	AtsNumber    string `json:"atsNumber,omitempty" bson:"atsNumber,omitempty"`       //внутренний номер

	Duration  int64  `json:"duration,omitempty" bson:"duration,omitempty"`   //длительность в секундах
	Direction string `json:"direction,omitempty" bson:"direction,omitempty"` // in || out
	Status    string `json:"status,omitempty" bson:"status,omitempty"`
	//    answered – разговор,
	//    busy – занято,
	//    cancel - отменен,
	//    no answer - без ответа,
	//    failed - не удался,
	//    no money - нет средств, превышен лимит,
	//    unallocated number - номер не существует,
	//    no limit - превышен лимит,
	//    no day limit - превышен дневной лимит,
	//    line limit - превышен лимит линий,
	//    no money, no limit - превышен лимит;

	RecordUrl       string `json:"recordUrl,omitempty" bson:"recordUrl,omitempty"`
	FileRecordLocal string `json:"fileRecordLocal,omitempty" bson:"fileRecordLocal,omitempty"`

	OrderId   string    `json:"orderId,omitempty" bson:"orderId,omitempty"`
	UpdatedAt time.Time `bson:"updatedTime" json:"updatedTime,omitempty"`
	CreatedAt time.Time `bson:"createdAt" json:"createdAt,omitempty"`
}

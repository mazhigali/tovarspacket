package entity

import "time"

type LogParser struct {
	Url       string
	SiteId    string
	TimeParse time.Time
}

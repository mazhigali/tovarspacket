package entity

type Region struct {
	Id           int    `json:"id"`
	MpttLevel    int    `json:"mptt_level"` //уровень вложения
	ParentID     int    `json:"parent_id"`
	Name         string `json:"name"`
	NameTranslit string `json:"name_translit"` //slug
	GenitiveName string `json:"genitive_name"` //склонение
	Favorite     bool   `json:"favorite"`
	Priority     int    `json:"priority"`
	Special      bool   `json:"special"`
}

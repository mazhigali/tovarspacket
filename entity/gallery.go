package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Gallery struct {
	Id            primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Name          string             `bson:"name" json:"name,omitempty"`
	NameTranslit  string             `bson:"nameTranslit" json:"nameTranslit,omitempty"` //название в урле
	IdVnutr       string             `bson:"idVnutr,omitempty" json:"idVnutr,omitempty"` //айдишник коллекции
	URL           string             `bson:"url" json:"url,omitempty"`
	Opisanie      string             `bson:"opisanie,omitempty" json:"opisanie,omitempty"`
	OpisanieShort string             `bson:"opisanieShort,omitempty" json:"opisanieShort,omitempty"`
	ImageUrl      string             `bson:"imageUrl,omitempty" json:"imageUrl,omitempty"`
	ImageUrlLocal string             `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty"` //ссылка на изображение на сервере
	ImagesMore    []string           `bson:"imagesmore,omitempty" json:"imagesMore,omitempty"`       // ссылка на изображения локальные
	Images        []Image            `bson:"images,omitempty" json:"images,omitempty"`
	CategoryIds   []string           `bson:"categoryIds,omitempty" json:"categoryIds,omitempty"`
	Videos        []string           `bson:"videos,omitempty" json:"videos,omitempty"`
	Documents     []string           `bson:"documents,omitempty" json:"documents,omitempty"`
	UpdatedTime   time.Time          `bson:"updatedTime" json:"updatedTime,omitempty"` //время когда был обновлен
}

type Image struct {
	Id                 primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name               string             `bson:"name,omitempty" json:"name,omitempty"`
	NameTranslit       string             `bson:"nameTranslit,omitempty" json:"nameTranslit,omitempty"` //название в урле
	IdVnutr            string             `bson:"idVnutr,omitempty" json:"idVnutr,omitempty"`           //айдишник коллекции
	URL                string             `bson:"url,omitempty" json:"url,omitempty"`
	Opisanie           string             `bson:"opisanie,omitempty" json:"opisanie,omitempty"`
	OpisanieShort      string             `bson:"opisanieShort,omitempty" json:"opisanieShort,omitempty"`
	ImageUrl           string             `bson:"imageUrl,omitempty" json:"imageUrl,omitempty"`
	ImageUrlLocal      string             `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty"`           //ссылка на изображение на сервере для сайта
	ImageSmallUrlLocal string             `bson:"imageSmallUrlLocal,omitempty" json:"imageSmallUrlLocal,omitempty"` //ссылка на mini изображение на сервере
	FileLocalPath      string             `bson:"fileLocalPath,omitempty" json:"fileLocalPath,omitempty"`           //полный локальный путь
	FileName           string             `bson:"fileName,omitempty" json:"fileName,omitempty" msgpack:"fileName,omitempty"`
	FileSize           uint64             `bson:"fileSize,omitempty" json:"fileSize,omitempty" msgpack:"fileSize,omitempty"`
	Alt                string             `bson:"alt,omitempty" json:"alt,omitempty"`
	Title              string             `bson:"title,omitempty" json:"title,omitempty"`
	Description        string             `bson:"description,omitempty" json:"description,omitempty"`
	UpdatedTime        time.Time          `bson:"updatedTime,omitempty" json:"updatedTime,omitempty"` //время когда был обновлен
}

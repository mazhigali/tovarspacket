package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Post struct {
	Id                primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Name              string             `bson:"name" json:"name,omitempty"`
	NameTranslit      string             `bson:"nameTranslit" json:"nameTranslit,omitempty"` //название в урле
	IdVnutr           string             `bson:"idVnutr,omitempty" json:"idVnutr,omitempty"` //TODO HZ
	URL               string             `bson:"url" json:"url,omitempty"`
	Proizvoditel      string             `bson:"proizvoditel,omitempty" json:"proizvoditel,omitempty"`
	Opisanie          string             `bson:"opisanie,omitempty" json:"opisanie,omitempty"`
	OpisanieHtml      interface{}        `bson:"opisanieHtml,omitempty" json:"opisanieHtml,omitempty"`
	OpisanieShort     string             `bson:"opisanieShort,omitempty" json:"opisanieShort,omitempty"`
	OpisanieShortHtml interface{}        `bson:"opisanieShortHtml,omitempty" json:"opisanieShortHtml,omitempty"`
	StranaIzgotovitel string             `bson:"stranaIzgotovitel,omitempty" json:"stranaIzgotovitel,omitempty"`
	ImageUrl          string             `bson:"imageUrl,omitempty" json:"imageUrl,omitempty"`
	ImageUrlLocal     string             `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty"` //ссылка на изображение на сервере
	ImagesMore        []string           `bson:"imagesmore,omitempty" json:"imagesMore,omitempty"`       // ссылка на изображения локальные
	Images            []Image            `bson:"images,omitempty" json:"images,omitempty"`               //работы
	Videos            []string           `bson:"videos,omitempty" json:"videos,omitempty"`
	Documents         []string           `bson:"documents,omitempty" json:"documents,omitempty"`
	Otzyvy            []string           `bson:"otzyvy,omitempty" json:"otzyvy,omitempty"`
	UpdatedTime       time.Time          `bson:"updatedTime" json:"updatedTime,omitempty"` //время когда был обновлен
	Title             string             `bson:"title" json:"title,omitempty"`
	Description       string             `bson:"description" json:"description,omitempty"`

	PartnerIdVnutr string `bson:"partnerIdVnutr" json:"partnerIdVnutr,omitempty"`

	RelatedTovarIds []string `bson:"relatedTovarIds,omitempty" json:"relatedTovarIds,omitempty"` //товары с которыми связан данный пост или проект
}

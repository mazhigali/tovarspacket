package entity

type Counter struct {
	Name  string `bson:"name" json:"name,omitempty"`
	Count int    `bson:"count" json:"count"`
}

package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Tovar struct {
	Id primitive.ObjectID `bson:"_id" json:"id,omitempty"`

	Name                    string         `bson:"name" json:"name,omitempty" msgpack:"name,omitempty"`
	NameTranslit            string         `bson:"nameTranslit,omitempty" json:"nameTranslit,omitempty" msgpack:"nameTranslit,omitempty"` //название в урле
	IdVnutr                 string         `bson:"idVnutr,omitempty" json:"idVnutr,omitempty" msgpack:"idVnutr,omitempty"`                //внутренний адйшиник сайта донора
	IdBitrix                string         `bson:"idBitrix,omitempty" json:"idBitrix,omitempty" msgpack:"idBitrix,omitempty"`
	URL                     string         `bson:"url" json:"url,omitempty" msgpack:"url,omitempty"`
	UrlBitrix               string         `bson:"urlBitrix,omitempty" json:"urlBitrix,omitempty" msgpack:"-"` //URl in Bitrix
	Articul                 string         `bson:"articul,omitempty" json:"articul,omitempty" msgpack:"articul,omitempty"`
	ArticulsMore            []string       `bson:"articulsMore,omitempty" json:"articulsMore,omitempty" msgpack:"articulsMore,omitempty"` // другие артикулы какие могут быть
	EAN                     string         `bson:"ean,omitempty" json:"ean,omitempty" msgpack:"ean,omitempty"`
	EANMore                 []string       `bson:"eanMore,omitempty" json:"eanMore,omitempty" msgpack:"eanMore,omitempty"` //другие EAN какие могут быть
	Categories              []string       `bson:"Categories,omitempty" json:"categories,omitempty" msgpack:"categories,omitempty"`
	CategoriesForSite       []Category     `bson:"categoriesForSite,omitempty" json:"categoriesForSite,omitempty" msgpack:"categoriesForSite,omitempty"` //категории для сателлитов
	Proizvoditel            string         `bson:"proizvoditel,omitempty" json:"proizvoditel,omitempty" msgpack:"proizvoditel,omitempty"`
	Opisanie                string         `bson:"opisanie,omitempty" json:"opisanie,omitempty" msgpack:"opisanie,omitempty"`
	OpisanieShort           string         `bson:"opisanieShort,omitempty" json:"opisanieShort,omitempty" msgpack:"opisanieShort,omitempty"`
	Seriya                  string         `bson:"seriya,omitempty" json:"seriya,omitempty" msgpack:"seriya,omitempty"`
	Collections             []string       `bson:"collections,omitempty" json:"collections,omitempty" msgpack:"collections,omitempty"`                      //айдишники коллекций в коллекции
	CollectionsForSite      []Tovar        `bson:"collectionsForSite,omitempty" json:"collectionsForSite,omitempty" msgpack:"collectionsForSite,omitempty"` //вычисляемое поле для сайта где хранятся сущности коллекций
	Model                   string         `bson:"model,omitempty" json:"model,omitempty" msgpack:"model,omitempty"`
	StranaIzgotovitel       string         `bson:"stranaIzgotovitel,omitempty" json:"stranaIzgotovitel,omitempty" msgpack:"stranaIzgotovitel,omitempty"`
	ExternalId              string         `bson:"externalId,omitempty" json:"externalId,omitempty" msgpack:"externalId,omitempty"`
	Price                   float64        `bson:"price" json:"price,omitempty" msgpack:"price,omitempty"`
	UpdatedPriceAt          *time.Time     `bson:"updatedPriceAt,omitempty" json:"updatedPriceAt,omitempty" msgpack:"updatedPriceAt,omitempty"`                   //время когда была обновлена цена
	KursPrice               float64        `bson:"kursPrice" json:"kursPrice,omitempty" msgpack:"kursPrice,omitempty"`                                            //курс валюты поставщика
	ValutaPrice             string         `bson:"valutaPrice,omitempty" json:"valutaPrice,omitempty" msgpack:"valutaPrice,omitempty"`                            //валюта цены которая идет на сайт
	EdinicaIzmereniya       string         `bson:"edinicaIzmereniya,omitempty" json:"edinicaIzmereniya,omitempty" msgpack:"edinicaIzmereniya,omitempty"`          //единица измерения товара (шт, м2 итд)
	PriceRecommend          float64        `bson:"priceRecommend" json:"priceRecommend,omitempty" msgpack:"priceRecommend,omitempty"`                             // рекомендованая цена
	ValutaPriceRecommend    string         `bson:"valutaPriceRecommend,omitempty" json:"valutaPriceRecommend,omitempty" msgpack:"valutaPriceRecommend,omitempty"` //валюта рекомендованой цены
	PriceOld                float64        `bson:"priceOld,omitempty" json:"priceOld,omitempty" msgpack:"priceOld,omitempty"`                                     //старая цена
	PriceAkciya             float64        `bson:"priceAcviya,omitempty" json:"priceAkciya,omitempty" msgpack:"priceAkciya,omitempty"`                            // цена с акционной скидкой
	PricesKonkur            bson.M         `bson:"pricesKonkur,omitempty" json:"pricesKonkur,omitempty" msgpack:"pricesKonkur,omitempty"`                         // цены конкурентов
	PriceMinYaMarket        float64        `bson:"priceMinYaMarket,omitempty" json:"priceMinYaMarket,omitempty" msgpack:"priceMinYaMarket,omitempty"`             // минимальная цена маркета
	PriceMaxYaMarket        float64        `bson:"priceMaxYaMarket,omitempty" json:"priceMaxYaMarket,omitempty" msgpack:"priceMaxYaMarket,omitempty"`             // максимальная цена маркета
	PriceAvgYaMarket        float64        `bson:"priceAvgYaMarket,omitempty" json:"priceAvgYaMarket,omitempty" msgpack:"priceAvgYaMarket,omitempty"`             // средня цена маркета
	Sklad                   float64        `bson:"sklad,omitempty" json:"sklad,omitempty" msgpack:"sklad,omitempty"`                                              //остаток на складе
	MarzhaMax               float64        `bson:"marzhaMax,omitempty" json:"marzhaMax,omitempty" msgpack:"marzhaMax,omitempty"`
	MarzhaMin               float64        `bson:"marzhaMin,omitempty" json:"marzhaMin,omitempty" msgpack:"marzhaMin,omitempty"`
	Marzha                  float64        `bson:"marzha,omitempty" json:"marzha,omitempty" msgpack:"marzha,omitempty"`
	Available               bool           `bson:"available" json:"available,omitempty" msgpack:"available,omitempty"`                         // доступность
	AvailableYaMarket       bool           `bson:"availableYaMarket" json:"availableYaMarket,omitempty" msgpack:"availableYaMarket,omitempty"` //Выгружать на маркет TRUE
	AvailableAvito          bool           `bson:"availableAvito" json:"availableAvito,omitempty" msgpack:"availableAvito,omitempty"`          //Выгружать на авито TRUE
	Aciya                   bool           `bson:"aciya" json:"aciya,omitempty" msgpack:"aciya,omitempty"`                                     //Если это акция TRUE
	ImageUrl                string         `bson:"imageUrl,omitempty" json:"imageUrl,omitempty" msgpack:"-"`
	ImageUrlLocal           string         `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty" msgpack:"imageUrlLocal,omitempty"` //ссылка на изображение на сервере
	ImageBitrix             string         `bson:"imageBitrix,omitempty" json:"imageBitrix,omitempty" msgpack:"-"`
	Image                   *Image         `bson:"image,omitempty" json:"image,omitempty" msgpack:"image,omitempty"`
	ImagesMore              []string       `bson:"imagesmore,omitempty" json:"imagesMore,omitempty" msgpack:"-"`                                   // ссылка на изображения
	ImagesMoreLocal         []string       `bson:"imagesMoreLocal,omitempty" json:"imagesMoreLocal,omitempty" msgpack:"imagesMoreLocal,omitempty"` // ссылка на изображения локальные
	Images                  []Image        `bson:"images,omitempty" json:"images,omitempty" msgpack:"images,omitempty"`
	Video                   string         `bson:"video,omitempty" json:"video,omitempty" msgpack:"video,omitempty"`
	Videos                  []string       `bson:"videos,omitempty" json:"videos,omitempty" msgpack:"videos,omitempty"`
	Players                 []string       `bson:"players,omitempty" json:"players,omitempty" msgpack:"players,omitempty"` //ссылки на плееры 3D визуализации
	Documents               []string       `bson:"documents,omitempty" json:"documents,omitempty" msgpack:"documents,omitempty"`
	DocumentsDoc            []Document     `bson:"documentsDoc,omitempty" json:"documentsDoc,omitempty" msgpack:"documentsDoc,omitempty"` //новая версия
	Otzyvy                  []string       `bson:"otzyvy,omitempty" json:"otzyvy,omitempty" msgpack:"otzyvy,omitempty"`
	ParsSiteId              string         `bson:"parsSiteId,omitempty" json:"parsSiteId,omitempty" msgpack:"parsSiteId,omitempty"`                                        //обозначение сайта донора
	Svoystva                bson.M         `bson:"Svoystva,omitempty" json:"svoystva,omitempty" msgpack:"-"`                                                               //необработанные свойсва
	SvoystvaMnozhestvo      bson.M         `bson:"svoystvaMnozhestvo,omitempty" json:"svoystvaMnozhestvo,omitempty" msgpack:"-"`                                           //необработанные свойсва со значениями в массиве
	SvoystvaSiteFilter      bson.M         `bson:"svoystvaSiteFilter,omitempty" json:"svoystvaSiteFilter,omitempty" msgpack:"svoystvaSiteFilter,omitempty"`                //свойсва для фильтра сайта
	SvoystvaSiteHaract      bson.M         `bson:"svoystvaSiteHaract,omitempty" json:"svoystvaSiteHaract,omitempty" msgpack:"svoystvaSiteHaract,omitempty"`                //свойства для характеристик карточки товара сайта
	SootvetstviePostavschik bson.M         `bson:"sootvetstviePostavschik,omitempty" json:"sootvetstviePostavschik,omitempty" msgpack:"sootvetstviePostavschik,omitempty"` //карта где имя поставщика: значение из файла для склейки  Устарел
	Sootvetstviya           []Sootvetstvie `bson:"sootvetstviya,omitempty" json:"sootvetstviya,omitempty" msgpack:"sootvetstviya,omitempty"`
	IsActiveForSite         bool           `json:"isActiveForSite,omitempty" msgpack:"isActiveForSite,omitempty"`                                              //Отображается на сайте
	Status                  string         `bson:"status,omitempty" json:"status,omitempty" msgpack:"status,omitempty"`                                        //Активный, Нет категории, Снято с производства, Под заказ
	SostavKomplekta         []string       `bson:"sostavkomplekta,omitempty" json:"sostavKomplekta,omitempty" msgpack:"sostavKomplekta,omitempty"`             // Состав комплекта массив айдишников
	Modifications           []Modification `bson:"modifications,omitempty" json:"modifications,omitempty" msgpack:"modifications,omitempty"`                   //модификации товара
	SostavKomplektaDop      []string       `bson:"sostavkomplektaDop,omitempty" json:"sostavKomplektaDop,omitempty" msgpack:"sostavKomplektaDop,omitempty"`    // состав комплекта опциональные
	VariantyIspolneniya     []string       `bson:"variantyIspolneniya,omitempty" json:"variantyIspolneniya,omitempty" msgpack:"variantyIspolneniya,omitempty"` // варианты исполнения (зелены, с ящиком, итд) https://i.imgur.com/uQWkd2k.png
	BidMarket               float64        `bson:"bidMarket,omitempty" json:"bidMarket,omitempty" msgpack:"bidMarket,omitempty"`                               //ставка для яндекс маркета

	TipTovara    string `bson:"tipTovara,omitempty" json:"tipTovara,omitempty" msgpack:"tipTovara,omitempty"`          //основной, запчасти, аксессуары
	Komplektnost string `bson:"komplektnost,omitempty" json:"komplektnost,omitempty" msgpack:"komplektnost,omitempty"` //Товар-комплект, Продается только в комплекте, Продается только отдельно, Продается в комплекте и отдельно,

	PricePostavkaOpt  []PriceInfo `bson:"pricePostavkaOpt,omitempty" json:"pricePostavkaOpt,omitempty" msgpack:"pricePostavkaOpt,omitempty"`
	PricePostavkaRozn []PriceInfo `bson:"pricePostavkaRozn,omitempty" json:"pricePostavkaRozn,omitempty" msgpack:"pricePostavkaRozn,omitempty"`
	SkladPostavka     []SkladInfo `bson:"skladPostavka,omitempty" json:"skladPostavka,omitempty" msgpack:"skladPostavka,omitempty"`
	UpdatedTime       *time.Time  `bson:"updatedTime,omitempty" json:"updatedTime,omitempty" msgpack:"updatedTime,omitempty"` //время когда был обновлен
	CreatedTime       *time.Time  `bson:"createdTime,omitempty" json:"createdTime,omitempty" msgpack:"createdTime,omitempty"` //время когда был создан
	SortOrder         int         `bson:"sortOrder,omitempty" json:"sortOrder,omitempty" msgpack:"sortOrder,omitempty"`       //порядок сортировки для сайта
	InCart            bool        `bson:"inCart" json:"inCart,omitempty" msgpack:"inCart,omitempty"`                          //есть ли товар в корзине
	IsFavorite        bool        `bson:"isFavorite" json:"isFavorite,omitempty" msgpack:"isFavorite,omitempty"`              //есть ли товар в избранном
	SEO               *SEO        `bson:"seo,omitempty" json:"seo,omitempty" msgpack:"seo,omitempty"`

	RelatedProjectIds []string `bson:"relatedProjectIds,omitempty" json:"relatedProjectIds,omitempty" msgpack:"relatedProjectIds,omitempty"` //проекты с которыми связан данный товар
}

type Modification struct {
	IdHex          string    `bson:"idHex,omitempty" json:"idHex,omitempty" msgpack:"idHex,omitempty"` //id товара в hex формате
	Name           string    `bson:"name,omitempty" json:"name,omitempty" msgpack:"name,omitempty"`
	NameTranslit   string    `bson:"nameTranslit,omitempty" json:"nameTranslit,omitempty" msgpack:"nameTranslit,omitempty"` //название в урле
	IdVnutr        string    `bson:"idVnutr,omitempty" json:"idVnutr,omitempty" msgpack:"idVnutr,omitempty"`                //внутренний адйшиник сайта донора
	Svoystva       bson.M    `bson:"Svoystva,omitempty" json:"svoystva,omitempty" msgpack:"svoystva,omitempty"`
	Price          float64   `bson:"price,omitempty" json:"price,omitempty" msgpack:"price,omitempty"`
	UpdatedPriceAt time.Time `bson:"updatedPriceAt,omitempty" json:"updatedPriceAt,omitempty" msgpack:"updatedPriceAt,omitempty"` //время когда была обновлена цена
	KursPrice      float64   `bson:"kursPrice,omitempty" json:"kursPrice,omitempty" msgpack:"kursPrice,omitempty"`                //курс валюты поставщика
	ValutaPrice    string    `bson:"valutaPrice,omitempty" json:"valutaPrice",omitempty" msgpack:"valutaPrice,omitempty"`         //валюта цены которая идет на сайт
	Image          *Image    `bson:"image,omitempty" json:"image,omitempty" msgpack:"image,omitempty"`
}

type Document struct {
	Name          string `bson:"name,omitempty" json:"name,omitempty" msgpack:"name,omitempty"`
	LocalFile     string `bson:"localFile,omitempty" json:"localFile,omitempty" msgpack:"localFile,omitempty"` //неполный путь для сайта
	FileLocalPath string `bson:"fileLocalPath,omitempty" json:"fileLocalPath,omitempty"`                       //полный локальный путь
	FileName      string `bson:"fileName,omitempty" json:"fileName,omitempty" msgpack:"fileName,omitempty"`
	FileSize      uint64 `bson:"fileSize,omitempty" json:"fileSize,omitempty" msgpack:"fileSize,omitempty"`
}

type SEO struct {
	Title       string `bson:"title,omitempty" json:"title,omitempty" msgpack:"title,omitempty"`
	Description string `bson:"description,omitempty" json:"description,omitempty" msgpack:"description,omitempty"`
	H1          string `bson:"h1,omitempty" json:"h1,omitempty" msgpack:"h1,omitempty"`
}

type Category struct {
	Name          string     `bson:"name,omitempty" json:"name,omitempty" msgpack:"name,omitempty"`
	URL           string     `bson:"url,omitempty" json:"url,omitempty" msgpack:"url,omitempty"`
	ImageUrlLocal string     `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty" msgpack:"imageUrlLocal,omitempty"`
	IsActive      bool       `json:"isActive,omitempty" msgpack:"isActive,omitempty"`                                    //для сайта индикация активной категории, не пишется в базу
	IsRoot        bool       `json:"isRoot,omitempty" msgpack:"isRoot,omitempty"`                                        //для сайта признак корневой директории
	GalleryId     string     `bson:"galleryId,omitempty" json:"galleryId,omitempty" msgpack:"galleryId,omitempty"`       //айдишник галлереи связанной с этой категорией
	SortOrder     int        `bson:"sortOrder,omitempty" json:"sortOrder,omitempty" msgpack:"sortOrder,omitempty"`       //порядок сортировки для сайта
	UpdatedTime   *time.Time `bson:"updatedTime,omitempty" json:"updatedTime,omitempty" msgpack:"updatedTime,omitempty"` //время когда был обновлен
}

type Sootvetstvie struct {
	Type               string `bson:"type,omitempty" json:"type,omitempty" msgpack:"type,omitempty"`                                           //импорт || счет
	NameTranslitPostav string `bson:"nameTranslitPostav,omitempty" json:"nameTranslitPostav,omitempty" msgpack:"nameTranslitPostav,omitempty"` //транслит имя поставщика
	Value              string `bson:"value,omitempty" json:"value,omitempty" msgpack:"value,omitempty"`                                        //значение
	Field              string `bson:"field,omitempty" json:"field,omitempty" msgpack:"field,omitempty"`                                        //название || артикул постав || имяибренд || артикул завода
}

type PriceInfo struct {
	NameTranslitPostav string     `bson:"nameTranslitPostav,omitempty" json:"nameTranslitPostav,omitempty" msgpack:"nameTranslitPostav,omitempty"`
	Value              float64    `bson:"value,omitempty" json:"value,omitempty" msgpack:"value,omitempty"`
	Valuta             string     `bson:"valuta,omitempty" json:"valuta,omitempty" msgpack:"valuta,omitempty"`
	UpdatedTime        *time.Time `bson:"updatedTime,omitempty" json:"updatedTime,omitempty" msgpack:"updatedTime,omitempty"`
}

type SkladInfo struct {
	NameTranslitPostav string     `bson:"nameTranslitPostav,omitempty" json:"nameTranslitPostav,omitempty" msgpack:"nameTranslitPostav,omitempty"`
	Value              float64    `bson:"value,omitempty" json:"value,omitempty" msgpack:"value,omitempty"`
	EdinicaIzmereniya  string     `bson:"edinicaIzmereniya,omitempty" json:"edinicaIzmereniya,omitempty" msgpack:"edinicaIzmereniya,omitempty"`
	UpdatedTime        *time.Time `bson:"updatedTime,omitempty" json:"updatedTime,omitempty" msgpack:"updatedTime,omitempty"`
}

package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Priemka struct {
	ID   primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Cart *Cart              `json:"cart,omitempty" bson:"cart,omitempty"`

	UPDNumber            string    `json:"updNumber,omitempty" bson:"updNumber,omitempty"`     //номер отгрузочного документа УПД
	DatePriemka          string    `json:"datePriemka,omitempty" bson:"datePriemka,omitempty"` //дата приемки
	DatePriemkaFormatted time.Time `json:"datePriemkaFormatted,omitempty" bson:"datePriemkaFormatted,omitempty"`

	PostavNameTranslit  string         `json:"postavNameTranslit,omitempty" bson:"postavNameTranslit,omitempty"`
	PostavRekvizity     *Rekvizity     `json:"postavRekvizity,omitempty" bson:"postavRekvizity,omitempty"`
	PostavBankRekvizity *BankRekvizity `json:"postavBankRekvizity,omitempty" bson:"postavBankRekvizity,omitempty"`

	PaymentId string `json:"paymentId,omitempty" bson:"paymentId,omitempty"` //id платежа
	BillId    string `json:"billId,omitempty" bson:"billId,omitempty"`       //id счета
	NomerBill string `json:"nomerBill,omitempty" bson:"nomerBill,omitempty"`
	OrderId   string `json:"orderId,omitempty" bson:"orderId,omitempty"`   //id заказа
	OrderNum  int    `json:"orderNum,omitempty" bson:"orderNum,omitempty"` //номер заказа
	//SkladId   string `json:"skladId,omitempty" bson:"skladId,omitempty"`     //id склада
	SkladNameTranslit string `json:"skladNameTranslit,omitempty" bson:"skladNameTranslit,omitempty"`
	ManagerId         string `json:"managerId,omitempty" bson:"managerId,omitempty"` //id менеджера 

	StatusCurrent string   `json:"statusCurrent" bson:"statusCurrent"`           //текущий статус
	Statuses      []Status `json:"statuses,omitempty" bson:"statuses,omitempty"` //статусы заказа

	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"` //время когда был обновлен
	CreatedAt time.Time `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
	Comments  []Comment `json:"comments,omitempty" bson:"comments,omitempty"` //комментарии 
}

//type Sklad struct {
//ID           primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
//Name         string             `json:"name,omitempty" bson:"name,omitempty"`                 // обычное название для себя - required
//NameTranslit string             `bson:"nameTranslit,omitempty" json:"nameTranslit,omitempty"` //название в урле и для загрузки - uniq, required
//}

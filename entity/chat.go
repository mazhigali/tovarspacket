package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AccountChat struct {
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name            string             `json:"name" bson:"name"`
	AccessToken     string             `json:"access_token" bson:"access_token,omitempty"`
	Provider        string             `json:"provider" bson:"provider"` // whatsapp, telegram, etc.
	WhatsAppAccount *WhatsAppAccount   `json:"whatsapp_account" bson:"whatsapp_account,omitempty"`
	IsActive        bool               `json:"is_active" bson:"is_active"`
	CreatedAt       time.Time          `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
	UpdatedAt       time.Time          `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
}

// Структура для хранения данных об аккаунте в MongoDB
type WhatsAppAccount struct {
	PhoneNumber string `json:"phone_number" bson:"phone_number"` // Номер телефона
	JID         string `json:"jid" bson:"jid"`                   // JID - ID для WhatsApp
}

// Chat room
type Chat struct {
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	AccountIdHex    string             `bson:"accountIdHex,omitempty" json:"accountIdHex,omitempty"`
	AccountForSite  *AccountChat       `bson:"accountForSite,omitempty" json:"accountForSite,omitempty"`
	Provider        string             `json:"provider" bson:"provider"` // whatsapp, telegram, etc.
	IdChatProvider  string             `bson:"idChatProvider,omitempty" json:"idChatProvider,omitempty"`
	CustomerIdHex   string             `bson:"customerIdHex,omitempty" json:"customerIdHex,omitempty"`
	ContactIdHex    string             `bson:"contactIdHex,omitempty" json:"contactIdHex,omitempty"`
	ContactForSite  *Contact           `bson:"contactForSite,omitempty" json:"contactForSite,omitempty"`
	CustomerForSite *Customer          `bson:"customerForSite,omitempty" json:"customerForSite,omitempty"`
	UnreadMsgsCount int                `bson:"unreadMsgsCount" json:"unreadMsgsCount"` //количество непрочитанных сообщений
	CreatedAt       time.Time          `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
	UpdatedAt       time.Time          `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
}

// Chat message
type Message struct {
	ID                 primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	AccountIdHex       string             `bson:"accountIdHex,omitempty" json:"accountIdHex,omitempty"`
	ChatIdHex          string             `bson:"chatIdHex,omitempty" json:"chatIdHex,omitempty"`
	Provider           string             `json:"provider" bson:"provider"` // whatsapp, telegram, etc.
	IdMessageProvider  string             `bson:"idMessageProvider,omitempty" json:"idMessageProvider,omitempty"`
	SenderUserIdHex    string             `bson:"senderUserIdHex,omitempty" json:"senderUserIdHex,omitempty"`
	RecipientUserIdHex string             `bson:"recipientUserIdHex,omitempty" json:"recipientUserIdHex,omitempty"`
	Content            string             `bson:"content,omitempty" json:"content,omitempty"`
	SenderType         string             `bson:"senderType,omitempty" json:"senderType,omitempty"` //manager, client
	TypeMsg            string             `bson:"typeMsg,omitempty" json:"typeMsg,omitempty"`       //text
	IsRead             bool               `bson:"isRead" json:"isRead"`
	Document           *Document          `bson:"document,omitempty" json:"document,omitempty"`
	Image              *Image             `bson:"image,omitempty" json:"image,omitempty"`
	Voice              *Voice             `bson:"voice,omitempty" json:"voice,omitempty"`
	ReadAt             time.Time          `bson:"readAt,omitempty" json:"readAt,omitempty"`
	ReadTime           time.Time          `bson:"readTime,omitempty" json:"readTime,omitempty"`
	CreatedAt          time.Time          `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
	UpdatedAt          time.Time          `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
}

type Voice struct {
	Name          string `bson:"name,omitempty" json:"name,omitempty" msgpack:"name,omitempty"`
	LocalFile     string `bson:"localFile,omitempty" json:"localFile,omitempty" msgpack:"localFile,omitempty"` //неполный путь для сайта
	FileLocalPath string `bson:"fileLocalPath,omitempty" json:"fileLocalPath,omitempty"`                       //полный локальный путь
	FileName      string `bson:"fileName,omitempty" json:"fileName,omitempty" msgpack:"fileName,omitempty"`
	FileSize      uint64 `bson:"fileSize,omitempty" json:"fileSize,omitempty" msgpack:"fileSize,omitempty"`
}

package entity

import "go.mongodb.org/mongo-driver/bson"

// для таблицы свойств
type Svoystvo struct {
	Name                  string              `bson:"name"`                                                     //имя свойства для поиска в базе товаров
	NameForSite           string              `bson:"nameForSite"`                                              //имя свойства для сайта
	NameTranslitForSite   string              `bson:"nameTranslitForSite" json:"nameTranslitForSite,omitempty"` //имя свойства в транслите в урле фильтра
	IsInFilterSite        bool                `bson:"inFilterSite"`                                             //должно ли учавствовать в фильтре  сайта
	IsActive              bool                `bson:"isActive"`                                                 //если ТРУ то будет отображаться в карточке товара сайта
	Znacheniya            []ZnachenieSvoystva `bson:"znacheniya,omitempty"`                                     //Значения свойств в формате структуры TODO
	ZnacheniyaValues      []interface{}       `bson:"znacheniyaValues,omitempty"`                               //массив всех возможных значений свойства из базы товаров
	ZnacheniyaMapTranslit bson.M              `bson:"znacheniyaMapTranslit,omitempty"`                          //карта значений свойств с транслитом для сайта: [серый]=seriy
	IsZnachenieFloat      bool                `bson:"isZnachenieFloat"`                                         //является ли тип значения float. По дефолту все значения string
	IsZnachenieArray      bool                `bson:"isZnachenieArray"`                                         //является ли значение массивом
	SortOrderInFilter     int                 `bson:"sortOrderInFilter,omitempty"`                              //порядок сортировки в фильтре

	SinonimsName []string `bson:"sinonimsName,omitempty"` //синонимы названия свойства для конвертации из других баз

	FloatValueMin float64 `bson:"floatValueMin"` //Для фильтра сайта
	FloatValueMax float64 `bson:"floatValueMax"` //Для фильтра сайта
}

type ZnachenieSvoystva struct {
	SvoystvoIdHex string      `bson:"svoystvoIdHex,omitempty"`
	Value         interface{} `bson:"value"`
	ValueTranslit string      `bson:"valueTranslit"`
	//Sinonims       []string `bson:"sinonims,omitempty"`
	//SinonimDefault string   `bson:"sinonimDefault,omitempty"`
	//Active         bool     `bson:"active"`
	SortOrderInFilter int `bson:"sortOrderInFilter,omitempty"` //порядок сортировки в фильтре

	Count int `bson:"count,omitempty"` //количество найденных документов по этому свойству

	UrlInFilter string
}

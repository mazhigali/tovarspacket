package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Payment struct {
	Id             primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	StatusCurrent  string             `json:"statusCurrent,omitempty" bson:"statusCurrent,omitempty"` //новый | подтвержденный | утвержденный
	Statuses       []Status           `json:"statuses,omitempty" bson:"statuses,omitempty"`           //статусы платежей
	Description    string             `json:"description,omitempty" bson:"description,omitempty"`     // может удалить
	Summa          float64            `json:"summa" bson:"summa"`
	Direction      string             `json:"direction,omitempty" bson:"direction,omitempty"`           //приход || расход
	SredstvoOplaty string             `json:"sredstvoOplaty,omitempty" bson:"sredstvoOplaty,omitempty"` //нал || безнал || перевод на карту || терминал
	Type           string             `json:"type,omitempty" bson:"type,omitempty"`                     //оплата покупателя || возврат покупателя || расчет с поставщиком || 

	KoshelekId string      `json:"koshelekId,omitempty" bson:"koshelekId,omitempty"`
	Contragent *Contragent `json:"contragent,omitempty" bson:"contragent,omitempty"`
	Payer      *Contragent `json:"payer,omitempty" bson:"payer,omitempty"`
	Receiver   *Contragent `json:"receiver,omitempty" bson:"receiver,omitempty"`

	ManagerId       string `json:"managerId,omitempty" bson:"managerId,omitempty"` //id менеджера
	BillId          string `json:"billId,omitempty" bson:"billId,omitempty"`       //id счета
	NomerBill       string `json:"nomerBill,omitempty" bson:"nomerBill,omitempty"` //номер счета //hz может тоже удалим
	OrderId         string `json:"orderId,omitempty" bson:"orderId,omitempty"`     //id заказа
	OrderNum        int    `json:"orderNum,omitempty" bson:"orderNum,omitempty"`   //номер заказа
	NomerChekaOrder string `json:"nomerChekaOrder,omitempty" bson:"nomerChekaOrder,omitempty"`

	CreatedAt time.Time `json:"createdAt,omitempty" bson:"createdAt,omitempty"` //время создания
	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"` //время когда был обновлен

	OperationId string `json:"operationId,omitempty" bson:"operationId,omitempty"` //id операции в клиент-банке
}

type Contragent struct {
	ContragentId  string         `json:"contragentId,omitempty" bson:"contragentId,omitempty"` //id контрагента в коллекции организаций или покупателей
	BankRekvizity *BankRekvizity `json:"bankRekvizity,omitempty" bson:"bankRekvizity,omitempty"`
	Name          string         `json:"name,omitempty" bson:"name,omitempty"`
	INN           string         `json:"inn,omitempty" bson:"inn,omitempty"`
	YurType       string         `json:"yurType,omitempty" bson:"yurType,omitempty"` //физлицо || организация -- в зависимости от типа будет зависеть в какой таблице искать
	CustomerId    string         `json:"customerId,omitempty" bson:"customerId,omitempty"`
}

type Bill struct {
	Id            primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	IdProvider    string             `json:"idProvider,omitempty" bson:"idProvider,omitempty"` // айдишник в системе банка
	NomerBill     string             `json:"nomerBill,omitempty" bson:"nomerBill,omitempty"`
	StatusCurrent string             `json:"statusCurrent,omitempty" bson:"statusCurrent,omitempty"` //подтвержденный | не подтвержденный | оплачен | не оплачен
	Statuses      []Status           `json:"statuses,omitempty" bson:"statuses,omitempty"`           //статусы платежей
	Description   string             `json:"description,omitempty" bson:"description,omitempty"`
	Summa         float64            `json:"summa" bson:"summa"`
	Direction     string             `json:"direction,omitempty" bson:"direction,omitempty"` //приход || расход
	KoshelekId    string             `json:"koshelekId,omitempty" bson:"koshelekId,omitempty"`
	Contragent    *Contragent        `json:"contragent,omitempty" bson:"contragent,omitempty"`

	PostavNameTranslit      string         `json:"postavNameTranslit,omitempty" bson:"postavNameTranslit,omitempty"`
	PostavRekvizity         *Rekvizity     `json:"postavRekvizity,omitempty" bson:"postavRekvizity,omitempty"`
	PostavBankRekvizity     *BankRekvizity `json:"postavBankRekvizity,omitempty" bson:"postavBankRekvizity,omitempty"`
	PostavDateBill          string         `json:"postavDateBill,omitempty" bson:"postavDateBill,omitempty"`                   //дата счета у поставщика
	PostavDateBillFormatted time.Time      `json:"postavDateBillFormatted,omitempty" bson:"postavDateBillFormatted,omitempty"` //дата счета у поставщика
	DatePayed               time.Time      `json:"datePayed,omitempty" bson:"datePayed,omitempty"`                             //время оплаты

	PayUrl string `json:"payUrl,omitempty" bson:"payUrl,omitempty"` //ссылка на оплату

	ManagerId string `json:"managerId,omitempty" bson:"managerId,omitempty"` //id менеджера
	PaymentId string `json:"paymentId,omitempty" bson:"paymentId,omitempty"` //id платежа
	OrderId   string `json:"orderId,omitempty" bson:"orderId,omitempty"`     //id заказа
	OrderNum  int    `json:"orderNum,omitempty" bson:"orderNum,omitempty"`   //номер заказа

	FileLocalLink string `json:"fileLocalLink,omitempty" bson:"fileLocalLink,omitempty"` //ссылка на файл в локальном хранилище

	CreatedAt time.Time `json:"createdAt,omitempty" bson:"createdAt,omitempty"` //время создания в системе
	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"` //время когда был обновлен

}

// Кошелек
type Koshelek struct {
	ID             primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name           string             `json:"name,omitempty" bson:"name,omitempty"`
	NameTranslit   string             `json:"nameTranslit,omitempty" bson:"nameTranslit,omitempty"` //uniq
	Balans         float64            `json:"balans,omitempty" bson:"balans,omitempty"`
	Description    string             `json:"description,omitempty" bson:"description,omitempty"`
	BankRekvizity  *BankRekvizity     `json:"bankRekvizity,omitempty" bson:"bankRekvizity,omitempty"`
	SredstvaOplaty []string           `json:"sredstvaOplaty,omitempty" bson:"sredstvaOplaty,omitempty"` //нал || безнал || перевод на карту || терминал

	CreatedAt time.Time `json:"createdAt,omitempty" bson:"createdAt,omitempty"` //время создания
	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"` //время когда был обновлен
}

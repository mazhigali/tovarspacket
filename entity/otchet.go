package entity

type OtchetByPostavchik struct {
	PostavschikName string          `json:"postavschikName"`
	OtchetsByBrands []OtchetByBrand `json:"otchetsByBrands"`
	Brands          []string        `json:"brands"`
}

type OtchetByBrand struct {
	BrandName         string             `json:"brandName"`
	OtchetsByCategory []OtchetByCategory `json:"otchetsByCategory"`
}

type OtchetByCategory struct {
	CategoryName  string    `json:"categoryName"` //Унитазы
	Segments      []Segment `json:"segments"`
	KolTovarov    int       `json:"kolTovarov"`
	AvgOptPrice   float64   `json:"avgOptPrice"`
	AvgRoznPrice  float64   `json:"avgRoznPrice"`
	AvgMarzhaProc float64   `json:"avgMarzhaProc"`
	AvgMarzha     float64   `json:"avgMarzha"`
}

type Segment struct {
	NameSegment    string   `json:"nameSegment"`    //имя сегманта 0-2850
	CategoryName   string   `json:"categoryName"`   //Унитазы
	PriceOt        float64  `json:"priceOt"`        //0
	PriceDo        float64  `json:"priceDo"`        //3000
	ProcentPokupok float64  `json:"procentPokupok"` //20
	KolTovarov     int      `json:"kolTovarov"`
	AvgOptPrice    float64  `json:"avgOptPrice"`
	AvgRoznPrice   float64  `json:"avgRoznPrice"`
	AvgMarzha      float64  `json:"avgMarzha"`
	AvgMarzhaProc  float64  `json:"avgMarzhaProc"`
	Brands         []string `json:"brands"`
}

var UnitazOtchet = OtchetByCategory{
	CategoryName: "Унитазы", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 2850.00, ProcentPokupok: 20.00},
		Segment{PriceOt: 2850.00, PriceDo: 5000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 5000.00, PriceDo: 10000.00, ProcentPokupok: 20.00},
		Segment{PriceOt: 10000.00, PriceDo: 18000.00, ProcentPokupok: 22.00},
		Segment{PriceOt: 18000.00, PriceDo: 45000.00, ProcentPokupok: 10.00},
		Segment{PriceOt: 45000.00, PriceDo: 120000.00, ProcentPokupok: 3.00},
	}}

var RakovinyOtchet = OtchetByCategory{
	CategoryName: "Раковины", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 2500.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 2500.00, PriceDo: 3500.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 3500.00, PriceDo: 5000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 5000.00, PriceDo: 7000.00, ProcentPokupok: 30.00},
		Segment{PriceOt: 7000.00, PriceDo: 12000.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 12000.00, PriceDo: 20000.00, ProcentPokupok: 10.00},
		Segment{PriceOt: 20000.00, PriceDo: 40000.00, ProcentPokupok: 5.00},
	}}

var ChugunVannaOtchet = OtchetByCategory{
	CategoryName: "Чугунные ванны", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 10000.00, ProcentPokupok: 20.00},
		Segment{PriceOt: 10000.00, PriceDo: 20000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 20000.00, PriceDo: 45000.00, ProcentPokupok: 35.00},
		Segment{PriceOt: 45000.00, PriceDo: 100000.00, ProcentPokupok: 20.00},
	}}

var StalVannaOtchet = OtchetByCategory{
	CategoryName: "Стальные ванны", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 5000.00, ProcentPokupok: 30.00},
		Segment{PriceOt: 5000.00, PriceDo: 10000.00, ProcentPokupok: 30.00},
		Segment{PriceOt: 10000.00, PriceDo: 20000.00, ProcentPokupok: 35.00},
		Segment{PriceOt: 20000.00, PriceDo: 30000.00, ProcentPokupok: 5.00},
	}}

var AkrilVannaOtchet = OtchetByCategory{
	CategoryName: "Акриловые ванны", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 3500.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 3500.00, PriceDo: 5000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 5000.00, PriceDo: 15000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 15000.00, PriceDo: 35000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 35000.00, PriceDo: 70000.00, ProcentPokupok: 10.00},
	}}

var IskusstvVannaOtchet = OtchetByCategory{
	CategoryName: "Ванны из искусственного камня", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 20000.00, ProcentPokupok: 50.00},
		Segment{PriceOt: 20000.00, PriceDo: 50000.00, ProcentPokupok: 35.00},
		Segment{PriceOt: 50000.00, PriceDo: 100000.00, ProcentPokupok: 15.00},
	}}

var MebelOtchet = OtchetByCategory{
	CategoryName: "Мебель для ванной комнаты", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 15000.00, ProcentPokupok: 30.00},
		Segment{PriceOt: 15000.00, PriceDo: 40000.00, ProcentPokupok: 40.00},
		Segment{PriceOt: 40000.00, PriceDo: 50000.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 50000.00, PriceDo: 60000.00, ProcentPokupok: 10.00},
		Segment{PriceOt: 60000.00, PriceDo: 120000.00, ProcentPokupok: 5.00},
	}}

var SmesiteliOtchet = OtchetByCategory{
	CategoryName: "Смесители", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 2000.00, ProcentPokupok: 50.00},
		Segment{PriceOt: 2000.00, PriceDo: 10000.00, ProcentPokupok: 30.00},
		Segment{PriceOt: 10000.00, PriceDo: 45000.00, ProcentPokupok: 20.00},
	}}

var DushOtchet = OtchetByCategory{
	CategoryName: "Душевые уголки, ограждения и поддоны", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 4000.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 4000.00, PriceDo: 7000.00, ProcentPokupok: 10.00},
		Segment{PriceOt: 7000.00, PriceDo: 12000.00, ProcentPokupok: 10.00},
		Segment{PriceOt: 12000.00, PriceDo: 30000.00, ProcentPokupok: 40.00},
		Segment{PriceOt: 30000.00, PriceDo: 40000.00, ProcentPokupok: 20.00},
		Segment{PriceOt: 40000.00, PriceDo: 100000.00, ProcentPokupok: 5.00},
	}}

var BideOtchet = OtchetByCategory{
	CategoryName: "Биде", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 5000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 5000.00, PriceDo: 15000.00, ProcentPokupok: 50.00},
		Segment{PriceOt: 15000.00, PriceDo: 35000.00, ProcentPokupok: 25.00},
	}}

var InstallaciiOtchet = OtchetByCategory{
	CategoryName: "Инсталляции", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 10000.00, ProcentPokupok: 20.00},
		Segment{PriceOt: 10000.00, PriceDo: 20000.00, ProcentPokupok: 40.00},
		Segment{PriceOt: 20000.00, PriceDo: 40000.00, ProcentPokupok: 25.00},
		Segment{PriceOt: 40000.00, PriceDo: 80000.00, ProcentPokupok: 15.00},
	}}

var KuhonMoykiOtchet = OtchetByCategory{
	CategoryName: "Мойки для кухни", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 3500.00, ProcentPokupok: 30.00},
		Segment{PriceOt: 3500.00, PriceDo: 6000.00, ProcentPokupok: 40.00},
		Segment{PriceOt: 6000.00, PriceDo: 15000.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 15000.00, PriceDo: 20000.00, ProcentPokupok: 10.00},
		Segment{PriceOt: 20000.00, PriceDo: 30000.00, ProcentPokupok: 5.00},
	}}

var SifonyOtchet = OtchetByCategory{
	CategoryName: "Сифоны", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 500.00, ProcentPokupok: 45.00},
		Segment{PriceOt: 500.00, PriceDo: 1000.00, ProcentPokupok: 35.00},
		Segment{PriceOt: 1000.00, PriceDo: 3000.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 3000.00, PriceDo: 100000.00, ProcentPokupok: 5.00},
	}}

var AksessuaryVannoyOtchet = OtchetByCategory{
	CategoryName: "Аксессуары для ванной комнаты и туалета", Segments: []Segment{
		Segment{PriceOt: 0.00, PriceDo: 500.00, ProcentPokupok: 45.00},
		Segment{PriceOt: 500.00, PriceDo: 1000.00, ProcentPokupok: 35.00},
		Segment{PriceOt: 1000.00, PriceDo: 3000.00, ProcentPokupok: 15.00},
		Segment{PriceOt: 3000.00, PriceDo: 100000.00, ProcentPokupok: 5.00},
	}}

var OtchetCategories = []OtchetByCategory{UnitazOtchet, RakovinyOtchet, ChugunVannaOtchet, StalVannaOtchet, AkrilVannaOtchet, IskusstvVannaOtchet, MebelOtchet, SmesiteliOtchet, DushOtchet, BideOtchet, InstallaciiOtchet, KuhonMoykiOtchet, SifonyOtchet, AksessuaryVannoyOtchet}

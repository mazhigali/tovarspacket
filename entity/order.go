package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ItemCart struct {
	//ID        primitive.ObjectID        `json:"id,omitempty" bson:"_id,omitempty"`
	Product          Tovar        `json:"product,omitempty" bson:"product,omitempty" mapstructure:"-"`
	IdProduct        string       `json:"idProduct" bson:"idProduct" mapstructure:"idProduct"`                    // ИД ТОВАРА в общей базе 
	IdVnutr          string       `bson:"idVnutr,omitempty" json:"idVnutr,omitempty" msgpack:"idVnutr,omitempty"` //Uniq внутренний адйшиник сквозной между базами
	Quantity         float64      `json:"quantity" bson:"quantity" mapstructure:"quantity"`
	Price            float64      `json:"price" bson:"price" mapstructure:"price"`
	Total            float64      `json:"total" bson:"total" mapstructure:"total"`                                                      //итоговая сумма без скидки
	SummaSoSkidkoy   float64      `json:"summaSoSkidkoy,omitempty" bson:"summaSoSkidkoy,omitempty" mapstructure:"summaSoSkidkoy"`       //итоговая сумма со скидкой
	Sootvetstvie     Sootvetstvie `json:"sootvetstvie,omitempty" bson:"sootvetstvie,omitempty" mapstructure:"sootvetstvie"`             //соответствие с поставщиком
	PricePostavkaOpt float64      `json:"pricePostavkaOpt,omitempty" bson:"pricePostavkaOpt,omitempty" mapstructure:"pricePostavkaOpt"` //цена поставки опт
	SummaPostavkiOpt float64      `json:"summaPostavkiOpt,omitempty" bson:"summaPostavkiOpt,omitempty" mapstructure:"summaPostavkiOpt"` //итоговая сумма поставки опт
	Pribyl           float64      `json:"pribyl,omitempty" bson:"pribyl,omitempty" mapstructure:"pribyl"`                               //прибыль это разницы между выручкой и суммой поставок
	ZpManagera       float64      `json:"zpManagera,omitempty" bson:"zpManagera,omitempty" mapstructure:"zpManagera"`                   //зарплата менеджера
	Rentabelnost     float64      `json:"rentabelnost,omitempty" bson:"rentabelnost,omitempty" mapstructure:"rentabelnost"`             //рентабельность
}

type Cart struct {
	//ID         primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Items         []ItemCart `json:"items" bson:"items" mapstructure:"items"`
	TotalItems    int        `json:"totalItems" bson:"totalItems" mapstructure:"totalItems"` // количество позиций в корзине
	Total         float64    `json:"total" bson:"total" mapstructure:"total"`                //общая сумма в корзине
	SummakOplate  float64    `json:"summakOplate,omitempty" bson:"summakOplate,omitempty"`   //сумма к оплате уже со скдикой
	SummaPostavki float64    `json:"summaPostavki,omitempty" bson:"summaPostavki,omitempty"` //итоговая сумма к оплате поставщикам

	Pribyl       float64   `json:"pribyl,omitempty" bson:"pribyl,omitempty" mapstructure:"pribyl"`                   //прибыль это разницы между выручкой и суммой поставок
	ZpManagera   float64   `json:"zpManagera,omitempty" bson:"zpManagera,omitempty" mapstructure:"zpManagera"`       //зарплата менеджера
	Rentabelnost float64   `json:"rentabelnost,omitempty" bson:"rentabelnost,omitempty" mapstructure:"rentabelnost"` //рентабельность
	UpdatedAt    time.Time `bson:"updatedTime,omitempty" json:"updatedTime,omitempty"`                               //время когда был обновлен
}

type Order struct {
	ID         primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	NumOrder   int                `json:"numOrder,omitempty" bson:"numOrder,omitempty"` //уникальный номер заказа
	NomerCheka string             `json:"nomerCheka,omitempty" bson:"nomerCheka,omitempty"`
	Cart       *Cart              `json:"cart,omitempty" bson:"cart,omitempty"`
	Text       string             `json:"text,omitempty" bson:"text,omitempty"` //первоначальный запрос клиента или комментарий к заказу

	Dostavka       Delivery `json:"dostavka,omitempty" bson:"dostavka,omitempty"`
	StatusDostavka string   `json:"statusDostavka,omitempty" bson:"statusDostavka,omitempty"` //ожидает отправки, в пути, доставлен

	PaymentIds    []string `json:"paymentIds,omitempty" bson:"paymentIds,omitempty"`       //id платежей
	StatusPayment string   `json:"statusPayment,omitempty" bson:"statusPayment,omitempty"` //оплачен, ожидает оплаты и т. д.
	BillsIds      []string `json:"billsIds,omitempty" bson:"billsIds,omitempty"`

	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"` //время когда был обновлен
	CreatedAt time.Time `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
	Comments  []Comment `json:"comments,omitempty" bson:"comments,omitempty"` //комментарии менеджера к сделке

	CustomerId      string    `json:"customerId,omitempty" bson:"customerId,omitempty"`           //id покупателя
	CustomerForSite *Customer `json:"customerForSite,omitempty" bson:"customerForSite,omitempty"` //покупатель для сайта

	ManagerId       string `json:"managerId,omitempty" bson:"managerId,omitempty"`             //id менеджера кто ведет текущую сделку
	ManagerSellerId string `json:"managerSellerId,omitempty" bson:"managerSellerId,omitempty"` //id менеджера кто продал
	ManagerForSite  *User  `json:"managerForSite,omitempty" bson:"managerForSite,omitempty"`   //менеджер для сайта

	StatusCurrent string   `json:"statusCurrent" bson:"statusCurrent"`           //текущий статус
	Statuses      []Status `json:"statuses,omitempty" bson:"statuses,omitempty"` //статусы заказа

	StatusBuhgalter string `json:"statusBuhgalter,omitempty" bson:"statusBuhgalter,omitempty"` //TODO возможно удалить.

	IstochnikLida string `json:"istochnikLida,omitempty" bson:"istochnikLida,omitempty"` //с магаза, с сайта, с карта

	OrganizationSellerNameTransit string `json:"organizationSellerNameTransit,omitempty" bson:"organizationSellerNameTransit,omitempty"`

	TehInfo *TehInfo `json:"tehInfo,omitempty" bson:"tehInfo,omitempty"`
}

type Status struct {
	Name string `json:"name,omitempty" bson:"name,omitempty"` //Поле для отслеживания текущего статуса заказа (например, новый, обрабатывается, отправлен, доставлен и т. д.).

	UpdatedAt time.Time `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
	ManagerId string    `json:"managerId,omitempty" bson:"managerId,omitempty"`
}

type Delivery struct {
	Method        string    `json:"method,omitempty"`  // метод доставки (курьер, самовывоз)
	Address       string    `json:"address,omitempty"` // адрес доставки
	Date          string    `json:"date,omitempty"`    // дата доставки
	DateFormatted time.Time `json:"dateFormatted,omitempty" bson:"dateFormatted"`
	//Comments     []Comment `json:"comments,omitempty" bson:"comments,omitempty"`
	VoditelOrgNameTranslit string `json:"voditelOrgNameTranslit,omitempty" bson:"voditelOrgNameTranslit,omitempty"`
	Car                    *Car   `json:"car,omitempty" bson:"car,omitempty"`
}

type Comment struct {
	//ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Text      string    `json:"text,omitempty"`
	Status    string    `json:"status,omitempty"`
	UpdatedAt time.Time `bson:"updatedAt" json:"updatedAt,omitempty"`           //время когда был обновлен
	ManagerId string    `json:"managerId,omitempty" bson:"managerId,omitempty"` //id менеджера кто внес изменения
}

type FormMessage struct {
	ID             primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Fio            string             `json:"fio,omitempty"`
	Email          string             `json:"email,omitempty"`
	Telephone      string             `json:"telephone,omitempty"`
	Telegram       string             `json:"telegram,omitempty"`
	Whatsapp       string             `json:"whatsapp,omitempty"`
	Text           string             `json:"text,omitempty"`
	PartnerIdVnutr string             `json:"partnerIdVnutr,omitempty"`                 //ссылка на партнера кому пишут
	UpdatedAt      time.Time          `bson:"updatedTime" json:"updatedTime,omitempty"` //время когда был обновлен
}

type Favorite struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Products  []Tovar            `json:"products,omitempty" bson:"products,omitempty"`
	UpdatedAt time.Time          `bson:"updatedTime,omitempty" json:"updatedTime,omitempty"` //время когда был обновлен
}

type TehInfo struct {
	IP        string `json:"ip,omitempty"`
	UserAgent string `json:"userAgent,omitempty"`
}

package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Partner struct {
	Id primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	//IDString     string             `bson:"idString" json:"idString,omitempty"`
	Name         string `bson:"name" json:"name,omitempty"`
	NameTranslit string `bson:"nameTranslit" json:"nameTranslit,omitempty"` //название в урле
	IdVnutr      string `bson:"idVnutr,omitempty" json:"idVnutr,omitempty"` //айдишник внутренний
	//URL           string             `bson:"url" json:"url,omitempty"`
	VidDeyatelnosti string   `bson:"vidDeyatelnosti,omitempty" json:"vidDeyatelnosti,omitempty"` //design or building
	RegionId        int      `bson:"regionId,omitempty" json:"regionId,omitempty"`
	Region          Region   `bson:"region,omitempty" json:"region,omitempty"`
	Email           string   `bson:"email" json:"email,omitempty"`
	Telegram        string   `bson:"telegram" json:"telegram,omitempty"`
	Opisanie        string   `bson:"opisanie,omitempty" json:"opisanie,omitempty"`
	OpisanieShort   string   `bson:"opisanieShort,omitempty" json:"opisanieShort,omitempty"`
	ImageUrl        string   `bson:"imageUrl,omitempty" json:"imageUrl,omitempty"`
	ImageUrlLocal   string   `bson:"imageUrlLocal,omitempty" json:"imageUrlLocal,omitempty"` //ссылка на изображение аватара на сервере
	ImagesMore      []string `bson:"imagesmore,omitempty" json:"imagesMore,omitempty"`       // ссылка на изображения локальные
	Images          []Image  `bson:"images,omitempty" json:"images,omitempty"`               //работы
	IsActiveForSite bool     `json:"isActiveForSite"`                                        //Отображается на сайте

	Title       string    `bson:"title,omitempty" json:"title,omitempty"`
	Description string    `bson:"description,omitempty" json:"description,omitempty"`
	UpdatedTime time.Time `bson:"updatedTime" json:"updatedTime,omitempty"` //время когда был обновлен
}

package payment

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindPaymentById(id string) (payment entity.Payment, err error) {
	id = strings.TrimSpace(id)
	if id == "" {
		return payment, entity.ErrInputValueNil
	}

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}
	filter := bson.M{"_id": objectId}
	err = c.collection.FindOne(context.TODO(), filter).Decode(&payment)
	return
}

func (c *MongoConnection) FindPaymentsByQuery(query bson.M) (payments []entity.Payment, err error) {

	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return
	}
	if err = cursor.All(context.TODO(), &payments); err != nil {
		return
	}
	return
}

func (c *MongoConnection) FindPaymentByQuery(query bson.M) (payment entity.Payment, err error) {
	err = c.collection.FindOne(context.TODO(), query).Decode(&payment)
	return
}

func (c *MongoConnection) UpdatePayment(payment *entity.Payment) (err error) {
	query := bson.M{"_id": payment.Id}
	update := bson.M{"$set": payment}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) UpdatePaymentByOperationID(payment *entity.Payment) (err error) {
	query := bson.M{"operationId": payment.OperationId}
	update := bson.M{"$set": payment}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemovePayment(id string) (err error) {
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectId})
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) FindBillById(id string) (bill entity.Bill, err error) {
	id = strings.TrimSpace(id)
	if id == "" {
		return bill, entity.ErrInputValueNil
	}

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	filter := bson.M{"_id": objectId}

	err = c.collection.FindOne(context.TODO(), filter).Decode(&bill)
	return
}

func (c *MongoConnection) FindBillsByQuery(query bson.M) (bills []entity.Bill, err error) {
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return
	}
	if err = cursor.All(context.TODO(), &bills); err != nil {
		return
	}

	return
}

func (c *MongoConnection) FindBillByQuery(query bson.M) (bill entity.Bill, err error) {
	err = c.collection.FindOne(context.TODO(), query).Decode(&bill)
	return
}

func (c *MongoConnection) UpdateBill(bill *entity.Bill) (err error) {
	query := bson.M{"_id": bill.Id}
	update := bson.M{"$set": bill}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}

	return nil
}

func (c *MongoConnection) RemoveBill(id string) (err error) {
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectId})
	if err != nil {
		return err
	}

	return nil
}

func (c *MongoConnection) FindKoshelekByQuery(query bson.M) (koshelek entity.Koshelek, err error) {
	err = c.collection.FindOne(context.TODO(), query).Decode(&koshelek)
	return
}

func (c *MongoConnection) FindKosheleksByQuery(query bson.M) (kosheleks []entity.Koshelek, err error) {

	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return
	}
	if err = cursor.All(context.TODO(), &kosheleks); err != nil {
		return
	}
	return
}

func (c *MongoConnection) UpdateKoshelek(wallet *entity.Koshelek) (err error) {

	query := bson.M{"nameTranslit": wallet.NameTranslit}
	update := bson.M{"$set": wallet}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}

	return nil
}

func (c *MongoConnection) RemoveKoshelek(id string) (err error) {
	id = strings.TrimSpace(id)
	if id == "" {
		return entity.ErrInputValueNil
	}
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}

	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectId})

	if err != nil {
		return err
	}

	return nil
}

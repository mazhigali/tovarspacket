package payment

import (
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindPaymentById(id string) (entity.Payment, error) {
	return s.repo.FindPaymentById(id)
}

func (s *Service) FindPaymentsByQuery(query bson.M) ([]entity.Payment, error) {
	return s.repo.FindPaymentsByQuery(query)
}

func (s *Service) FindPaymentByQuery(query bson.M) (entity.Payment, error) {
	return s.repo.FindPaymentByQuery(query)
}

func (s *Service) UpdatePayment(payment *entity.Payment) error {
	payment.UpdatedAt = time.Now()
	if payment.Id == primitive.NilObjectID {
		payment.Id = primitive.NewObjectID()
		payment.CreatedAt = time.Now()
	}
	return s.repo.UpdatePayment(payment)
}

func (s *Service) UpdatePaymentByOperationID(payment *entity.Payment) error {
	return s.repo.UpdatePaymentByOperationID(payment)
}

func (s *Service) RemovePayment(id string) error {
	return s.repo.RemovePayment(id)
}

func (s *Service) FindBillById(id string) (entity.Bill, error) {
	return s.repo.FindBillById(id)
}

func (s *Service) FindBillsByQuery(query bson.M) ([]entity.Bill, error) {
	return s.repo.FindBillsByQuery(query)
}

func (s *Service) FindBillByQuery(query bson.M) (entity.Bill, error) {
	return s.repo.FindBillByQuery(query)
}

func (s *Service) UpdateBill(bill *entity.Bill) error {
	bill.UpdatedAt = time.Now()
	if bill.Id == primitive.NilObjectID {
		bill.Id = primitive.NewObjectID()
		bill.CreatedAt = time.Now()
	}
	return s.repo.UpdateBill(bill)
}

func (s *Service) RemoveBill(id string) error {
	return s.repo.RemoveBill(id)
}

func (s *Service) FindKoshelekByQuery(query bson.M) (entity.Koshelek, error) {
	return s.repo.FindKoshelekByQuery(query)
}

func (s *Service) FindKosheleksByQuery(query bson.M) ([]entity.Koshelek, error) {
	return s.repo.FindKosheleksByQuery(query)
}

func (s *Service) UpdateKoshelek(wallet *entity.Koshelek) error {
	if wallet.NameTranslit == "" {
		return entity.ErrInputValueNil
	}
	wallet.UpdatedAt = time.Now()
	if wallet.ID == primitive.NilObjectID {
		walletDb, err := s.repo.FindKoshelekByQuery(bson.M{"nameTranslit": wallet.NameTranslit})
		if err != nil {
			switch err {
			case mongo.ErrNoDocuments:
				walletDb.CreatedAt = time.Now()
				walletDb.ID = primitive.NewObjectID()
				return s.repo.UpdateKoshelek(wallet)
			default:
				return err
			}
		}
		wallet.ID = walletDb.ID
	}
	return s.repo.UpdateKoshelek(wallet)
}

func (s *Service) RemoveKoshelek(id string) error {
	return s.repo.RemoveKoshelek(id)
}

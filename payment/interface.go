package payment

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()

	FindPaymentById(id string) (entity.Payment, error)
	FindPaymentByQuery(query bson.M) (entity.Payment, error)
	FindPaymentsByQuery(query bson.M) ([]entity.Payment, error)

	FindBillById(id string) (entity.Bill, error)
	FindBillByQuery(query bson.M) (entity.Bill, error)
	FindBillsByQuery(query bson.M) ([]entity.Bill, error)

	FindKoshelekByQuery(query bson.M) (entity.Koshelek, error)
	FindKosheleksByQuery(query bson.M) ([]entity.Koshelek, error)
}

// Writer interface
type Writer interface {
	UpdatePayment(payment *entity.Payment) error
	UpdatePaymentByOperationID(payment *entity.Payment) error
	RemovePayment(id string) error

	UpdateBill(bill *entity.Bill) error
	RemoveBill(id string) error

	UpdateKoshelek(wallet *entity.Koshelek) error
	RemoveKoshelek(id string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

package order

import (
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindById(id string) (*entity.Order, error) {
	return s.repo.FindById(id)
}

func (s *Service) FindByIdProduct(idProduct string) (*entity.Order, error) {
	return s.repo.FindByIdProduct(idProduct)
}

func (s *Service) FindAll() ([]entity.Order, error) {
	return s.repo.FindAll()
}
func (s *Service) FindOrdersByQuery(query bson.M) ([]entity.Order, error) {
	return s.repo.FindOrdersByQuery(query)
}

func (s *Service) FindFavoriteById(id string) (*entity.Favorite, error) {
	return s.repo.FindFavoriteById(id)
}

func (s *Service) UpdateOrder(order *entity.Order) error {
	order.UpdatedAt = time.Now()

	if order.ID == primitive.NilObjectID {
		order.ID = primitive.NewObjectID()
		order.CreatedAt = time.Now()
	}

	return s.repo.UpdateOrder(order)
}

func (s *Service) RemoveOrder(id string) error {
	return s.repo.RemoveOrder(id)
}

func (s *Service) UpdateFormMessage(formMessage *entity.FormMessage) error {
	if formMessage.Telephone == "" {
		return entity.ErrInputValueNil
	}

	formMessage.UpdatedAt = time.Now()

	if formMessage.ID.IsZero() == true {
		formMessage.ID = primitive.NewObjectID()
	}

	return s.repo.UpdateFormMessage(formMessage)
}

func (s *Service) UpdateFavorite(favorite *entity.Favorite) error {
	favorite.UpdatedAt = time.Now()
	if favorite.ID.IsZero() == true {
		favorite.ID = primitive.NewObjectID()
	}

	return s.repo.UpdateFavorite(favorite)
}

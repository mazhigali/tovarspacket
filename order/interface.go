package order

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByIdProduct(idProduct string) (*entity.Order, error)

	FindById(id string) (*entity.Order, error)

	FindOrdersByQuery(query bson.M) ([]entity.Order, error)
	FindAll() ([]entity.Order, error)

	FindFavoriteById(id string) (*entity.Favorite, error)
}

// Writer bookmark writer
type Writer interface {
	UpdateOrder(order *entity.Order) error
	UpdateFormMessage(formMessage *entity.FormMessage) error
	UpdateFavorite(favorite *entity.Favorite) error
	RemoveOrder(id string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

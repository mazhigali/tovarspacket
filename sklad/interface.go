package sklad

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()

	FindPriemkaById(id string) (entity.Priemka, error)
	FindPriemkaByQuery(query bson.M) (entity.Priemka, error)
	FindPriemkasByQuery(query bson.M) ([]entity.Priemka, error)
}

// Writer interface
type Writer interface {
	UpdatePriemka(priemka *entity.Priemka) error
	RemovePriemka(id string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

package sklad

import (
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindPriemkaById(id string) (entity.Priemka, error) {
	return s.repo.FindPriemkaById(id)
}

func (s *Service) FindPriemkaByQuery(query bson.M) (entity.Priemka, error) {
	return s.repo.FindPriemkaByQuery(query)
}

func (s *Service) FindPriemkasByQuery(query bson.M) ([]entity.Priemka, error) {
	return s.repo.FindPriemkasByQuery(query)
}

func (s *Service) UpdatePriemka(priemka *entity.Priemka) error {
	priemka.UpdatedAt = time.Now()
	if priemka.ID == primitive.NilObjectID {
		priemka.ID = primitive.NewObjectID()
		priemka.CreatedAt = time.Now()
	}
	return s.repo.UpdatePriemka(priemka)
}

func (s *Service) RemovePriemka(id string) error {
	return s.repo.RemovePriemka(id)
}

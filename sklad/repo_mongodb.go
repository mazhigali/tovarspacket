package sklad

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindPriemkaById(id string) (priemka entity.Priemka, err error) {
	id = strings.TrimSpace(id)
	if id == "" {
		return priemka, entity.ErrInputValueNil
	}
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectId}).Decode(&priemka)
	return
}

func (c *MongoConnection) FindPriemkaByQuery(query bson.M) (priemka entity.Priemka, err error) {
	err = c.collection.FindOne(context.TODO(), query).Decode(&priemka)
	return
}

func (c *MongoConnection) FindPriemkasByQuery(query bson.M) (priemkas []entity.Priemka, err error) {
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return
	}
	err = cursor.All(context.TODO(), &priemkas)
	return
}

func (c *MongoConnection) UpdatePriemka(priemka *entity.Priemka) (err error) {
	query := bson.M{"_id": priemka.ID}
	update := bson.M{"$set": priemka}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemovePriemka(id string) (err error) {
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectId})
	return
}

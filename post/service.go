package post

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindById(id string) (*entity.Post, error) {
	return s.repo.FindById(id)
}

func (s *Service) FindByName(name string) (*entity.Post, error) {
	return s.repo.FindByName(name)
}

func (s *Service) FindByNameTranslit(name string) (*entity.Post, error) {
	return s.repo.FindByNameTranslit(name)
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.Post, error) {
	return s.repo.FindByQuery(query)
}

func (s *Service) FindAll() ([]entity.Post, error) {
	return s.repo.FindAll()
}

func (s *Service) Update(post *entity.Post) error {
	if post.Id == primitive.NilObjectID {
		post.Id = primitive.NewObjectID()
	}

	return s.repo.Update(post)
}

func (s *Service) RemoveByName(name string) error {
	return s.repo.RemoveByName(name)
}

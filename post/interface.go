package post

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindById(id string) (*entity.Post, error)
	FindByName(name string) (*entity.Post, error)
	FindByNameTranslit(name string) (*entity.Post, error)

	FindByQuery(query bson.M) ([]entity.Post, error)
	FindAll() ([]entity.Post, error)
}

// Writer bookmark writer
type Writer interface {
	Update(post *entity.Post) error
	RemoveByName(name string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

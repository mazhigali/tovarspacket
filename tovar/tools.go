package tovar

import (
	"crypto/sha1"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"time"

	apiYaMarket "gitlab.com/mazhigali/apiyamarket"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/mazhigali/tovarspacket/entity"
)

func (s *Service) UpdatePricesFromYaMarket(YaMarketToken, YaMarketClientId string) error {
	//cApi := apiYaMarket.NewClient(config.YaMarketToken, config.YaMarketClientId)
	cApi := apiYaMarket.NewClient(YaMarketToken, YaMarketClientId)
	defer cApi.Close()
	//offersWithModel := getOfferWithModel(cApi)
	campaigns, err := cApi.ListCampaigns()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(campaigns)
	offersWithModel := []apiYaMarket.Offer{}
	offers, err := cApi.GetOffersAll(campaigns[0].ID)
	//println(len(offers))
	for _, offer := range offers {
		if offer.ModelId > 0 {
			offersWithModel = append(offersWithModel, offer)
		}
	}
	fmt.Println("Offers with MODEL in YM", len(offersWithModel))
	if len(offersWithModel) == 0 {
		return entity.ErrNoDataFromMarket
	}

	m := make(map[uint64]string)
	modelsIds := []uint64{}
	for _, offer := range offersWithModel {
		modelsIds = append(modelsIds, uint64(offer.ModelId))
		m[uint64(offer.ModelId)] = offer.Id
	}
	//fmt.Println(c.GetModels(modelsIds))
	modelsWithPrices, err := cApi.GetModels(modelsIds)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Models with PRICES in YM", len(modelsWithPrices))
	if len(modelsWithPrices) == 0 {
		return entity.ErrNoDataFromMarket
	}

	for _, modelId := range modelsWithPrices {
		err = s.repo.SetYaMarketPrices(m[modelId.Id], modelId.Prices.Min, modelId.Prices.Max, modelId.Prices.Avg)
		if err != nil {
			//fmt.Println("ERROR Update Prices", err.Error())
			return err
		}
	}
	return err
}

func (s *Service) ExportAllWithContent2XmlEnergo(proizvoditel string) error {
	// создаем файл экспорта
	filename := "allWithContent.xml"
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)
	tovarsWithId := []entity.Tovar{}
	if proizvoditel == "" {
		tovarsWithId, err = s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "model": bson.M{"$exists": true}, "proizvoditel": bson.M{"$exists": true}})
	} else {
		tovarsWithId, err = s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "model": bson.M{"$exists": true}, "proizvoditel": proizvoditel})
	}
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT ExportAllWithContent2XmlEnergo", len(tovarsWithId))
	count := 0

	fmt.Fprintln(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
	fmt.Fprintln(file, "<offers>")

	for _, tovar := range tovarsWithId {
		//ПИШЕМ В ФАЙЛ данные
		fmt.Fprintf(file, "<offer name='%s'>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <Articul>%s</Articul>\n", r.Replace(tovar.Articul))
		fmt.Fprintf(file, "    <idMon>%s</idMon>\n", tovar.Id.Hex())
		fmt.Fprintf(file, "    <Categories>%s</Categories>\n", r.Replace(strings.Join(tovar.Categories, "|")))
		fmt.Fprintf(file, "    <Proizvoditel>%s</Proizvoditel>\n", r.Replace(tovar.Proizvoditel))
		fmt.Fprintf(file, "    <StranaIzgotovitel>%s</StranaIzgotovitel>\n", r.Replace(tovar.StranaIzgotovitel))
		fmt.Fprintf(file, "    <IdVnutr>%s</IdVnutr>\n", tovar.IdVnutr)
		fmt.Fprintf(file, "    <Seriya>%s</Seriya>\n", r.Replace(tovar.Seriya))
		fmt.Fprintf(file, "    <Model>%s</Model>\n", r.Replace(tovar.Model))
		if tovar.KursPrice > 0.00 {
			priceRub := tovar.Price * tovar.KursPrice
			fmt.Fprintf(file, "    <Price>%f</Price>\n", priceRub)
			fmt.Fprintf(file, "    <Valuta>%s</Valuta>\n", "RUB")
		} else {
			fmt.Fprintf(file, "    <Price>%f</Price>\n", tovar.Price)
			fmt.Fprintf(file, "    <Valuta>%s</Valuta>\n", tovar.ValutaPrice)
		}
		fmt.Fprintf(file, "    <ImageUrlLocal>%s</ImageUrlLocal>\n", tovar.ImageUrlLocal)
		fmt.Fprintf(file, "    <ImagesMore>%s</ImagesMore>\n", strings.Join(tovar.ImagesMore, "|"))
		fmt.Fprintf(file, "    <ParsSiteId>%s</ParsSiteId>\n", tovar.ParsSiteId)
		fmt.Fprintf(file, "    <Videos>%s</Videos>\n", r.Replace(strings.Join(tovar.Videos, "|")))
		docs := r.Replace(strings.Join(tovar.Documents, "|"))
		fmt.Fprintf(file, "    <Documents>%s</Documents>\n", strings.Replace(docs, "dogovor-na-to-gk-energoprof.doc", "dogovor-na-to-energoteka.doc", 1))
		fmt.Fprintf(file, "    <SostavKomplekta>%s</SostavKomplekta>\n", strings.Join(tovar.SostavKomplekta, "|"))
		fmt.Fprintf(file, "    <SostavKomplektaDop>%s</SostavKomplektaDop>\n", strings.Join(tovar.SostavKomplektaDop, "|"))
		fmt.Fprintf(file, "    <BidMarket>%f</BidMarket>\n", tovar.BidMarket)
		fmt.Fprintf(file, "    <Komplektnost>%s</Komplektnost>\n", tovar.Komplektnost)
		fmt.Fprintf(file, "    <AvailableYaMarket>%t</AvailableYaMarket>\n", tovar.AvailableYaMarket)

		//анмаршалим свойства формата bson
		var flattened = make(map[string]interface{})
		flatten(tovar.Svoystva, "", &flattened)
		svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		//анмаршалим свойства формата bson

		for _, header := range svoystvaHeaders {
			if header == "Мощность стабилизатора" {
				znachenie := fmt.Sprint(flattened[header])
				fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", "Мощность", r.Replace(znachenie))
			} else {
				znachenie := fmt.Sprint(flattened[header])
				fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
			}
		}

		//анмаршалим множественные свойства формата bson
		flatten(tovar.SvoystvaMnozhestvo, "", &flattened)
		svoystvaHeaders = getHeadersFromBson(tovar.SvoystvaMnozhestvo)

		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			//fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, strings.Join(znachenie, "|"))
			fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
		}

		var moschnost string
		moschnost = fmt.Sprint(flattened["Мощность"])
		_, is := flattened["Мощность"]
		//если мощности нету, то будем записывать в мощность номинальную мощность
		if is == false {
			_, is = flattened["Мощность номинальная"]
			if is == false {
				fmt.Fprintf(file, "    <param name=\"Мощность\">%s</param>\n", r.Replace(fmt.Sprint(flattened["Мощность максимальная"])))
				moschnost = fmt.Sprint(flattened["Мощность максимальная"])
			} else {
				fmt.Fprintf(file, "    <param name=\"Мощность\">%s</param>\n", r.Replace(fmt.Sprint(flattened["Мощность номинальная"])))
				moschnost = fmt.Sprint(flattened["Мощность номинальная"])
			}
		}

		_, is = flattened["Длина"]
		if is == true {
			fmt.Fprintf(file, "    <param name=\"Габариты (ДхШхВ), (мм)\">%sx%sx%s</param>\n", r.Replace(fmt.Sprint(flattened["Длина"])), r.Replace(fmt.Sprint(flattened["Ширина"])), r.Replace(fmt.Sprint(flattened["Высота"])))
		} else {
			_, is = flattened["Габариты, мм"]
			if is == true {
				fmt.Fprintf(file, "    <param name=\"Габариты (ДхШхВ), (мм)\">%s</param>\n", r.Replace(fmt.Sprint(flattened["Габариты, мм"])))
			}
		}

		if len(tovar.Categories) > 0 {
			if tovar.Categories[0] != "Стабилизаторы напряжения" {
				//DESCRIPTION
				fmt.Fprintf(file, "    <Description><![CDATA[")

				fmt.Fprintf(file, "    <p>Приобрести %s можно с доставкой – свяжитесь с менеджером и обговорите стоимость услуги.</p>", tovar.Name)
				if moschnost != "" && tovar.Svoystva["Топливо"] != nil {
					fmt.Fprintf(file, "    <p>Электростанция мощностью %s кВт работает на топливе - %s</p>", moschnost, tovar.Svoystva["Топливо"].(string))
				}
				fmt.Fprintf(file, "    <p>Модель генератора %s имеет напряжение на выходе %s Вольт и исполнение %s</p>", tovar.Model, flattened["Напряжение"], flattened["Исполнение"])
				fmt.Fprintf(file, "    ]]></Description>\n")
				//DESCRIPTION
			}
		}

		fmt.Fprintln(file, "</offer>")

		count++
		//}
	}

	fmt.Fprintln(file, "</offers>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

func (s *Service) ExportAllWithContent2XmlRUSC(seriiService *Service) error {
	// создаем файл экспорта
	filename := "allWithContent.xml"
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	//_, tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "proizvoditel": "Kerama Marazzi", "pricePostavkaRozn": bson.M{"$exists": true}}) //FOR ALL
	tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}}) //FOR ALL
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT ExportAllWithContent2XmlRUSC", len(tovarsWithId))
	count := 0

	fmt.Fprintln(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
	fmt.Fprintln(file, "<offers>")

	for _, tovar := range tovarsWithId {
		//if tovar.Komplektnost != "Комплект СО" && tovar.Komplektnost != "Комплект одним товаром СО" && tovar.Sklad > 0.00 {
		//ПИШЕМ В ФАЙЛ данные
		fmt.Fprintf(file, "<offer name='%s'>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <Articul>%s</Articul>\n", r.Replace(tovar.Articul))
		fmt.Fprintf(file, "    <idMon>%s</idMon>\n", tovar.Id.Hex())
		fmt.Fprintf(file, "    <Categories>%s</Categories>\n", r.Replace(strings.Join(tovar.Categories, "|")))
		fmt.Fprintf(file, "    <Proizvoditel>%s</Proizvoditel>\n", r.Replace(tovar.Proizvoditel))
		fmt.Fprintf(file, "    <StranaIzgotovitel>%s</StranaIzgotovitel>\n", r.Replace(tovar.StranaIzgotovitel))
		fmt.Fprintf(file, "    <IdVnutr>%s</IdVnutr>\n", tovar.IdVnutr)
		fmt.Fprintf(file, "    <Seriya>%s</Seriya>\n", r.Replace(tovar.Seriya))
		fmt.Fprintf(file, "    <Model>%s</Model>\n", r.Replace(tovar.Model))
		fmt.Fprintf(file, "    <Price>%f</Price>\n", tovar.Price)
		fmt.Fprintf(file, "    <Valuta>%s</Valuta>\n", tovar.ValutaPrice)
		fmt.Fprintf(file, "    <ImageUrlLocal>%s</ImageUrlLocal>\n", tovar.ImageUrlLocal)
		fmt.Fprintf(file, "    <ImagesMore>%s</ImagesMore>\n", strings.Join(tovar.ImagesMore, "|"))
		fmt.Fprintf(file, "    <ParsSiteId>%s</ParsSiteId>\n", tovar.ParsSiteId)
		fmt.Fprintf(file, "    <Videos>%s</Videos>\n", r.Replace(strings.Join(tovar.Videos, "|")))
		fmt.Fprintf(file, "    <Documents>%s</Documents>\n", r.Replace(strings.Join(tovar.Documents, "|")))
		fmt.Fprintf(file, "    <SostavKomplekta>%s</SostavKomplekta>\n", strings.Join(tovar.SostavKomplekta, "|"))
		fmt.Fprintf(file, "    <SostavKomplektaDop>%s</SostavKomplektaDop>\n", strings.Join(tovar.SostavKomplektaDop, "|"))
		fmt.Fprintf(file, "    <BidMarket>%f</BidMarket>\n", tovar.BidMarket)
		fmt.Fprintf(file, "    <Komplektnost>%s</Komplektnost>\n", tovar.Komplektnost)
		fmt.Fprintf(file, "    <AvailableYaMarket>%t</AvailableYaMarket>\n", tovar.AvailableYaMarket)
		fmt.Fprintf(file, "    <EdinicaIzmereniya>%s</EdinicaIzmereniya>\n", tovar.EdinicaIzmereniya)
		fmt.Fprintf(file, "    <Aciya>%t</Aciya>\n", tovar.Aciya)
		if len(tovar.SootvetstviePostavschik) > 0 || tovar.Proizvoditel == "Иранская плитка" {
			//if tovar.Sklad > 0 {
			fmt.Fprintf(file, "    <Zakaz>N</Zakaz>\n")
		} else {
			fmt.Fprintf(file, "    <Zakaz>Y</Zakaz>\n")
		}

		fmt.Fprintf(file, "    <Description><![CDATA[")
		if tovar.Categories[0] == "Керамическая плитка" {
			fmt.Fprintf(file, "    <p>Керамическая %s бренда %s</p>", tovar.Name, tovar.Proizvoditel)
		} else {
			fmt.Fprintf(file, "    <p>%s бренда %s</p>", tovar.Name, tovar.Proizvoditel)
		}
		fmt.Fprintf(file, "    <p>Все характеристики %s:</p>", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <ul>")
		//анмаршалим свойства формата bson
		var flattened = make(map[string]interface{})
		flatten(tovar.Svoystva, "", &flattened)
		svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
				fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
			}
		}
		fmt.Fprintf(file, "    </ul>")
		fmt.Fprintf(file, "    <p>Чтобы купить %s, бренда %s можно заказать его через корзину или по телефону +7(495)233-02-05.</p>", r.Replace(tovar.Name), r.Replace(tovar.Proizvoditel))
		fmt.Fprintf(file, "    ]]></Description>\n")

		//var flattened = make(map[string]interface{})
		//flatten(tovar.Svoystva, "", &flattened)

		//анмаршалим свойства формата bson
		//svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			if header != "Форма" {
				fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
			}
		}

		//анмаршалим множественные свойства формата bson
		flatten(tovar.SvoystvaMnozhestvo, "", &flattened)
		svoystvaHeaders = getHeadersFromBson(tovar.SvoystvaMnozhestvo)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			//fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, strings.Join(znachenie, "|"))
			fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
		}

		if len(tovar.Categories) == 1 {
			fmt.Fprintf(file, "    <Razdel1Name>%s</Razdel1Name>\n", r.Replace(tovar.Categories[0]))
			fmt.Fprintf(file, "    <Razdel1VneshKod>%x</Razdel1VneshKod>\n", sha1.Sum([]byte(r.Replace(tovar.Categories[0]))))
		} else {
			if tovar.Categories[0] != "Отделочные материалы" {
				fmt.Fprintf(file, "    <Razdel1Name>%s</Razdel1Name>\n", r.Replace(tovar.Categories[0]))
				fmt.Fprintf(file, "    <Razdel1VneshKod>%x</Razdel1VneshKod>\n", sha1.Sum([]byte(r.Replace(tovar.Categories[0]))))

				fmt.Fprintf(file, "    <Razdel2Name>%s</Razdel2Name>\n", r.Replace(tovar.Categories[1]))
				s := tovar.Categories[1] + tovar.Proizvoditel
				fmt.Fprintf(file, "    <Razdel2VneshKod>%x</Razdel2VneshKod>\n", sha1.Sum([]byte(s)))
			} else {
				fmt.Fprintf(file, "    <Razdel1Name>%s</Razdel1Name>\n", r.Replace(tovar.Categories[1]))
				fmt.Fprintf(file, "    <Razdel1VneshKod>%x</Razdel1VneshKod>\n", sha1.Sum([]byte(r.Replace(tovar.Categories[1]))))

				fmt.Fprintf(file, "    <Razdel2Name>%s</Razdel2Name>\n", r.Replace(tovar.Categories[2]))
				s := tovar.Categories[2] + tovar.Proizvoditel
				fmt.Fprintf(file, "    <Razdel2VneshKod>%x</Razdel2VneshKod>\n", sha1.Sum([]byte(s)))
			}
		}
		for _, header := range svoystvaHeaders {
			if header == "Коллекции" {
				znachenie := fmt.Sprint(flattened[header])
				seriiName := strings.Split(znachenie, "|")
				var seriiIds []string
				for _, seriyaName := range seriiName {
					if seriyaName != "" {
						s := seriyaName + tovar.Proizvoditel
						seriyaKod := fmt.Sprintf("%x", sha1.Sum([]byte(s)))
						seriiIds = append(seriiIds, seriyaKod)

						//IF sootv then update colletion value that availAvito true
						//if len(tovar.SootvetstviePostavschik) > 0 {
						if len(tovar.SootvetstviePostavschik) > 0 || tovar.Proizvoditel == "Иранская плитка" {
							ser, err := seriiService.FindByVnutrID(seriyaKod)
							if err != nil {
								fmt.Println(tovar.Name)
								fmt.Println(seriyaKod)
								fmt.Println(seriyaName + "-" + tovar.Proizvoditel)
								fmt.Println(err)
								//return err
							} else {
								ser.AvailableAvito = true
								err = seriiService.UpdateTovarByVnutrId(ser)
								if err != nil {
									return err
								}
							}
						}
					}
				}
				fmt.Fprintf(file, "    <SeriiIds>%s</SeriiIds>\n", strings.Join(seriiIds, "|"))
			}
		}

		fmt.Fprintln(file, "</offer>")

		count++
		//}
	}

	fmt.Fprintln(file, "</offers>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

// экспортирует в xml товары у которых есть ImageUrlLocal в указанное название файла
func (s *Service) ExportAllWithContent2Xml(filename string) error {
	// создаем файл экспорта
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	//_, tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "idVnutr": bson.M{"$exists": true}, "parsSiteId": "santehnika-online"})  //FOR LOROSA
	//_, tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "pricePostavkaOpt": bson.M{"$exists": true}})
	//_, tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "proizvoditel": "Lorosa"}) //FOR ALL
	//_, tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}}) //FOR ALL
	tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrl": bson.M{"$exists": true}}) //FOR ALL
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT ExportAllWithContent2Xml", len(tovarsWithId))
	count := 0

	fmt.Fprintln(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
	fmt.Fprintln(file, "<offers>")

	for _, tovar := range tovarsWithId {
		//if tovar.Komplektnost != "Комплект СО" && tovar.Komplektnost != "Комплект одним товаром СО" && tovar.Sklad > 0.00 {
		//ПИШЕМ В ФАЙЛ данные
		fmt.Fprintf(file, "<offer name='%s'>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <Articul>%s</Articul>\n", r.Replace(tovar.Articul))
		fmt.Fprintf(file, "    <idMon>%s</idMon>\n", tovar.Id.Hex())
		fmt.Fprintf(file, "    <Categories>%s</Categories>\n", r.Replace(strings.Join(tovar.Categories, "|")))
		fmt.Fprintf(file, "    <Proizvoditel>%s</Proizvoditel>\n", r.Replace(tovar.Proizvoditel))
		fmt.Fprintf(file, "    <StranaIzgotovitel>%s</StranaIzgotovitel>\n", r.Replace(tovar.StranaIzgotovitel))
		fmt.Fprintf(file, "    <IdVnutr>%s</IdVnutr>\n", tovar.IdVnutr)
		fmt.Fprintf(file, "    <Seriya>%s</Seriya>\n", r.Replace(tovar.Seriya))
		fmt.Fprintf(file, "    <Model>%s</Model>\n", r.Replace(tovar.Model))
		fmt.Fprintf(file, "    <Price>%f</Price>\n", tovar.Price)
		fmt.Fprintf(file, "    <Valuta>%s</Valuta>\n", tovar.ValutaPrice)
		fmt.Fprintf(file, "    <ImageUrlLocal>%s</ImageUrlLocal>\n", tovar.ImageUrlLocal)
		fmt.Fprintf(file, "    <ImageUrl>%s</ImageUrl>\n", tovar.ImageUrl)
		fmt.Fprintf(file, "    <ImagesMore>%s</ImagesMore>\n", strings.Join(tovar.ImagesMore, "|"))
		fmt.Fprintf(file, "    <ParsSiteId>%s</ParsSiteId>\n", tovar.ParsSiteId)
		fmt.Fprintf(file, "    <Videos>%s</Videos>\n", r.Replace(strings.Join(tovar.Videos, "|")))
		fmt.Fprintf(file, "    <Documents>%s</Documents>\n", r.Replace(strings.Join(tovar.Documents, "|")))
		fmt.Fprintf(file, "    <SostavKomplekta>%s</SostavKomplekta>\n", strings.Join(tovar.SostavKomplekta, "|"))
		fmt.Fprintf(file, "    <SostavKomplektaDop>%s</SostavKomplektaDop>\n", strings.Join(tovar.SostavKomplektaDop, "|"))
		fmt.Fprintf(file, "    <BidMarket>%f</BidMarket>\n", tovar.BidMarket)
		fmt.Fprintf(file, "    <Komplektnost>%s</Komplektnost>\n", tovar.Komplektnost)
		fmt.Fprintf(file, "    <AvailableYaMarket>%t</AvailableYaMarket>\n", tovar.AvailableYaMarket)
		fmt.Fprintf(file, "    <EdinicaIzmereniya>%s</EdinicaIzmereniya>\n", tovar.EdinicaIzmereniya)
		fmt.Fprintf(file, "    <Aciya>%t</Aciya>\n", tovar.Aciya)

		var flattened = make(map[string]interface{})
		flatten(tovar.Svoystva, "", &flattened)

		//анмаршалим свойства формата bson
		svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
		}

		//анмаршалим множественные свойства формата bson
		flatten(tovar.SvoystvaMnozhestvo, "", &flattened)
		svoystvaHeaders = getHeadersFromBson(tovar.SvoystvaMnozhestvo)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			//fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, strings.Join(znachenie, "|"))
			fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
		}

		fmt.Fprintln(file, "</offer>")

		count++
		//}
	}

	fmt.Fprintln(file, "</offers>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

// экспортирует в xml для турбо-страниц
func (s *Service) Export2XmlYaTurbo(nameMagaz, domainUrl string) ([]string, error) {
	result := []string{}

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	tovarsWithIds, err := s.repo.FindByQuery(bson.M{"urlBitrix": bson.M{"$exists": true}})
	if err != nil {
		return result, err
	}

	//GET MAP------------------------------------------------
	mapCategory := make(map[string]int)
	var num int
	for _, tovar1 := range tovarsWithIds {
		if len(tovar1.Categories) > 0 {
			_, found := mapCategory[tovar1.Categories[0]]
			if found == false {
				num++
				mapCategory[tovar1.Categories[0]] = num
			}
		}
	}
	//GET MAP------------------------------------------------
	tovars := split(tovarsWithIds, 10000)
	for n, tovarsWithId := range tovars {

		//TODO разбивать на части по 30тыс
		// создаем файл экспорта
		filename := "yaturbo" + fmt.Sprint(n) + ".xml"
		file, err := os.Create(filename)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		//-- создаем файл экспорта
		result = append(result, filename)

		nowTime := time.Now()
		fmt.Fprintln(file, "<yml_catalog date=\""+nowTime.Format("2006-01-02 15:04")+"\">")
		fmt.Fprintln(file, "<shop>")
		fmt.Fprintf(file, "<name>%s</name>\n", nameMagaz)
		fmt.Fprintf(file, "<currencies>\n<currency id=\"RUB\" rate=\"1\"/>\n</currencies>\n")

		fmt.Fprintln(file, "<categories>")
		for category, number := range mapCategory {
			fmt.Fprintf(file, "    <category id=\"%v\"> %s</category>\n", number, r.Replace(category))
		}
		fmt.Fprintln(file, "</categories>")
		fmt.Fprintln(file, "<offers>")

		count := 0
		for _, tovar := range tovarsWithId {
			// записываем в файл при условии что есть локальное изображение и нет определенный КОМплектов
			if len(tovar.ImageUrlLocal) > 0 && len(tovar.Categories) > 0 {

				fmt.Fprintf(file, "<offer id=\"%s\" available=\"true\">\n", tovar.Id.Hex())
				fmt.Fprintf(file, "    <name>%s</name>\n", r.Replace(tovar.Name))
				fmt.Fprintf(file, "    <url>%s</url>\n", r.Replace(tovar.UrlBitrix))
				fmt.Fprintf(file, "    <price>%f</price>\n", tovar.Price)
				fmt.Fprintf(file, "    <currencyId>RUB</currencyId>\n")
				fmt.Fprintf(file, "    <categoryId>%v</categoryId>\n", mapCategory[tovar.Categories[0]])
				fmt.Fprintf(file, "    <vendor>%s</vendor>\n", r.Replace(tovar.Proizvoditel))
				fmt.Fprintf(file, "    <vendorCode>%s</vendorCode>\n", r.Replace(tovar.Articul))
				fmt.Fprintf(file, "    <country_of_origin>%s</country_of_origin>\n", r.Replace(tovar.StranaIzgotovitel))
				picUrl := domainUrl + r.Replace(tovar.ImageUrlLocal)
				fmt.Fprintf(file, "    <picture>%s</picture>\n", picUrl)

				fmt.Fprintf(file, "    <description><![CDATA[")
				if tovar.Categories[0] == "Керамическая плитка" {
					fmt.Fprintf(file, "    <p>Керамическая %s бренда %s</p>", tovar.Name, tovar.Proizvoditel)
				} else {
					fmt.Fprintf(file, "    <p>%s бренда %s</p>", tovar.Name, tovar.Proizvoditel)
				}
				fmt.Fprintf(file, "    <p>Все характеристики %s:</p>", r.Replace(tovar.Name))
				fmt.Fprintf(file, "    <ul>")
				//анмаршалим свойства формата bson
				var flattened = make(map[string]interface{})
				flatten(tovar.Svoystva, "", &flattened)
				svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
						fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
					}
				}
				fmt.Fprintf(file, "    </ul>")
				fmt.Fprintf(file, "    <p>Чтобы купить %s, бренда %s можно заказать его через корзину или по телефону +7(495)233-02-05.</p>", r.Replace(tovar.Name), r.Replace(tovar.Proizvoditel))
				fmt.Fprintf(file, "    ]]></description>\n")

				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					if header != "Форма" {
						fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
					}
				}

				//анмаршалим множественные свойства формата bson
				flatten(tovar.SvoystvaMnozhestvo, "", &flattened)
				svoystvaHeaders = getHeadersFromBson(tovar.SvoystvaMnozhestvo)
				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					//fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, strings.Join(znachenie, "|"))
					fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
				}

				fmt.Fprintln(file, "</offer>")

				count++
			}
		}

		fmt.Fprintln(file, "</offers>")
		fmt.Fprintln(file, "</shop>")
		fmt.Fprintln(file, "</yml_catalog>")

		fmt.Printf("\n%d record(s) exported\n", count)
	}
	return result, err
}

func (s *Service) ExportCollection2Csv(path string) error {
	return s.repo.ExportCollection2Csv(path)
}

// экспортирует в xml товары у которых есть ImageUrlLocal и битриксID в формате retealXML
// TODO CATEGORIES
func (s *Service) ExportAllWithContent2RetailXml() error {
	// создаем файл экспорта
	filename := "retailcrm.xml"
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	tovarsWithId, err := s.repo.FindByQuery(bson.M{"imageUrlLocal": bson.M{"$exists": true}, "idBitrix": bson.M{"$exists": true}})
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT ExportAllWithContent2RetailXml", len(tovarsWithId))

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	fmt.Fprintln(file, "<yml_catalog date=\"2018-12-04 23:00:01\">")
	fmt.Fprintln(file, "<offers>")

	// Iterate over all items in a collection
	count := 0
	for _, tovar := range tovarsWithId {

		// записываем в файл при условии что есть локальное изображение и нет определенный КОМплектов

		fmt.Fprintf(file, "<offer id=\"%s\" productId=\"%s\" quantity=\"%f\">\n", tovar.IdBitrix, tovar.IdBitrix, tovar.Sklad)
		//fmt.Fprintf(file, "<offer name='%s'>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <picture>%s</picture>\n", tovar.ImageBitrix)
		fmt.Fprintf(file, "    <url>%s</url>\n", tovar.UrlBitrix)
		fmt.Fprintf(file, "    <price>%f</price>\n", tovar.Price)
		//TODO <categoryId>1824</categoryId>
		fmt.Fprintf(file, "    <name>%s</name>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <xmlId>%s</xmlId>\n", tovar.Id.Hex())
		fmt.Fprintf(file, "    <productName>%s</productName>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <param name=\"article\">%s</param>\n", r.Replace(tovar.Articul))
		fmt.Fprintf(file, "    <vendor>%s</vendor>\n", r.Replace(tovar.Proizvoditel))

		//TODO <param name="height" unit="mm">10</param>

		var flattened = make(map[string]interface{})
		flatten(tovar.Svoystva, "", &flattened)

		//анмаршалим свойства формата bson
		svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
		}
		fmt.Fprintln(file, "</offer>")

		count++
	}

	fmt.Fprintln(file, "</offers>")
	fmt.Fprintln(file, "</yml_catalog>")

	fmt.Printf("%d record(s) exported\n", count)

	return err
}

func (s *Service) Export2XmlLorosa(query bson.M) error {
	// создаем файл экспорта
	filename := "lorosa.xml"
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	tovarsWithId, err := s.repo.FindByQuery(query)
	if err != nil {
		return err
	}

	//GET MAP------------------------------------------------
	mapCategory := make(map[string]int)
	var num int
	for _, tovar1 := range tovarsWithId {
		if len(tovar1.Categories) > 0 && tovar1.Sklad > 0.00 {
			_, found := mapCategory[tovar1.Categories[0]]
			if found == false {
				num++
				mapCategory[tovar1.Categories[0]] = num
			}
		}
	}
	//GET MAP------------------------------------------------

	fmt.Fprintln(file, "<yml_catalog date=\"2019-02-01 08:31\">")
	fmt.Fprintln(file, "<shop>")
	fmt.Fprintln(file, "<name>lorosa.ru</name>")
	fmt.Fprintf(file, "<currencies>\n<currency id=\"RUB\" rate=\"1\"/>\n</currencies>\n")

	fmt.Fprintln(file, "<categories>")
	for category, number := range mapCategory {
		fmt.Fprintf(file, "    <category id=\"%v\"> %s</category>\n", number, r.Replace(category))
	}
	fmt.Fprintln(file, "</categories>")
	fmt.Fprintln(file, "<offers>")

	count := 0
	for _, tovar := range tovarsWithId {
		// записываем в файл при условии что есть локальное изображение и нет определенный КОМплектов
		//minOptPrice := CountMinOptPriceFromPostavchiki(tovar.PricePostavkaOpt)
		//mvc := minOptPrice + 600.00 + 1000.00 + 100.00

		//if len(tovar.ImageUrlLocal) > 0 && tovar.Komplektnost != "Комплект СО" && tovar.Komplektnost != "Комплект одним товаром СО" && minOptPrice > 0 && tovar.Sklad > 0.00 && len(tovar.Categories) > 0 {

		fmt.Fprintf(file, "<offer id=\"%s\" available=\"true\">\n", tovar.Id.Hex())
		//fmt.Fprintf(file, "    <url>%s</url>\n", r.Replace(tovar.UrlBitrix))
		fmt.Fprintf(file, "    <price>%f</price>\n", tovar.Price)
		fmt.Fprintf(file, "    <currencyId>RUB</currencyId>\n")
		fmt.Fprintf(file, "    <categoryId>%v</categoryId>\n", mapCategory[tovar.Categories[0]])
		fmt.Fprintf(file, "    <name>%s</name>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <vendor>%s</vendor>\n", r.Replace(tovar.Proizvoditel))
		fmt.Fprintf(file, "    <vendorCode>%s</vendorCode>\n", r.Replace(tovar.Articul))
		//fmt.Fprintf(file, "    <minprice>%f</minprice>\n", mvc)
		imgP := "http://content.tekopt.ru" + tovar.ImageUrlLocal
		fmt.Fprintf(file, "    <picture>%s</picture>\n", imgP)

		fmt.Fprintf(file, "    <Articul>%s</Articul>\n", r.Replace(tovar.Articul))
		fmt.Fprintf(file, "    <Categories>%s</Categories>\n", r.Replace(strings.Join(tovar.Categories, "|")))
		fmt.Fprintf(file, "    <StranaIzgotovitel>%s</StranaIzgotovitel>\n", r.Replace(tovar.StranaIzgotovitel))
		fmt.Fprintf(file, "    <Seriya>%s</Seriya>\n", r.Replace(tovar.Seriya))
		fmt.Fprintf(file, "    <Model>%s</Model>\n", r.Replace(tovar.Model))

		var imagesMore []string
		for _, v := range tovar.ImagesMore {
			img := "http://content.tekopt.ru" + v
			imagesMore = append(imagesMore, img)
		}
		fmt.Fprintf(file, "    <ImagesMore>%s</ImagesMore>\n", strings.Join(imagesMore, "|"))
		fmt.Fprintf(file, "    <Videos>%s</Videos>\n", r.Replace(strings.Join(tovar.Videos, "|")))
		var documents []string
		for _, v := range tovar.Documents {
			img := "http://content.tekopt.ru" + v
			documents = append(documents, img)
		}
		fmt.Fprintf(file, "    <Documents>%s</Documents>\n", r.Replace(strings.Join(documents, "|")))

		var flattened = make(map[string]interface{})
		flatten(tovar.Svoystva, "", &flattened)

		//анмаршалим свойства формата bson
		svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			fmt.Fprintf(file, "    <param name=\"%s\">%s</param>\n", header, r.Replace(znachenie))
		}

		fmt.Fprintln(file, "</offer>")

		count++
		//}
	}

	fmt.Fprintln(file, "</offers>")
	fmt.Fprintln(file, "</shop>")
	fmt.Fprintln(file, "</yml_catalog>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

func (s *Service) ObnulAkciiByVnutrId() error {
	tovars, err := s.repo.FindByQuery(bson.M{"aciya": true})
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR OBNUL Akcii", len(tovars))
	if len(tovars) > 0 {

		for _, tovar := range tovars {
			tovar.Aciya = false
			err = s.repo.UpdateTovarByVnutrId(&tovar)
			if err != nil {
				return err
			}
		}
	}

	return err
}

// make all products avito false
func (s *Service) ObnulAvitoByVnutrId() error {
	tovars, err := s.repo.FindByQuery(bson.M{"availableAvito": true})
	//tovars, err := s.repo.FindAll()
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR OBNUL Avito", len(tovars))
	if len(tovars) > 0 {

		for _, tovar := range tovars {
			tovar.AvailableAvito = false
			err = s.repo.UpdateTovarByVnutrId(&tovar)
			if err != nil {
				return err
			}
		}
	}

	return err
}

// выгружает xml в формате Авито  http://autoload.avito.ru/format/dlya_doma_i_dachi/
func (s *Service) ExportXmlAvito(filename, address, category, goodsType, goodsSubType, condition, domainUrl string) error {
	// создаем файл экспорта
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	addresses := []string{}
	addresses = append(addresses, address)
	//addresses = append(addresses, "Санкт-Петербург, Богословская, 4 к1")
	//addresses = append(addresses, "Екатеринбург, Монтажников, 26а")
	//addresses = append(addresses, "Краснодар, Зиповская, 34 к2")

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	tovarsWithId, err := s.repo.FindByQuery(bson.M{"availableAvito": true})
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT Avito", len(tovarsWithId))
	count := 0

	fmt.Fprintln(file, "<Ads formatVersion=\"3\" target=\"Avito.ru\">")

	for _, tovar := range tovarsWithId {
		for n, adres := range addresses {

			//ПИШЕМ В ФАЙЛ данные
			fmt.Fprintf(file, "<Ad>\n")
			fmt.Fprintf(file, "    <Id>%s</Id>\n", tovar.Id.Hex()+fmt.Sprint(n))
			fmt.Fprintf(file, "    <AllowEmail>Да</AllowEmail>\n")
			fmt.Fprintf(file, "    <AdType>Товар приобретен на продажу</AdType>\n")
			fmt.Fprintf(file, "    <Address>%s</Address>\n", adres)
			fmt.Fprintf(file, "    <Category>%s</Category>\n", category)
			fmt.Fprintf(file, "    <GoodsType>%s</GoodsType>\n", goodsType)
			fmt.Fprintf(file, "    <GoodsSubType>%s</GoodsSubType>\n", goodsSubType)
			fmt.Fprintf(file, "    <Condition>%s</Condition>\n", condition)
			fmt.Fprintf(file, "    <Title>%s</Title>\n", r.Replace(tovar.Name))

			switch tovar.Name {
			case "Керамогранит Montana Form White GP6MOF00 41*41":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 790)
			case "Керамогранит 610015000244 Charme Evo Lux Antracite 59x59", "Керамогранит 610015000274 Heat Steel Lap 60x120":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1550)
			case "Керамогранит 610015000269 Heat Iron Lap 60x60":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1400)
			case "Керамогранит 610015000449 Supernova Onyx Ivory Chiffon Lap 60x120", "Керамогранит 610015000442 Allure Grey Beauty Lap/Аллюр Грей Бьюти Шлиф 60x120":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1580)
			case "Керамогранит 610015000447 Allure Imperial Black Lap/Аллюр Империал Блек Шлиф 59x59":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1980)
			case "Керамогранит 610010000742 Supernova Onyx Persian Jade Rett 60х60":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1180)
			case "Керамогранит 610010000795 Heat Steel Rett 60x60":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1202)
			case "Керамогранит 610015000136 гриджио лаппато 60х60":
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 1700)
			default:
				//fmt.Fprintf(file, "    <Price>%d</Price>\n", int(tovar.Price*0.5))
				fmt.Fprintf(file, "    <Price>%d</Price>\n", 100)

			}

			fmt.Fprintf(file, "    <Images>")
			fmt.Fprintf(file, "    		<Image url=\"%s%s\"/>\n", domainUrl, tovar.ImageUrlLocal)
			if len(tovar.ImagesMore) > 0 {
				for _, dopImg := range tovar.ImagesMore {
					fmt.Fprintf(file, "    		<Image url=\"%s%s\"/>\n", domainUrl, dopImg)
				}
			}
			fmt.Fprintf(file, "    </Images>\n")

			if len(tovar.Videos) > 0 {
				for _, video := range tovar.Videos {
					fmt.Fprintf(file, "    		<VideoURL>=\"%s\"</VideoURL>\n", strings.Replace(video, "http://www.youtube.com/embed/", "https://www.youtube.com/watch?v=", 1))
				}
			}

			switch adres {
			case "Россия, Москва, поселение Мосрентген, Калужское шоссе 20 км":
				fmt.Fprintf(file, "    <Description><![CDATA[")
				fmt.Fprintf(file, "    <p>%s</p>", tovar.Name)
				fmt.Fprintf(file, "    <p>Плитка бренда %s</p>", tovar.Proizvoditel)
				fmt.Fprint(file, "    <p><strong>Скидки на товар до 50% до конца декабря - напиши чтобы узнать подробности</strong></p>")
				fmt.Fprintf(file, "    <p>Консультация по подбору плитки - БЕСПЛАТНО</p>")
				fmt.Fprintf(file, "    <p>Шоу-рум в Москве. Доставка в любую точку РФ</p>")
				fmt.Fprintf(file, "    <p>При отправке гарантируем целостность плитки</p>")
				fmt.Fprintf(file, "    <p>Вышлем образцы</p>")
				fmt.Fprintf(file, "    <p>Характеристики %s , бренда %s:</p>", tovar.Name, tovar.Proizvoditel)
				fmt.Fprintf(file, "    <ul>")

				//анмаршалим свойства формата bson
				var flattened = make(map[string]interface{})
				flatten(tovar.Svoystva, "", &flattened)
				svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
						fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
					}
				}

				fmt.Fprintf(file, "    </ul>")
				fmt.Fprintf(file, "    <p><strong>Просто напишите нам в чат Авито - мы определимся с ценами и количеством товаров</strong></p>")
				fmt.Fprintf(file, "    <p>Наш товар также ищут по запросам: кераморгранит, керамогранит для пола, керамическая плитка, плитка керамогранит, купить керамическую плитку, плитка напольная, плитка настенная</p>")
				fmt.Fprintf(file, "    ]]></Description>\n")

			default:
				fmt.Fprintf(file, "    <Description><![CDATA[")
				fmt.Fprintf(file, "    <p>%s</p>", tovar.Name)
				fmt.Fprintf(file, "    <p>Плитка бренда %s</p>", tovar.Proizvoditel)
				fmt.Fprint(file, "    <p><strong>Скидки на товар до 50% до конца декабря - напиши чтобы узнать подробности</strong></p>")
				fmt.Fprintf(file, "    <p><strong>ПОЖАЛУЙСТА ПЕРЕД ВЫЕЗДОМ УТОЧНЯЙТЕ ПО НАЛИЧИЮ ТОЙ ИЛИ ИНОЙ ПЛИТКИ И МЕСТО ДИСЛОКАЦИИ ШОУ-РУМА</strong></p>")
				fmt.Fprintf(file, "    <p>Консультация по подбору плитки - БЕСПЛАТНО</p>")
				fmt.Fprintf(file, "    <p>Доставка в любую точку РФ</p>")
				fmt.Fprintf(file, "    <p>При отправке гарантируем целостность плитки</p>")
				fmt.Fprintf(file, "    <p>Вышлем образцы</p>")
				fmt.Fprintf(file, "    <p>Характеристики %s , бренда %s:</p>", tovar.Name, tovar.Proizvoditel)
				fmt.Fprintf(file, "    <ul>")

				//анмаршалим свойства формата bson
				var flattened = make(map[string]interface{})
				flatten(tovar.Svoystva, "", &flattened)
				svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
						fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
					}
				}

				fmt.Fprintf(file, "    </ul>")
				fmt.Fprintf(file, "    <p><strong>Просто напишите нам в чат Авито - мы определимся с ценами и количеством товаров</strong></p>")
				fmt.Fprintf(file, "    <p>Наш товар также ищут по запросам: кераморгранит, керамогранит для пола, керамическая плитка, плитка керамогранит, купить керамическую плитку, плитка напольная, плитка настенная</p>")
				fmt.Fprintf(file, "    ]]></Description>\n")
			}
			fmt.Fprintln(file, "</Ad>")
		}

		count++
	}

	fmt.Fprintln(file, "</Ads>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

// яндекс объявления https://o.yandex.ru/my/feed/
func (s *Service) ExportXmlYandexObjavleniya(filename, address, category, condition, domainUrl, phone string) error {
	// создаем файл экспорта
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	addresses := []string{}
	addresses = append(addresses, address)
	//addresses = append(addresses, "Санкт-Петербург, Богословская, 4 к1")
	//addresses = append(addresses, "Екатеринбург, Монтажников, 26а")
	//addresses = append(addresses, "Краснодар, Зиповская, 34 к2")

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	tovarsWithId, err := s.repo.FindByQuery(bson.M{"availableAvito": true})
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT YandexObjavleniya", len(tovarsWithId))
	count := 0

	fmt.Fprintln(file, "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
	fmt.Fprintln(file, "<feed version=\"1\">")
	fmt.Fprintln(file, "<offers>")

	for _, tovar := range tovarsWithId {
		for n, adres := range addresses {

			//ПИШЕМ В ФАЙЛ данные
			fmt.Fprintf(file, "<offer>\n")
			fmt.Fprintf(file, "    <id>%s</id>\n", tovar.Id.Hex()+fmt.Sprint(n))
			fmt.Fprintf(file, "    <seller>\n")
			fmt.Fprintf(file, "        <contacts>\n")
			fmt.Fprintf(file, "           <phone>%s</phone>\n", phone)
			fmt.Fprintf(file, "           <contact-method>any</contact-method>\n")
			fmt.Fprintf(file, "        </contacts>\n")
			fmt.Fprintf(file, "        <locations>\n")
			fmt.Fprintf(file, "            <location>\n")
			fmt.Fprintf(file, "               <address>%s</address>\n", adres)
			fmt.Fprintf(file, "            </location>\n")
			fmt.Fprintf(file, "        </locations>\n")
			fmt.Fprintf(file, "    </seller>\n")
			fmt.Fprintf(file, "    <title>%s</title>\n", r.Replace(tovar.Name))
			fmt.Fprintf(file, "    <condition>%s</condition>\n", condition)
			fmt.Fprintf(file, "    <category>%s</category>\n", category)
			//fmt.Fprintf(file, "    <AdType>Товар приобретен на продажу</AdType>\n")
			//fmt.Fprintf(file, "    <GoodsType>%s</GoodsType>\n", goodsType)
			//fmt.Fprintf(file, "    <GoodsSubType>%s</GoodsSubType>\n", goodsSubType)

			switch tovar.Name {
			case "Керамогранит Montana Form White GP6MOF00 41*41":
				fmt.Fprintf(file, "    <price>%d</price>\n", 790)
			case "Керамогранит 610015000244 Charme Evo Lux Antracite 59x59", "Керамогранит 610015000274 Heat Steel Lap 60x120":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1550)
			case "Керамогранит 610015000269 Heat Iron Lap 60x60":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1400)
			case "Керамогранит 610015000449 Supernova Onyx Ivory Chiffon Lap 60x120", "Керамогранит 610015000442 Allure Grey Beauty Lap/Аллюр Грей Бьюти Шлиф 60x120":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1580)
			case "Керамогранит 610015000447 Allure Imperial Black Lap/Аллюр Империал Блек Шлиф 59x59":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1980)
			case "Керамогранит 610010000742 Supernova Onyx Persian Jade Rett 60х60":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1180)
			case "Керамогранит 610010000795 Heat Steel Rett 60x60":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1202)
			case "Керамогранит 610015000136 гриджио лаппато 60х60":
				fmt.Fprintf(file, "    <price>%d</price>\n", 1700)
			default:
				fmt.Fprintf(file, "    <price>%d</price>\n", int(tovar.Price*0.7))
				//fmt.Fprintf(file, "    <price>%d</price>\n", 100)

			}

			fmt.Fprintf(file, "    <images>\n")
			fmt.Fprintf(file, "    		<image>%s%s</image>\n", domainUrl, tovar.ImageUrlLocal)
			if len(tovar.ImagesMore) > 0 {
				for _, dopImg := range tovar.ImagesMore {
					fmt.Fprintf(file, "    		<image>%s%s</image>\n", domainUrl, dopImg)
				}
			}
			fmt.Fprintf(file, "    </images>\n")

			if len(tovar.Videos) > 0 {
				for _, video := range tovar.Videos {
					fmt.Fprintf(file, "    		<video>%s</video>\n", strings.Replace(video, "http://www.youtube.com/embed/", "https://www.youtube.com/watch?v=", 1))
				}
			}

			switch adres {
			case "Россия, Москва, поселение Мосрентген, Калужское шоссе 20 км":
				fmt.Fprintf(file, "    <description><![CDATA[")
				fmt.Fprintf(file, "    <p>%s</p>", tovar.Name)
				fmt.Fprintf(file, "    <p>Плитка бренда %s</p>", tovar.Proizvoditel)
				fmt.Fprintln(file, `    <p><strong>Скидки на товар до 50% до конца декабря - напиши чтобы узнать подробности</strong></p>`)
				fmt.Fprintf(file, "    <p>Консультация по подбору плитки - БЕСПЛАТНО</p>")
				fmt.Fprintf(file, "    <p>Шоу-рум в Москве. Доставка в любую точку РФ</p>")
				fmt.Fprintf(file, "    <p>При отправке гарантируем целостность плитки</p>")
				fmt.Fprintf(file, "    <p>Вышлем образцы</p>")
				fmt.Fprintf(file, "    <p>Характеристики %s , бренда %s:</p>", tovar.Name, tovar.Proizvoditel)
				fmt.Fprintf(file, "    <ul>")

				//анмаршалим свойства формата bson
				var flattened = make(map[string]interface{})
				flatten(tovar.Svoystva, "", &flattened)
				svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
						fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
					}
				}

				fmt.Fprintf(file, "    </ul>")
				fmt.Fprintf(file, "    <p><strong>Просто обращайтесь к нам - мы определимся с ценами и количеством товаров</strong></p>")
				fmt.Fprintf(file, "    <p>Наш товар также ищут по запросам: кераморгранит, керамогранит для пола, керамическая плитка, плитка керамогранит, купить керамическую плитку, плитка напольная, плитка настенная</p>")
				fmt.Fprintf(file, "    ]]></description>\n")

			default:
				fmt.Fprintf(file, "    <description><![CDATA[")
				fmt.Fprintf(file, "    <p>%s</p>", tovar.Name)
				fmt.Fprintf(file, "    <p>Плитка бренда %s</p>", tovar.Proizvoditel)
				//fmt.Fprintf(file, "    <p><strong>Скидки на товар до 50% до конца декабря - напиши чтобы узнать подробности</strong></p>")
				fmt.Fprintf(file, "    <p><strong>ПОЖАЛУЙСТА ПЕРЕД ВЫЕЗДОМ УТОЧНЯЙТЕ ПО НАЛИЧИЮ ТОЙ ИЛИ ИНОЙ ПЛИТКИ И МЕСТО ДИСЛОКАЦИИ ШОУ-РУМА</strong></p>")
				fmt.Fprintf(file, "    <p>Консультация по подбору плитки - БЕСПЛАТНО</p>")
				fmt.Fprintf(file, "    <p>Доставка в любую точку РФ</p>")
				fmt.Fprintf(file, "    <p>При отправке гарантируем целостность плитки</p>")
				fmt.Fprintf(file, "    <p>Вышлем образцы</p>")
				fmt.Fprintf(file, "    <p>Характеристики %s , бренда %s:</p>", tovar.Name, tovar.Proizvoditel)
				fmt.Fprintf(file, "    <ul>")

				//анмаршалим свойства формата bson
				var flattened = make(map[string]interface{})
				flatten(tovar.Svoystva, "", &flattened)
				svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
				for _, header := range svoystvaHeaders {
					znachenie := fmt.Sprint(flattened[header])
					if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
						fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
					}
				}

				fmt.Fprintf(file, "    </ul>")
				fmt.Fprintf(file, "    <p><strong>Просто напишите нам в чат Авито - мы определимся с ценами и количеством товаров</strong></p>")
				fmt.Fprintf(file, "    <p>Наш товар также ищут по запросам: кераморгранит, керамогранит для пола, керамическая плитка, плитка керамогранит, купить керамическую плитку, плитка напольная, плитка настенная</p>")
				fmt.Fprintf(file, "    ]]></description>\n")
			}
			fmt.Fprintln(file, "</offer>")
		}

		count++
	}

	fmt.Fprintln(file, "</offers>")
	fmt.Fprintln(file, "</feed>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

// выгружает xml в формате Юлы  https://docs.google.com/document/d/1flyFODQ1UGy6pKh5jwi0-yuNz2SzqkeZsNwEvD1zbmU/edit#heading=h.w9x1rxgx1b6g
func (s *Service) ExportXmlYoula(filename, youlaCategoryId, youlaSubcategoryId, tipCategory, domainUrl, phone string) error {
	// создаем файл экспорта
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	//-- создаем файл экспорта

	r := strings.NewReplacer(
		`&`, "&amp;",
		//`'`, "&#39;", // "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
		`'`, "&apos;",
		`<`, "&lt;",
		`>`, "&gt;",
		//`"`, "&#34;", // "&#34;" is shorter than "&quot;".
		`"`, "&quot;",
	)

	tovarsWithId, err := s.repo.FindByQuery(bson.M{"availableAvito": true})
	if err != nil {
		return err
	}
	fmt.Println("Kol TOVARS FOR EXPORT Youla", len(tovarsWithId))
	count := 0

	fmt.Fprintln(file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")

	nowTime := time.Now()
	fmt.Fprintln(file, "<yml_catalog date=\""+nowTime.Format("2006-01-02 15:04")+"\">")

	fmt.Fprintln(file, "<shop>")
	fmt.Fprintln(file, "<offers>")

	for _, tovar := range tovarsWithId {
		//ПИШЕМ В ФАЙЛ данные
		fmt.Fprintf(file, "    <offer id=\"%s\">\n", tovar.Id.Hex())
		fmt.Fprintf(file, "    <youlaCategoryId>%s</youlaCategoryId>\n", youlaCategoryId)
		fmt.Fprintf(file, "    <youlaSubcategoryId>%s</youlaSubcategoryId>\n", youlaSubcategoryId)
		fmt.Fprintf(file, "    %s\n", tipCategory)
		fmt.Fprintf(file, "    <name>%s</name>\n", r.Replace(tovar.Name))
		fmt.Fprintf(file, "    <phone>%s</phone>\n", phone)

		switch tovar.Name {
		case "Керамогранит Montana Form White GP6MOF00 41*41":
			fmt.Fprintf(file, "    <price>%d</price>\n", 790)
		case "Керамогранит 610015000244 Charme Evo Lux Antracite 59x59", "Керамогранит 610015000274 Heat Steel Lap 60x120":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1550)
		case "Керамогранит 610015000269 Heat Iron Lap 60x60":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1400)
		case "Керамогранит 610015000449 Supernova Onyx Ivory Chiffon Lap 60x120", "Керамогранит 610015000442 Allure Grey Beauty Lap/Аллюр Грей Бьюти Шлиф 60x120":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1580)
		case "Керамогранит 610015000447 Allure Imperial Black Lap/Аллюр Империал Блек Шлиф 59x59":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1980)
		case "Керамогранит 610010000742 Supernova Onyx Persian Jade Rett 60х60":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1180)
		case "Керамогранит 610010000795 Heat Steel Rett 60x60":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1202)
		case "Керамогранит 610015000136 гриджио лаппато 60х60":
			fmt.Fprintf(file, "    <price>%d</price>\n", 1700)
		default:
			//fmt.Fprintf(file, "    <price>%d</price>\n", int(tovar.Price*0.5))
			fmt.Fprintf(file, "    <price>%d</price>\n", 100)

		}

		fmt.Fprintf(file, "    		<picture>\"%s%s\"</picture>\n", domainUrl, tovar.ImageUrlLocal)
		if len(tovar.ImagesMore) > 0 {
			for _, dopImg := range tovar.ImagesMore {
				fmt.Fprintf(file, "    		<picture>\"%s%s\"</picture>\n", domainUrl, dopImg)
			}
		}

		fmt.Fprintf(file, "    <description><![CDATA[")
		fmt.Fprintf(file, "    <p>%s в Москве</p>", tovar.Name)
		fmt.Fprintf(file, "    <p>Плитка бренда %s</p>", tovar.Proizvoditel)
		fmt.Fprintln(file, "    <p><strong>Скидки на товар до 50% - напишите чтобы узнать подробности</strong></p>")
		fmt.Fprintf(file, "    <p>Консультация по подбору плитки - БЕСПЛАТНО</p>")
		fmt.Fprintf(file, "    <p>Доставка по Москве и МО</p>")
		fmt.Fprintf(file, "    <p>Отправка в регионы (гарантируем целостность плитки)</p>")
		fmt.Fprintf(file, "    <p>Вышлем образцы</p>")
		fmt.Fprintf(file, "    <p>Характеристики %s , бренда %s:</p>", tovar.Name, tovar.Proizvoditel)
		fmt.Fprintf(file, "    <ul>")

		//анмаршалим свойства формата bson
		var flattened = make(map[string]interface{})
		flatten(tovar.Svoystva, "", &flattened)
		svoystvaHeaders := getHeadersFromBson(tovar.Svoystva)
		for _, header := range svoystvaHeaders {
			znachenie := fmt.Sprint(flattened[header])
			if header != "Акция?" && header != "Распродажа" && header != "Ректификат?" && header != "Хиты продаж?" {
				fmt.Fprintf(file, "    <li>\"%s\" - %s</li>\n", header, r.Replace(znachenie))
			}
		}

		fmt.Fprintf(file, "    </ul>")
		fmt.Fprintf(file, "    <p><strong>Просто напишите нам в чат Авито - мы определимся с ценами и количеством товаров</strong></p>")
		fmt.Fprintf(file, "    <p>Наш товар также ищут по запросам: кераморгранит, керамогранит для пола, керамическая плитка, плитка керамогранит, купить керамическую плитку, плитка напольная, плитка настенная</p>")
		fmt.Fprintf(file, "    ]]></description>\n")

		fmt.Fprintln(file, "</offer>")

		count++
	}

	fmt.Fprintln(file, "</offers>")
	fmt.Fprintln(file, "</shop>")
	fmt.Fprintln(file, "</yml_catalog>")

	fmt.Printf("\n%d record(s) exported\n", count)
	return err
}

// разбивает массив товаров на части. длина одного массива = lim
func split(buf []entity.Tovar, lim int) [][]entity.Tovar {
	var chunk []entity.Tovar
	chunks := make([][]entity.Tovar, 0, len(buf)/lim+1)
	for len(buf) >= lim {
		chunk, buf = buf[:lim], buf[lim:]
		chunks = append(chunks, chunk)
	}
	if len(buf) > 0 {
		chunks = append(chunks, buf[:len(buf)])
	}
	return chunks
}

func (s *Service) RemoveTovarFromSlice(tovars []entity.Tovar, tToDel entity.Tovar) []entity.Tovar {
	for i, t := range tovars {
		if t.IdVnutr == tToDel.IdVnutr {
			return append(tovars[:i], tovars[i+1:]...)
		}
	}
	return tovars
}

// Функция для сравнения двух BSON-документов
func (s *Service) CompareBSONDocuments(doc1, doc2 bson.M) bool {
	// Сравниваем длины карт
	if len(doc1) != len(doc2) {
		return false
	}

	// Сравниваем каждую пару ключ-значение
	for key, value := range doc1 {
		if !reflect.DeepEqual(value, doc2[key]) {
			return false
		}
	}

	return true
}

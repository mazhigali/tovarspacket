package tovar

import (
	"log"
	"testing"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

func TestUpdateTovarByVnutrId(t *testing.T) {
	log.Println("TestUpdateTovarByVnutrId")
	dbConTT := NewDBConnection(&ConfMongo{
		ConnectionString: "mongodb://127.0.0.1:27017",
		DbName:           "rusceramica",
		CollectionName:   "seriyaEstima",
		//Indexes:          []models.Index{},
	})
	defer dbConTT.CloseConnection()

	s := NewService(dbConTT)

	err := s.UpdateTovarByVnutrId(&entity.Tovar{IdVnutr: "162598", Name: "test"})
	if err != nil {
		t.Error("TestGetSvoystvaByQuery: ", err)
	}
}

func TestGetSvoystvaByQuery(t *testing.T) {
	log.Println("TestGetSvoystvaByQuery")
	dbConTT := NewDBConnection(&ConfMongo{
		ConnectionString: "mongodb://127.0.0.1:27017",
		DbName:           "rusceramica",
		CollectionName:   "tovary-together",
		//Indexes:          []models.Index{},
	})
	defer dbConTT.CloseConnection()

	query := bson.M{}

	svoystva, err := dbConTT.GetSvoystvaByQuery(query)
	if err != nil {
		t.Error("GetSvoystvaByQuery: ", err)
	}

	if len(svoystva) == 0 {
		t.Error("GetSvoystvaByQuery: ", err)
	}

	log.Println("GetSvoystvaByQuery: ", len(svoystva))
}

func TestGetHeadersAll(t *testing.T) {
	log.Println("GetHeadersAll")
	dbConTT := NewDBConnection(&ConfMongo{
		ConnectionString: "mongodb://127.0.0.1:27017",
		DbName:           "rusceramica",
		CollectionName:   "tovary-together",
		//Indexes:          []models.Index{},
	})
	defer dbConTT.CloseConnection()

	headers, err := dbConTT.GetHeadersAll()
	if err != nil {
		t.Error("TestGetHeadersAll: ", err)
	}

	if len(headers) == 0 {
		t.Error("TestGetHeadersAll: ", err)
	}

	log.Println("TestGetHeadersAll: ", len(headers))
}

func TestSetSootvetstviePostavschikByVnutrId(t *testing.T) {
	log.Println("TestSetSootvetstviePostavschikByVnutrId")
	dbConTT := NewDBConnection(&ConfMongo{
		ConnectionString: "mongodb://127.0.0.1:27017",
		DbName:           "rusceramica",
		CollectionName:   "tovary-together",
		//Indexes:          []models.Index{},
	})
	defer dbConTT.CloseConnection()

	sootvetstviePostavschik := entity.Sootvetstvie{
		Type:               "импорт",
		NameTranslitPostav: "italon",
		Value:              "Tоццетто Фэшн 7,2X7,2600090000304Italon",
		Field:              "Name+ArticulVendor+Proizvoditel",
	}
	//sootvetstviePostavschik := entity.Sootvetstvie{
	//Type:               "импорт",
	//NameTranslitPostav: "italon2",
	//Value:              "Tоццетто Фэшн 7,2X7,2600090000304Italon3",
	//Field:              "Name+ArticulVendor+Proizvoditel",
	//}
	err := dbConTT.SetSootvetstviePostavschikByVnutrId("162598", sootvetstviePostavschik)
	if err != nil {
		t.Error("TestSetSootvetstviePostavschikByVnutrId: ", err)
	}

}

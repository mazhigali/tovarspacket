package tovar

import (
	"reflect"
	"sort"

	"go.mongodb.org/mongo-driver/bson"
)

//считает максимальную розничную цену из всех поставщиков и возращает ее
func CountMaxRoznPriceFromPostavchiki(optPricesPostavshikov bson.M) float64 {
	v := reflect.ValueOf(optPricesPostavshikov)

	allOptPrices := []float64{}

	for i := 0; i < v.NumField(); i++ {
		var chislo float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			chislo = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				chislo = v.Field(i).Elem().Float()
			}
		}
		//fmt.Printf("\nTYPE: %T VALUE: %f \n", chislo, chislo)

		allOptPrices = append(allOptPrices, chislo)
	}

	maxOptPrice := maxFloatSlice(allOptPrices)

	return maxOptPrice
}

//считает минимальную розничную из всех поставщиков и возращает ее
func CountMinRoznPriceFromPostavchiki(RoznPricesPostavschikov bson.M) float64 {
	v := reflect.ValueOf(RoznPricesPostavschikov)

	allRoznPrices := []float64{}

	for i := 0; i < v.NumField(); i++ {
		var chislo float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			chislo = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				chislo = v.Field(i).Elem().Float()
			}
		}
		//fmt.Printf("\nTYPE: %T VALUE: %f \n", chislo, chislo)

		if chislo > 10.00 { // это чтобы поражняка не было с ценами кукольными или поставщик запихнул цену 1 руб как уже было
			allRoznPrices = append(allRoznPrices, chislo)
		}
	}

	var minRoznPrice float64
	//fmt.Println(allRoznPrices)
	if len(allRoznPrices) > 0 {
		minRoznPrice = minFloatSliceNotNul(allRoznPrices)
		//fmt.Println("MIN ROZN CENA: ", minRoznPrice)
		if minRoznPrice != 0 {
			return minRoznPrice
		}
	}
	return minRoznPrice
}

//считает максимальный остаток на складе у всех поставщиков и возращает ее
func CountMaxOstatokFromPostavchiki(sklady bson.M) float64 {
	v := reflect.ValueOf(sklady)
	var maxOstatok float64

	allOstatki := []float64{}

	for i := 0; i < v.NumField(); i++ {

		//fmt.Println(v.Field(i).Type().Kind().String())

		switch v.Field(i).Type().Kind().String() {
		case "float64":
			//fmt.Println(v.Field(i).Float())
			allOstatki = append(allOstatki, v.Field(i).Float())
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				//fmt.Println(v.Field(i).Elem().Float())
				//fmt.Println(v.Field(i).Elem())
				//fmt.Println(v.Field(i).Interface())  //ХУЙНЯ если так то интерфейс показывает адреса
				allOstatki = append(allOstatki, v.Field(i).Elem().Float())
			}
			//} else {
			//fmt.Println("NOT VALID")
			//}
		}

	}

	if len(allOstatki) > 0 {
		maxOstatok = maxFloatSlice(allOstatki)
		//fmt.Println("MAX OSTATOK: ", maxOstatok)
	} else {
		maxOstatok = 0.00
	}

	return maxOstatok
}

//считает минимальную оптовую цену из всех поставщиков и возращает ее
func CountMinOptPriceFromPostavchiki(optPricesPostavshikov bson.M) float64 {
	v := reflect.ValueOf(optPricesPostavshikov)

	allOptPrices := []float64{}

	for i := 0; i < v.NumField(); i++ {
		var chislo float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			chislo = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				chislo = v.Field(i).Elem().Float()
			}
		}
		//fmt.Printf("\nTYPE: %T VALUE: %f \n", chislo, chislo)

		allOptPrices = append(allOptPrices, chislo)
	}

	minOptPrice := minFloatSliceNotNul(allOptPrices)
	//fmt.Println("MIN ROZN CENA: ", minRoznPrice)
	if minOptPrice != 0 {
		return minOptPrice
	}

	return minOptPrice
}

//считает среднюю закупочную цену из всех поставщиков и возращает ее
func CountSredOptPriceFromPostavchiki(optPricesPostavshikov bson.M) float64 {
	v := reflect.ValueOf(optPricesPostavshikov)
	num := 0.00
	sum := 0.00

	for i := 0; i < v.NumField(); i++ {
		var chislo float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			chislo = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				chislo = v.Field(i).Elem().Float()
			}
		}
		//fmt.Printf("\nTYPE: %T VALUE: %f \n", chislo, chislo)
		if chislo > 0.00 {
			sum += chislo
			num++
		}
	}
	if num != 0.00 {
		srednyaa := sum / num
		return srednyaa
	} else {
		return 0.00
	}

}

//считает среднюю розничную цену из всех поставщиков и возращает ее
func CountSredRoznPriceFromPostavchiki(optPricesPostavshikov bson.M) float64 {
	v := reflect.ValueOf(optPricesPostavshikov)
	num := 0.00
	sum := 0.00

	for i := 0; i < v.NumField(); i++ {
		var chislo float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			chislo = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				chislo = v.Field(i).Elem().Float()
			}
		}
		//fmt.Printf("\nTYPE: %T VALUE: %f \n", chislo, chislo)
		if chislo > 0.00 {
			sum += chislo
			num++
		}
	}
	if num != 0.00 {
		srednyaa := sum / num
		return srednyaa
	} else {
		return 0.00
	}
}

//считает максимальную оптовую цену из всех поставщиков и возращает ее
func CountMaxOptPriceFromPostavchiki(optPricesPostavshikov bson.M) float64 {
	v := reflect.ValueOf(optPricesPostavshikov)

	allOptPrices := []float64{}

	for i := 0; i < v.NumField(); i++ {
		var chislo float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			chislo = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				chislo = v.Field(i).Elem().Float()
			}
		}
		//fmt.Printf("\nTYPE: %T VALUE: %f \n", chislo, chislo)

		allOptPrices = append(allOptPrices, chislo)
	}

	maxOptPrice := maxFloatSlice(allOptPrices)
	//fmt.Println("MIN ROZN CENA: ", minRoznPrice)

	return maxOptPrice
}

func PriceOpt2Map(optPricesPostavshikov bson.M) map[string]float64 {
	resultMap := make(map[string]float64)

	v := reflect.ValueOf(&optPricesPostavshikov).Elem()

	for i := 0; i < v.NumField(); i++ {
		varName := v.Type().Field(i).Name

		var varValue float64
		switch v.Field(i).Type().Kind().String() {
		case "float64":
			varValue = v.Field(i).Float()
		case "ptr":
			if v.Field(i).Elem().IsValid() == true {
				varValue = v.Field(i).Elem().Float()
			}
		}

		resultMap[varName] = varValue
	}

	return resultMap
}

//получить минимальную цифру из массива кроме 0 !!!ЕСЛИ ВСЕ НУЛИ ТО ВЕРНЕТ 0
func minFloatSliceNotNul(v []float64) float64 {
	sort.Float64s(v)
	for _, i := range v {
		if i > 0.00 {
			return i
		}
	}
	return v[0]
}

//получить максимальную цифру из массива
func maxFloatSlice(v []float64) float64 {
	sort.Float64s(v)
	return v[len(v)-1]
}

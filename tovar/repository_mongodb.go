package tovar

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatalf("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatalf("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		log.Fatal(errors.New("Collection could not be created, maybe need to create it manually"))
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(4 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatalf("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindBySootvetstviePostavshik(namePostavshcik, nameForSearch string) (*entity.Tovar, error) {
	nameForSearch = strings.TrimSpace(nameForSearch)
	if nameForSearch == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}

	queryPostavshikName := "sootvetstviePostavschik." + namePostavshcik
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{queryPostavshikName: nameForSearch}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindTovarByQuery(query bson.M) (*entity.Tovar, error) {
	// создаем новый документ
	result := entity.Tovar{}

	// ищем
	err := c.collection.FindOne(context.TODO(), query).Decode(&result)
	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByArticul(articul string) (*entity.Tovar, error) {
	articul = strings.TrimSpace(articul)
	if articul == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}

	articulRegex := "^" + regexp.QuoteMeta(articul) + "$"

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"articul": primitive.Regex{articulRegex, "i"}}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByArticulMore(articul string) (*entity.Tovar, error) {
	articul = strings.TrimSpace(articul)
	if articul == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}

	articulRegex := "^" + regexp.QuoteMeta(articul) + "$"
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"articulsMore": primitive.Regex{articulRegex, "i"}}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByName(name string) (*entity.Tovar, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"name": name}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByModel(name string) (*entity.Tovar, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	articulRegex := "^" + regexp.QuoteMeta(name) + "$"
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"model": primitive.Regex{articulRegex, "i"}}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByNametranslit(name string) (*entity.Tovar, error) {
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	articulRegex := "^" + regexp.QuoteMeta(name) + "$"
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"nameTranslit": primitive.Regex{articulRegex, "i"}}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByVnutrID(idVnutr string) (*entity.Tovar, error) {
	idVnutr = strings.TrimSpace(idVnutr)
	if idVnutr == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"idVnutr": idVnutr}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByExtID(id string) (*entity.Tovar, error) {
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}

	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"externalId": id}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByEan(ean string) (*entity.Tovar, error) {
	ean = strings.TrimSpace(ean)
	if ean == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	articulRegex := "^" + regexp.QuoteMeta(ean) + "$"
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"ean": primitive.Regex{articulRegex, "i"}}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByEanMore(ean string) (*entity.Tovar, error) {
	ean = strings.TrimSpace(ean)
	if ean == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	articulRegex := "^" + regexp.QuoteMeta(ean) + "$"
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"eanMore": primitive.Regex{articulRegex, "i"}}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByUrl(url string) (*entity.Tovar, error) {
	// создаем новый документ
	result2 := entity.Tovar{}
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"url": url}).Decode(&result2)

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByIdBitrix(idBitrix string) (*entity.Tovar, error) {
	// создаем новый документ
	result := entity.Tovar{}
	// ищем
	err := c.collection.FindOne(context.TODO(), bson.M{"idBitrix": idBitrix}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByID(id string) (*entity.Tovar, error) {
	result2 := entity.Tovar{}
	// ищем
	idObject, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": idObject}).Decode(&result2)
	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByArtAndName(articul, name string) (*entity.Tovar, error) {
	articul = strings.TrimSpace(articul)
	name = strings.TrimSpace(name)
	if articul == "" || name == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(articul) + "$"
	nameRegex := "^" + regexp.QuoteMeta(name) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"articul": primitive.Regex{articulRegex, "i"}, "name": primitive.Regex{nameRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByArticulAndBrand(articul, brand string) (*entity.Tovar, error) {
	articul = strings.TrimSpace(articul)
	brand = strings.TrimSpace(brand)
	if articul == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(articul) + "$"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"articul": primitive.Regex{articulRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByModelAndBrand(model, brand string) (*entity.Tovar, error) {
	model = strings.TrimSpace(model)
	brand = strings.TrimSpace(brand)
	if model == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(model) + "$"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"model": primitive.Regex{articulRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByNameAndBrand(name, brand string) (*entity.Tovar, error) {
	name = strings.TrimSpace(name)
	brand = strings.TrimSpace(brand)
	if name == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(name) + "$"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"name": primitive.Regex{articulRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByEanAndBrand(model, brand string) (*entity.Tovar, error) {
	model = strings.TrimSpace(model)
	brand = strings.TrimSpace(brand)
	if model == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(model) + "$"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"ean": primitive.Regex{articulRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByEanMoreAndBrand(model, brand string) (*entity.Tovar, error) {
	model = strings.TrimSpace(model)
	brand = strings.TrimSpace(brand)
	if model == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	// получаем копию оригинальной сессии
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(model) + "$"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"eanMore": primitive.Regex{articulRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByArticulMoreAndBrand(articul, brand string) (*entity.Tovar, error) {
	articul = strings.TrimSpace(articul)
	brand = strings.TrimSpace(brand)
	if articul == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	articulRegex := "^" + regexp.QuoteMeta(articul) + "$"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"articulsMore": primitive.Regex{articulRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindPartInNameAndBrand(partInName, brand string) (*entity.Tovar, error) {
	partInName = strings.TrimSpace(partInName)
	brand = strings.TrimSpace(brand)
	if partInName == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}
	// создаем новый документ
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	nameRegex := ".*" + regexp.QuoteMeta(partInName) + ".*"
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	err := c.collection.FindOne(context.TODO(), bson.M{"name": primitive.Regex{nameRegex, "i"}, "proizvoditel": primitive.Regex{brandRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByPartInName(partInName string) (*entity.Tovar, error) {
	partInName = strings.TrimSpace(partInName)
	if partInName == "" {
		return nil, entity.ErrInputValueNil
	}
	result2 := entity.Tovar{}
	//--------------regex caseinsesitive-----------------
	nameRegex := ".*" + regexp.QuoteMeta(partInName) + ".*"

	err := c.collection.FindOne(context.TODO(), bson.M{"name": primitive.Regex{nameRegex, "i"}}).Decode(&result2)
	//--------------//regex caseinsesitive-----------------

	switch err {
	case nil:
		return &result2, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindAll() ([]entity.Tovar, error) {
	resultArray := []entity.Tovar{}

	cursor, err := c.collection.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindAllByBrand(brand string) ([]entity.Tovar, error) {
	resultWithId := []entity.Tovar{}
	// ищем
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	cursor, err := c.collection.Find(context.TODO(), bson.M{"proizvoditel": primitive.Regex{brandRegex, "i"}})

	err = cursor.All(context.TODO(), &resultWithId)

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllByCollection(collectionVnutrId string) ([]entity.Tovar, error) {
	resultWithId := []entity.Tovar{}
	// ищем

	cursor, err := c.collection.Find(context.TODO(), bson.M{"collections": collectionVnutrId})

	err = cursor.All(context.TODO(), &resultWithId)

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllByVnutrIds(vnutrIds []string) ([]entity.Tovar, error) {
	resultArray := []entity.Tovar{}
	// ищем

	cursor, err := c.collection.Find(context.TODO(), bson.D{
		{"idVnutr",
			bson.D{
				{"$in",
					vnutrIds,
				},
			},
		},
	})

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllByVnutrIds2(vnutrIds []string) ([]entity.Tovar, error) {
	resultArray := []entity.Tovar{}
	// ищем

	cursor, err := c.collection.Find(context.TODO(), bson.D{
		{"idVnutr",
			bson.D{
				{"$in",
					vnutrIds,
				},
			},
		},
	})

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllByBrandAndCategory(brand, category string) ([]entity.Tovar, error) {
	resultWithId := []entity.Tovar{}

	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"
	categoryRegex := "^" + regexp.QuoteMeta(category) + "$"

	cursor, err := c.collection.Find(context.TODO(), bson.M{"proizvoditel": primitive.Regex{brandRegex, "i"}, "Categories": primitive.Regex{categoryRegex, "i"}})

	err = cursor.All(context.TODO(), &resultWithId)

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllByBrandAndCategoryUrl(brand, categoryUrl string) ([]entity.Tovar, error) {
	resultWithId := []entity.Tovar{}

	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"
	categoryRegex := "^" + regexp.QuoteMeta(categoryUrl) + "$"

	cursor, err := c.collection.Find(context.TODO(), bson.M{"proizvoditel": primitive.Regex{brandRegex, "i"}, "categoriesForSite.url": primitive.Regex{categoryRegex, "i"}})

	err = cursor.All(context.TODO(), &resultWithId)

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllWithArticul() ([]entity.Tovar, error) {
	// создаем новый документ
	resultWithId := []entity.Tovar{}
	// ищем

	cursor, err := c.collection.Find(context.TODO(), bson.M{"articul": bson.M{"$ne": ""}})

	// Iterate through the returned cursor.
	for cursor.Next(context.TODO()) {
		res2 := entity.Tovar{}
		cursor.Decode(&res2)
		resultWithId = append(resultWithId, res2)
	}

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllWithoutArticul() ([]entity.Tovar, error) {
	// создаем новый документ
	resultWithId := []entity.Tovar{}
	// ищем

	cursor, err := c.collection.Find(context.TODO(), bson.M{"articul": ""})

	// Iterate through the returned cursor.
	for cursor.Next(context.TODO()) {
		res2 := entity.Tovar{}
		cursor.Decode(&res2)
		resultWithId = append(resultWithId, res2)
	}

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err

	}
}

func (c *MongoConnection) FindAllWithSkladAndImageAndArticul() ([]entity.Tovar, error) {
	// создаем новый документ
	resultArray := []entity.Tovar{}
	// ищем

	cursor, err := c.collection.Find(context.TODO(), bson.M{"skladPostavka": bson.M{"$exists": true}, "imageUrlLocal": bson.M{"$exists": true}})

	// Iterate through the returned cursor.
	for cursor.Next(context.TODO()) {
		res1 := entity.Tovar{}
		cursor.Decode(&res1)
		resultArray = append(resultArray, res1)
	}

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindAllWithSkladGTE(skladName string, kolOt int) ([]entity.Tovar, error) {
	resultArray := []entity.Tovar{}
	// ищем
	querySkladName := "skladPostavka." + skladName
	cursor, err := c.collection.Find(context.TODO(), bson.M{querySkladName: bson.M{"$gte": kolOt}})
	if err != nil {
		return nil, err
	}

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByQuery(query bson.M) ([]entity.Tovar, error) {
	resultArray := []entity.Tovar{}
	// ищем
	cursor, err := c.collection.Find(context.TODO(), query)

	err = cursor.All(context.TODO(), &resultArray)

	switch err {
	case nil:
		return resultArray, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) FindByQueryByPage(query bson.M, page, perPage int64) (resultArray []entity.Tovar, total int64, lastPage float64, err error) {

	if perPage == 0 {
		perPage = 100
	}
	if page == 0 {
		page = 1
	}

	findOptions := options.Find()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	total, err = c.collection.CountDocuments(ctx, query)
	if err != nil {
		return nil, total, lastPage, err
	}

	findOptions.SetSkip((int64(page) - 1) * perPage)
	findOptions.SetLimit(perPage)
	findOptions.SetSort(bson.D{{"price", 1}})

	// ищем
	cursor, err := c.collection.Find(ctx, query, findOptions)
	if err != nil {
		return nil, total, lastPage, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &resultArray)

	lastPage = math.Ceil(float64(total / perPage))

	switch err {
	case nil:
		return resultArray, total, lastPage, nil
	case mongo.ErrNoDocuments:
		return nil, total, lastPage, entity.ErrNotFound
	default:
		return nil, total, lastPage, err
	}
}

func (c *MongoConnection) FindByQuery2(query bson.M) ([]entity.Tovar, error) {
	resultWithId := []entity.Tovar{}
	// ищем
	cursor, err := c.collection.Find(context.TODO(), query)

	err = cursor.All(context.TODO(), &resultWithId)

	switch err {
	case nil:
		return resultWithId, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) AddTovar(tovarMongoDocument *entity.Tovar) (err error) {

	// добавляем новый аргумент
	_, err = c.collection.InsertOne(context.TODO(), &tovarMongoDocument)

	switch err {
	case nil:
		return nil
	default:
		// проверяем, была ли это ошибка вызванная дублированием
		//if mongo.IsDuplicateKeyError(err) == true {
		//err = errors.New("Duplicate name exists ")
		//}
		return err
	}
}

func (c *MongoConnection) AddDopArticul(idMongoString, dopArticul string) (err error) {
	idObject, err := primitive.ObjectIDFromHex(idMongoString)
	if err != nil {
		return err
	}

	// Query
	query := bson.M{"_id": idObject}
	update := bson.M{"$push": bson.M{"articulsMore": dopArticul}}

	//update
	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	return
}

func (c *MongoConnection) UpdateTovarByIdMongo(tovar *entity.Tovar) (err error) {
	query := bson.M{"_id": tovar.Id}
	update := bson.M{"$set": tovar}

	//idObject, err := primitive.ObjectIDFromHex(idString)
	//if err != nil {
	//return err
	//}

	currentTime := time.Now()
	tovar.UpdatedTime = &currentTime
	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) UpdateTovarByVnutrId(tovar *entity.Tovar) (err error) {
	if tovar.IdVnutr == "" {
		return entity.ErrInputValueNil
	}

	query := bson.M{"idVnutr": tovar.IdVnutr}
	update := bson.M{"$set": tovar}

	currentTime := time.Now()
	tovar.UpdatedTime = &currentTime

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) UpdateTovarByExtId(tovarMongoDocument *entity.Tovar) (err error) {
	if tovarMongoDocument.ExternalId == "" {
		return entity.ErrInputValueNil
	}

	query := bson.M{"externalId": tovarMongoDocument.ExternalId}
	update := bson.M{"$set": tovarMongoDocument}

	currentTime := time.Now()
	tovarMongoDocument.UpdatedTime = &currentTime

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) UpdateTovarByArtAndName(tovarMongoDocument *entity.Tovar) (err error) {

	query := bson.M{"articul": tovarMongoDocument.Articul, "name": tovarMongoDocument.Name}
	update := bson.M{"$set": tovarMongoDocument}
	//Поиск по артикулу и имени
	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// обновляет товар ищет совпадение артикула и brand  CASEINSENSITIVE
func (c *MongoConnection) UpdateTovarByArtBrand(tovarMongoDocument *entity.Tovar) (err error) {

	proizvoditelRegex := "^" + regexp.QuoteMeta(tovarMongoDocument.Proizvoditel) + "$"

	query := bson.M{"articul": tovarMongoDocument.Articul, "proizvoditel": primitive.Regex{proizvoditelRegex, "i"}}
	update := bson.M{"$set": tovarMongoDocument}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) UpdateTovarByModelBrand(tovarMongoDocument *entity.Tovar) (err error) {

	proizvoditelRegex := "^" + regexp.QuoteMeta(tovarMongoDocument.Proizvoditel) + "$"
	modelRegex := "^" + regexp.QuoteMeta(tovarMongoDocument.Model) + "$"
	currentTime := time.Now()
	tovarMongoDocument.UpdatedTime = &currentTime

	query := bson.M{"model": primitive.Regex{modelRegex, "i"}, "proizvoditel": primitive.Regex{proizvoditelRegex, "i"}}
	update := bson.M{"$set": tovarMongoDocument}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) UpdateTovarByNameBrand(tovarMongoDocument *entity.Tovar) (err error) {

	proizvoditelRegex := "^" + regexp.QuoteMeta(tovarMongoDocument.Proizvoditel) + "$"

	query := bson.M{"name": tovarMongoDocument.Name, "proizvoditel": primitive.Regex{proizvoditelRegex, "i"}}
	update := bson.M{"$set": tovarMongoDocument}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) RemoveTovar(tovarMongoDocument *entity.Tovar) (err error) {

	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"articul": tovarMongoDocument.Articul, "name": tovarMongoDocument.Name})
	if err != nil {
		return errors.New("ERROR REMOVE" + err.Error())
	}

	return err
}

func (c *MongoConnection) RemoveById(mongoId primitive.ObjectID) (err error) {

	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": mongoId})

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) RemoveByIdVnutr(vnutrId string) error {

	_, err := c.collection.DeleteOne(context.TODO(), bson.M{"idVnutr": vnutrId})

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) RemoveByUrl(url string) error {

	_, err := c.collection.DeleteOne(context.TODO(), bson.M{"url": url})

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) GetUniqBrandsAllCollection() ([]string, error) {
	var result []string

	// Specify the MaxTime option to limit the amount of time the operation can
	// run on the server.
	filter := bson.D{}
	opts := options.Distinct().SetMaxTime(4 * time.Second)
	values, err := c.collection.Distinct(context.TODO(), "proizvoditel", filter, opts)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range values {
		result = append(result, v.(string))
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) GetUniqBrandsWithoutArticul() ([]string, error) {
	var result []string

	filter := bson.M{"articul": bson.M{"$eq": ""}}
	opts := options.Distinct().SetMaxTime(4 * time.Second)
	values, err := c.collection.Distinct(context.TODO(), "proizvoditel", filter, opts)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range values {
		result = append(result, v.(string))
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) GetUniqCategoriesByBrand(brand string) ([]string, error) {
	result := []string{}
	// ищем
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	filter := bson.M{"proizvoditel": primitive.Regex{
		Pattern: brandRegex,
		Options: "i",
	}}
	opts := options.Distinct().SetMaxTime(4 * time.Second)

	values, err := c.collection.Distinct(context.TODO(), "Categories", filter, opts)
	if err != nil {
		return nil, err
	}

	for _, value := range values {
		result = append(result, value.(string))
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) GetUniqCategoriesForSiteByBrand(brand string) ([]entity.Category, error) {

	result := []entity.Category{}
	// ищем
	brandRegex := "^" + regexp.QuoteMeta(brand) + "$"

	filter := bson.M{"proizvoditel": primitive.Regex{
		Pattern: brandRegex,
		Options: "i",
	}}
	opts := options.Distinct().SetMaxTime(4 * time.Second)

	values, err := c.collection.Distinct(context.TODO(), "categoriesForSite", filter, opts)
	if err != nil {
		return nil, err
	}

	for _, value := range values {
		m := value.(primitive.D).Map()
		cat := entity.Category{}
		if name, ok := m["name"].(string); ok {
			cat.Name = name
		}
		if url, ok := m["url"].(string); ok {
			cat.URL = url
		}
		result = append(result, cat)
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) GetUniqCategoriesForSiteByQuery(query bson.M) ([]entity.Category, error) {

	result := []entity.Category{}
	// ищем
	opts := options.Distinct().SetMaxTime(4 * time.Second)

	values, err := c.collection.Distinct(context.TODO(), "categoriesForSite", query, opts)
	if err != nil {
		return nil, err
	}

	for _, value := range values {
		m := value.(primitive.D).Map()
		cat := entity.Category{}
		if name, ok := m["name"].(string); ok {
			cat.Name = name
		}
		if url, ok := m["url"].(string); ok {
			cat.URL = url
		}
		result = append(result, cat)
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

// TODO CHECK не будет ли проблем со множественными свойствами
func (c *MongoConnection) GetSvoystvaByQueryForSiteFilter(query bson.M) ([]entity.Svoystvo, error) {

	var result []entity.Svoystvo

	filter := query

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},
		bson.D{
			{"$project", bson.D{
				{"arrayofkeyvalue", bson.D{
					{"$objectToArray", "$$ROOT.svoystvaSiteFilter"},
				}},
			}},
		},
		bson.D{
			{"$unwind", bson.D{
				{"path", "$arrayofkeyvalue"},
			}},
		},
		bson.D{
			{"$group", bson.D{
				{"_id", "$arrayofkeyvalue.k"},
				{"znacheniyaValues", bson.D{
					{"$addToSet", "$arrayofkeyvalue.v"},
				}},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(120 * time.Second)
	opts.SetAllowDiskUse(true)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return nil, err
	}

	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		return nil, err
	}
	//log.Println("results", results)

	for _, v := range results {
		encountered := map[interface{}]bool{}
		sliceValues := v["znacheniyaValues"].(primitive.A)
		for _, val := range sliceValues {
			if val != "" {
				switch val.(type) {
				case string:
					encountered[val] = true
				case primitive.A:
					//fmt.Println("primitive.A:", v)
					sliceValues2 := val.(primitive.A)
					for k, val2 := range sliceValues2 {
						if val2 != nil {
							encountered[sliceValues2[k]] = true
						}

					}
				case float64:
					encountered[val] = true
				default:
					encountered[val] = true
					fmt.Println("unknown GetSvoystvaByQueryForSiteFilter")
				}
			}
		}
		var arrayUniqValues []interface{}
		for key, _ := range encountered {
			arrayUniqValues = append(arrayUniqValues, key)
		}
		result = append(result, entity.Svoystvo{
			NameForSite:      v["_id"].(string),
			ZnacheniyaValues: arrayUniqValues,
		})
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

func (c *MongoConnection) GetSvoystvaByQuery(query bson.M) ([]entity.Svoystvo, error) {
	var result []entity.Svoystvo

	filter := query

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},
		bson.D{
			{"$project", bson.D{
				{"arrayofkeyvalue", bson.D{
					{"$objectToArray", "$$ROOT.Svoystva"},
				}},
			}},
		},
		bson.D{
			{"$unwind", bson.D{
				{"path", "$arrayofkeyvalue"},
			}},
		},
		bson.D{
			{"$group", bson.D{
				{"_id", "$arrayofkeyvalue.k"},
				{"znacheniyaValues", bson.D{
					{"$addToSet", "$arrayofkeyvalue.v"},
				}},
			}},
		},
	}

	pipelineExt2 := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},
		bson.D{
			{"$project", bson.D{
				{"arrayofkeyvalue", bson.D{
					{"$objectToArray", "$$ROOT.svoystvaMnozhestvo"},
				}},
			}},
		},
		bson.D{
			{"$unwind", bson.D{
				{"path", "$arrayofkeyvalue"},
			}},
		},
		bson.D{
			{"$group", bson.D{
				{"_id", "$arrayofkeyvalue.k"},
				{"znacheniyaValues", bson.D{
					{"$addToSet", "$arrayofkeyvalue.v"},
				}},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(120 * time.Second)
	opts.SetAllowDiskUse(true)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return nil, err
	}

	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		return nil, err
	}
	//log.Println("results", results)

	for _, v := range results {
		result = append(result, entity.Svoystvo{
			Name:             "Svoystva." + v["_id"].(string),
			NameForSite:      v["_id"].(string),
			ZnacheniyaValues: v["znacheniyaValues"].(primitive.A),
		})
	}

	cursor, err = c.collection.Aggregate(
		context.TODO(),
		pipelineExt2,
		opts)
	if err != nil {
		return nil, err
	}

	if err = cursor.All(context.TODO(), &results); err != nil {
		return nil, err
	}

	for _, v := range results {
		encountered := map[string]bool{}
		sliceValues := v["znacheniyaValues"].(primitive.A)
		for _, val := range sliceValues {
			if val != "" {
				switch val.(type) {
				case string:
					encountered[val.(string)] = true
				case primitive.A:
					//fmt.Println("primitive.A:", v)
					sliceValues2 := val.(primitive.A)
					for k, val2 := range sliceValues2 {
						if val2 != nil {
							encountered[sliceValues2[k].(string)] = true
						}

					}
				default:
					fmt.Println("unknown")
				}
			}
		}
		var arrayUniqValues []interface{}
		for key, _ := range encountered {
			arrayUniqValues = append(arrayUniqValues, key)
		}
		result = append(result, entity.Svoystvo{
			Name:             "svoystvaMnozhestvo." + v["_id"].(string),
			NameForSite:      v["_id"].(string),
			ZnacheniyaValues: arrayUniqValues,
		})
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

// Возвращают получить уникальные значения конкретного свойства из всей коллекции
func (c *MongoConnection) GetUniqValues(nameField string) ([]string, error) {
	var result []string

	filter := bson.M{}
	opts := options.Distinct().SetMaxTime(4 * time.Second)
	values, err := c.collection.Distinct(context.TODO(), nameField, filter, opts)
	if err != nil {
		return nil, err
	}

	for _, v := range values {
		if v != nil {
			switch v.(type) {
			case string:
				result = append(result, v.(string))
			case float64:
				result = append(result, strconv.FormatFloat(v.(float64), 'f', -1, 64))
			}
		}
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

// Возвращают получить уникальные значения конкретного свойства по выборке товаров из запроса
func (c *MongoConnection) GetUniqValuesFromQuery(query interface{}, nameField string) ([]string, error) {
	var result []string

	filter := query
	opts := options.Distinct().SetMaxTime(4 * time.Second)
	values, err := c.collection.Distinct(context.TODO(), nameField, filter, opts)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range values {
		result = append(result, v.(string))
	}

	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, entity.ErrNotFound
	default:
		return nil, err
	}
}

// Возвращают количество документов найденых в ЗАДАННОМ запросе
func (c *MongoConnection) CountDocsFromQuery(query interface{}) (int, error) {

	result, err := c.collection.CountDocuments(context.TODO(), query)

	switch err {
	case nil:
		return int(result), nil
	case mongo.ErrNoDocuments:
		return 0, entity.ErrNotFound
	default:
		return 0, err
	}
}

func (c *MongoConnection) SetYaMarketAvailable(mongoId primitive.ObjectID, status bool) error {
	// Query
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"availableYaMarket": status}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) SetAvailableAvito(mongoId primitive.ObjectID, status bool) error {
	// Query
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"availableAvito": status}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление цены яндекс маркета: минимальная, средняя, максимальная
func (c *MongoConnection) SetYaMarketPrices(bitrixId string, minPrice, maxPrice, avgPrice float32) error {
	if bitrixId == "" {
		return entity.ErrInputValueNil
	}

	query := bson.M{"idBitrix": bitrixId}
	update := bson.M{"$set": bson.M{"priceMinYaMarket": float64(minPrice), "priceMaxYaMarket": float64(maxPrice), "priceAvgYaMarket": float64(avgPrice)}}

	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// обнуляет цены яндекс маркета
func (c *MongoConnection) SetYaMarketPricesNull() error {
	query := bson.M{"priceAvgYaMarket": bson.M{"$exists": true}}
	update := bson.M{"$set": bson.M{"priceMinYaMarket": 0.00, "priceMaxYaMarket": 0.00, "priceAvgYaMarket": 0.00}}

	_, err := c.collection.UpdateMany(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление остатка склада у товара
func (c *MongoConnection) SetSkladOstatok(mongoId primitive.ObjectID, ostatok float64) error {

	// Query
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"sklad": ostatok}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) SetPriceOptPostavschik(mongoId primitive.ObjectID, nameOptPrice string, price float64) error {
	currentTime := time.Now()

	// Ищем документ по ID
	query := bson.M{"_id": mongoId}

	// Проверяем, существует ли элемент с указанным nameTranslitPostav в массиве pricePostavkaOpt
	filter := bson.M{
		"_id": mongoId,
		"pricePostavkaOpt": bson.M{
			"$elemMatch": bson.M{
				"nameTranslitPostav": nameOptPrice,
			},
		},
	}

	// Если элемент существует, обновляем его
	update := bson.M{
		"$set": bson.M{
			"pricePostavkaOpt.$[elem].value":       price,
			"pricePostavkaOpt.$[elem].updatedTime": currentTime,
		},
	}

	arrayFilters := options.ArrayFilters{
		Filters: []interface{}{
			bson.M{"elem.nameTranslitPostav": nameOptPrice},
		},
	}

	opts := options.Update().SetArrayFilters(arrayFilters)

	// Пытаемся обновить существующий элемент
	result, err := c.collection.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		return err
	}

	// Если ни один документ не был обновлен, добавляем новый элемент в массив
	if result.MatchedCount == 0 {
		newPriceInfo := entity.PriceInfo{
			NameTranslitPostav: nameOptPrice,
			Value:              price,
			UpdatedTime:        &currentTime,
		}

		update = bson.M{
			"$push": bson.M{
				"pricePostavkaOpt": newPriceInfo,
			},
		}

		_, err = c.collection.UpdateOne(context.TODO(), query, update)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *MongoConnection) SetPriceRoznPostavschik(mongoId primitive.ObjectID, nameRoznPrice string, price float64) error {
	currentTime := time.Now()

	// Ищем документ по ID
	query := bson.M{"_id": mongoId}

	// Проверяем, существует ли элемент с указанным nameTranslitPostav в массиве pricePostavkaRozn
	filter := bson.M{
		"_id": mongoId,
		"pricePostavkaRozn": bson.M{
			"$elemMatch": bson.M{
				"nameTranslitPostav": nameRoznPrice,
			},
		},
	}

	// Если элемент существует, обновляем его
	update := bson.M{
		"$set": bson.M{
			"pricePostavkaRozn.$[elem].value":       price,
			"pricePostavkaRozn.$[elem].updatedTime": currentTime,
		},
	}

	arrayFilters := options.ArrayFilters{
		Filters: []interface{}{
			bson.M{"elem.nameTranslitPostav": nameRoznPrice},
		},
	}

	opts := options.Update().SetArrayFilters(arrayFilters)

	// Пытаемся обновить существующий элемент
	result, err := c.collection.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		return err
	}

	// Если ни один документ не был обновлен, добавляем новый элемент в массив
	if result.MatchedCount == 0 {
		newPriceInfo := entity.PriceInfo{
			NameTranslitPostav: nameRoznPrice,
			Value:              price,
			UpdatedTime:        &currentTime,
		}

		update = bson.M{
			"$push": bson.M{
				"pricePostavkaRozn": newPriceInfo,
			},
		}

		_, err = c.collection.UpdateOne(context.TODO(), query, update)
		if err != nil {
			return err
		}
	}

	return nil
}

// обнуляет цены оптовые и розничные поставщиков
func (c *MongoConnection) SetNullPricesPostavschik(mongoId primitive.ObjectID, nameRoznPrice, nameOptPrice string) error {
	nul := 0.00
	queryOptPrice := "pricePostavkaOpt." + nameOptPrice
	queryRoznPrice := "pricePostavkaRozn." + nameRoznPrice

	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{queryOptPrice: nul}}
	update1 := bson.M{"$set": bson.M{queryRoznPrice: nul}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)
	_, err = c.collection.UpdateOne(context.TODO(), query, update1)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// обнуляет склад поставщика
func (c *MongoConnection) SetNullSkladPostavschik(mongoId primitive.ObjectID, skladName string) error {
	nul := 0.00

	querySklad := "skladPostavka." + skladName
	// ищем

	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{querySklad: nul}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// ставим остаток на склад поставщика
func (c *MongoConnection) SetSkladPostavschik(mongoId primitive.ObjectID, skladName string, ostatok float64) error {
	currentTime := time.Now()

	// Ищем документ по ID
	query := bson.M{"_id": mongoId}

	// Проверяем, существует ли элемент с указанным nameTranslitPostav в массиве skladPostavka
	filter := bson.M{
		"_id": mongoId,
		"skladPostavka": bson.M{
			"$elemMatch": bson.M{
				"nameTranslitPostav": skladName,
			},
		},
	}

	// Если элемент существует, обновляем его
	update := bson.M{
		"$set": bson.M{
			"skladPostavka.$[elem].value":       ostatok,
			"skladPostavka.$[elem].updatedTime": currentTime,
		},
	}

	arrayFilters := options.ArrayFilters{
		Filters: []interface{}{
			bson.M{"elem.nameTranslitPostav": skladName},
		},
	}

	opts := options.Update().SetArrayFilters(arrayFilters)

	// Пытаемся обновить существующий элемент
	result, err := c.collection.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		return err
	}

	// Если ни один документ не был обновлен, добавляем новый элемент в массив
	if result.MatchedCount == 0 {
		newSkladInfo := entity.SkladInfo{
			NameTranslitPostav: skladName,
			Value:              ostatok,
			UpdatedTime:        &currentTime,
		}

		update = bson.M{
			"$push": bson.M{
				"skladPostavka": newSkladInfo,
			},
		}

		_, err = c.collection.UpdateOne(context.TODO(), query, update)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *MongoConnection) SetSvoystvaForSite(mongoId primitive.ObjectID, svoystvaSiteFilter, svoystvaSiteHaract bson.M) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"svoystvaSiteFilter": svoystvaSiteFilter, "svoystvaSiteHaract": svoystvaSiteHaract}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) SetCollections(mongoId primitive.ObjectID, collections []string) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"collections": collections}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление маржу
func (c *MongoConnection) SetMarzha(mongoId primitive.ObjectID, marzha float64) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"marzha": marzha}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление максимальной и минимальной маржы
func (c *MongoConnection) SetMarzhaMaxAndMin(mongoId primitive.ObjectID, maxMarzha, minMarzha float64) error {

	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"marzhaMax": maxMarzha, "marzhaMin": minMarzha}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление price у товара
func (c *MongoConnection) SetPrice(mongoId primitive.ObjectID, price float64) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"price": price, "updatedPriceAt": time.Now()}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление kursPrice у товара
func (c *MongoConnection) SetKursPrice(mongoId primitive.ObjectID, kurs float64) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"kursPrice": kurs, "updatedTime": time.Now()}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) SetValuta(mongoId primitive.ObjectID, valuta string) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"valutaPrice": valuta, "updatedTime": time.Now()}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) SetPriceRecommend(mongoId primitive.ObjectID, price float64) error {
	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{"priceRecommend": price, "updatedTime": time.Now()}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление price у товара у которого совпадает URL
func (c *MongoConnection) SetPriceByUrl(url string, price float64) error {
	if url == "" {
		return entity.ErrInputValueNil
	}

	query := bson.M{"url": url}
	update := bson.M{"$set": bson.M{"price": price, "updatedPriceAt": time.Now()}}

	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// устанавление цен конкурентов
func (c *MongoConnection) SetPriceKonkur(mongoId primitive.ObjectID, dbCollection string, cena float64) error {
	nameCena := "pricesKonkur." + dbCollection

	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{nameCena: cena}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// ставим значение соответствия для поиска товара конкрентного поставщика
func (c *MongoConnection) SetSootvetstviePostavschik(mongoId primitive.ObjectID, namePostavshcik, sootvetstvieValue string) error {
	sootvetstvieValue = strings.TrimSpace(sootvetstvieValue)
	if sootvetstvieValue == "" {
		return entity.ErrInputValueNil
	}

	nameField := "sootvetstviePostavschik." + namePostavshcik

	query := bson.M{"_id": mongoId}
	update := bson.M{"$set": bson.M{nameField: sootvetstvieValue}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// обнуляет соответствие конкретного поставщика
func (c *MongoConnection) RemoveSootvetstviePostavschikByVnutrId(idVnutr, namePostavshcik string) error {
	nameField := "sootvetstviePostavschik." + namePostavshcik

	query := bson.M{"idVnutr": idVnutr}
	update := bson.M{"$unset": bson.M{nameField: ""}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) RemoveSootvetstviePostavschik(idVnutr string) error {
	nameField := "sootvetstviePostavschik"

	query := bson.M{"idVnutr": idVnutr}
	update := bson.M{"$unset": bson.M{nameField: ""}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) RemoveField(mongoId primitive.ObjectID, nameField string) error {

	query := bson.M{"_id": mongoId}
	update := bson.M{"$unset": bson.M{nameField: ""}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// ставим значение соответствия для поиска товара конкрентного поставщика
func (c *MongoConnection) SetSootvetstviePostavschikByVnutrId(vnutrId string, sootvetstvie entity.Sootvetstvie) error {
	sootvetstvieValue := strings.TrimSpace(sootvetstvie.Value)
	if sootvetstvieValue == "" {
		return entity.ErrInputValueNil
	}

	//nameField := "sootvetstviePostavschik." + sootvetstvie.NameTranslitPostav

	//query := bson.M{"idVnutr": vnutrId}
	//update := bson.M{"$set": bson.M{nameField: sootvetstvieValue}}
	//update
	//_, err := c.collection.UpdateOne(context.TODO(), query, update)

	// Подготовка значения для обновления
	//updateValue := bson.M{
	//"type":               sootvetstvie.Type,
	//"nameTranslitPostav": sootvetstvie.NameTranslitPostav,
	//"field":              sootvetstvie.Field,
	//"value":              sootvetstvieValue,
	//}

	// Строка для поиска нужного элемента в массиве Sootvetstviya
	match := bson.M{
		"idVnutr": vnutrId,
		"sootvetstviya": bson.M{
			"$elemMatch": bson.M{
				"type":               sootvetstvie.Type,
				"nameTranslitPostav": sootvetstvie.NameTranslitPostav,
				"field":              sootvetstvie.Field,
			},
		},
	}
	//match := bson.M{
	//"idVnutr": vnutrId,
	//}

	// Запрос для добавления или обновления элемента массива Sootvetstviya
	update := bson.M{
		"$addToSet": bson.M{"sootvetstviya": sootvetstvie},
	}
	// Запрос для добавления элемента массива Sootvetstviya, если его еще нет
	//update := bson.M{
	//"$push": bson.M{
	//"sootvetstviya": bson.M{
	//"$each": []bson.M{updateValue}, // Добавление конкретного элемента
	//},
	//},
	//}

	// Обновление
	result, err := c.collection.UpdateOne(context.TODO(), match, update)
	switch err {
	case nil:
		if result.ModifiedCount == 0 {
			_, err := c.collection.UpdateOne(context.TODO(), bson.M{"idVnutr": vnutrId}, update)
			if err != nil {
				return err
			}
		}
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) SetSootvetstviePostavschikByNameBrand(name, brand, namePostavshcik, sootvetstvieValue string) error {
	sootvetstvieValue = strings.TrimSpace(sootvetstvieValue)
	if sootvetstvieValue == "" {
		return entity.ErrInputValueNil
	}

	nameField := "sootvetstviePostavschik." + namePostavshcik

	query := bson.M{"name": name, "proizvoditel": brand}
	update := bson.M{"$set": bson.M{nameField: sootvetstvieValue}}

	//update
	_, err := c.collection.UpdateOne(context.TODO(), query, update)

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

func (c *MongoConnection) ExportCollection2Csv(path string) error {

	// создаем файл экспорта
	filename := path + c.collection.Name() + ".csv"
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Create Writer
	writer := csv.NewWriter(file)

	//получаем все хедеры
	headers, err := c.GetHeadersAll()
	if err != nil {
		return errors.New("ERROR get headers:" + err.Error())
	}

	headersSvoystva, err := c.GetHeadersSvoystva()
	if err != nil {
		return errors.New("ERROR get headers svoystva:" + err.Error())
	}

	// write headers to file
	headers = append(headers, headersSvoystva...)
	writer.Write(headers)
	writer.Flush()

	cursor, err := c.collection.Find(context.TODO(), bson.M{})

	// Iterate through the returned cursor.
	count := 0
	for cursor.Next(context.TODO()) {
		var m bson.M
		cursor.Decode(&m)
		//Создаем карту куда будем анмаршалить данные из монги
		var flattened = make(map[string]interface{})
		var record []string
		flatten(m, "", &flattened)
		for _, header := range headers {
			znachenie := fmt.Sprint(flattened[header])
			if znachenie == "<nil>" {
				znachenie = " "
			}
			record = append(record, znachenie)
		}
		writer.Write(record)
		writer.Flush()
		count++
	}

	if cursor.Err() != nil {
		cursor.Close(context.TODO())
	}

	fmt.Printf("%d record(s) exported\n", count)
	return err
}

func (c *MongoConnection) GetHeadersAll() ([]string, error) {
	var result []string

	filter := bson.M{}

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},
		bson.D{
			{"$project", bson.D{
				{"arrayofkeyvalue", bson.D{
					{"$objectToArray", "$$ROOT"},
				}},
			}},
		},
		bson.D{
			{"$unwind", bson.D{
				{"path", "$arrayofkeyvalue"},
			}},
		},
		bson.D{
			{"$group", bson.D{
				{"_id", "$arrayofkeyvalue.k"},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(60 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	type FromAggrigate struct {
		Name string `bson:"_id"`
	}
	var results []FromAggrigate
	if err = cursor.All(context.TODO(), &results); err != nil {
		return result, err
	}
	for _, s := range results {
		result = append(result, s.Name)
	}

	sort.Strings(result)
	return result, err
}

// возвращает уникальные все возможные хидеры свойств товаров коллекции
func (c *MongoConnection) GetHeadersSvoystva() ([]string, error) {
	var result []string

	filter := bson.M{}

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},
		bson.D{
			{"$project", bson.D{
				{"arrayofkeyvalue", bson.D{
					{"$objectToArray", "$$ROOT.Svoystva"},
				}},
			}},
		},
		bson.D{
			{"$unwind", bson.D{
				{"path", "$arrayofkeyvalue"},
			}},
		},
		bson.D{
			{"$group", bson.D{
				{"_id", "$arrayofkeyvalue.k"},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(60 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	type FromAggrigate struct {
		Name string `bson:"_id"`
	}
	var results []FromAggrigate
	if err = cursor.All(context.TODO(), &results); err != nil {
		return result, err
	}
	for _, s := range results {
		result = append(result, "Svoystva."+s.Name)
	}

	sort.Strings(result)
	return result, err

}

// возвращает уникальные все возможные хидеры свойств товаров коллекции из запроса
func (c *MongoConnection) GetHeadersSvoystvaFromQuery(query interface{}) ([]string, error) {
	var result []string

	filter := query

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},
		bson.D{
			{"$project", bson.D{
				{"arrayofkeyvalue", bson.D{
					{"$objectToArray", "$$ROOT.Svoystva"},
				}},
			}},
		},
		bson.D{
			{"$unwind", bson.D{
				{"path", "$arrayofkeyvalue"},
			}},
		},
		bson.D{
			{"$group", bson.D{
				{"_id", "$arrayofkeyvalue.k"},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(4 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	type FromAggrigate struct {
		Name string `bson:"_id"`
	}
	var results []FromAggrigate
	if err = cursor.All(context.TODO(), &results); err != nil {
		return result, err
	}
	for _, s := range results {
		result = append(result, "Svoystva."+s.Name)
	}

	sort.Strings(result)
	return result, err
}

type DublesAgregate struct {
	Id        IdStruct             `bson:"_id"`
	UniqueIds []primitive.ObjectID `bson:"uniqueIds"`
	Count     int                  `bson:"count,omitempty"`
}

type IdStruct struct {
	Articul      string `bson:"articul,omitempty"`
	Proizvoditel string `bson:"proizvoditel,omitempty"`
}

// ищет дубли товаров по артикулу и производитлею
func (c *MongoConnection) FindDublByArtBrand() ([]DublesAgregate, error) {
	result := []DublesAgregate{}

	filter := bson.D{}
	groupStage := bson.D{
		//_id обязательное поле группировки и по этим свойствам документы мы группируем
		primitive.E{Key: "_id", Value: bson.D{
			primitive.E{Key: "articul", Value: "$articul"},
			primitive.E{Key: "proizvoditel", Value: "$proizvoditel"},
		}},
		// Мы создаем массив из ID документов из которых состоят созданые группы

		primitive.E{Key: "uniqueIds", Value: primitive.E{Key: "$addToSet", Value: "$_id"}},
		// Тут мы выводим показатель где бдует количество документов в группе
		primitive.E{Key: "count", Value: primitive.E{Key: "$sum", Value: 1}},
	}

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},

		//primitive.E{Key: "$group", Value: groupStage},
		bson.D{
			{"$group", groupStage},
		},

		bson.D{
			primitive.E{"$match", bson.D{ // тут мы выводим только те группы в которых документов в группе больше 1
				primitive.E{Key: "count", Value: bson.D{primitive.E{"$gt", 1.0}}},
			},
			}},

		//а тут мы просто сортируем группы по порядку убывания количества документов в группе
		bson.D{
			primitive.E{"$sort", bson.D{
				primitive.E{"count", -1},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(4 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		return result, err
	}

	return result, err
}

// ищет дубли товаров по внутреннемуId
func (c *MongoConnection) FindDublByidVnutr() ([]DublesAgregate, error) {
	result := []DublesAgregate{}

	filter := bson.D{}

	pipelineExt := bson.A{
		//тут мы берем выборку документов по которым делать агрегацию
		bson.D{{"$match", filter}},
		bson.D{
			{"$group",
				bson.D{
					//_id обязательное поле группировки и по этим свойствам документы мы группируем
					{"_id", bson.D{{"idVnutr", "$idVnutr"}}},
					// Мы создаем массив из ID документов из которых состоят созданые группы
					{"uniqueIds", bson.D{{"$addToSet", "$_id"}}},
					// Тут мы выводим показатель где бдует количество документов в группе
					{"count", bson.D{{"$sum", 1}}},
				},
			},
		},
		// тут мы выводим только те группы в которых документов в группе больше 1
		bson.D{{"$match", bson.D{{"count", bson.D{{"$gt", 1}}}}}},
		//а тут мы просто сортируем группы по порядку убывания количества документов в группе
		bson.D{{"$sort", bson.D{{"count", -1}}}},
	}

	opts := options.Aggregate().SetMaxTime(4 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		return result, err
	}

	return result, err
}

func (c *MongoConnection) FindDublByQuery(query bson.D) ([]DublesAgregate, error) {
	result := []DublesAgregate{}

	filter := bson.D{}

	pipelineExt := bson.A{
		//тут мы берем выборку документов по которым делать агрегацию
		bson.D{{"$match", filter}},
		bson.D{
			{"$group",
				bson.D{
					//_id обязательное поле группировки и по этим свойствам документы мы группируем
					{"_id", query},
					// Мы создаем массив из ID документов из которых состоят созданые группы
					{"uniqueIds", bson.D{{"$addToSet", "$_id"}}},
					// Тут мы выводим показатель где бдует количество документов в группе
					{"count", bson.D{{"$sum", 1}}},
				},
			},
		},
		// тут мы выводим только те группы в которых документов в группе больше 1
		bson.D{{"$match", bson.D{{"count", bson.D{{"$gt", 1}}}}}},
		//а тут мы просто сортируем группы по порядку убывания количества документов в группе
		bson.D{{"$sort", bson.D{{"count", -1}}}},
	}

	opts := options.Aggregate().SetMaxTime(4 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		return result, err
	}

	return result, err
}

func (c *MongoConnection) FindDublByName() ([]DublesAgregate, error) {
	result := []DublesAgregate{}

	filter := bson.D{}
	groupStage := bson.D{
		//_id обязательное поле группировки и по этим свойствам документы мы группируем
		primitive.E{Key: "_id", Value: bson.D{
			primitive.E{Key: "name", Value: "$name"},
		}},
		// Мы создаем массив из ID документов из которых состоят созданые группы

		primitive.E{Key: "uniqueIds", Value: primitive.E{Key: "$addToSet", Value: "$_id"}},
		// Тут мы выводим показатель где бдует количество документов в группе
		primitive.E{Key: "count", Value: primitive.E{Key: "$sum", Value: 1}},
	}

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},

		//primitive.E{Key: "$group", Value: groupStage},
		bson.D{
			{"$group", groupStage},
		},

		bson.D{
			primitive.E{"$match", bson.D{ // тут мы выводим только те группы в которых документов в группе больше 1
				primitive.E{Key: "count", Value: bson.D{primitive.E{"$gt", 1.0}}},
			},
			}},

		//а тут мы просто сортируем группы по порядку убывания количества документов в группе
		bson.D{
			primitive.E{"$sort", bson.D{
				primitive.E{"count", -1},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(4 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		return result, err
	}

	return result, err
}

// ищем дубли по модели и бренду
func (c *MongoConnection) FindDublByModelBrand() ([]DublesAgregate, error) {
	result := []DublesAgregate{}

	filter := bson.D{}
	groupStage := bson.D{
		//_id обязательное поле группировки и по этим свойствам документы мы группируем
		primitive.E{Key: "_id", Value: bson.D{
			primitive.E{Key: "model", Value: "$model"},
			primitive.E{Key: "proizvoditel", Value: "$proizvoditel"},
		}},
		// Мы создаем массив из ID документов из которых состоят созданые группы

		primitive.E{Key: "uniqueIds", Value: primitive.E{Key: "$addToSet", Value: "$_id"}},
		// Тут мы выводим показатель где бдует количество документов в группе
		primitive.E{Key: "count", Value: primitive.E{Key: "$sum", Value: 1}},
	}

	pipelineExt := mongo.Pipeline{
		bson.D{
			{"$match", filter},
		},

		//primitive.E{Key: "$group", Value: groupStage},
		bson.D{
			{"$group", groupStage},
		},

		bson.D{
			primitive.E{"$match", bson.D{ // тут мы выводим только те группы в которых документов в группе больше 1
				primitive.E{Key: "count", Value: bson.D{primitive.E{"$gt", 1.0}}},
			},
			}},

		//а тут мы просто сортируем группы по порядку убывания количества документов в группе
		bson.D{
			primitive.E{"$sort", bson.D{
				primitive.E{"count", -1},
			}},
		},
	}

	opts := options.Aggregate().SetMaxTime(4 * time.Second)

	cursor, err := c.collection.Aggregate(
		context.TODO(),
		pipelineExt,
		opts)
	if err != nil {
		return result, err
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		return result, err
	}

	return result, err
}

func (c *MongoConnection) FindCategoryByUrl(url string) (*entity.Category, error) {
	url = strings.TrimSpace(url)
	if url == "" {
		return nil, errors.New("Input value is empty")
	}
	// создаем новый документ
	result := entity.Category{}

	err := c.collection.FindOne(context.TODO(), bson.M{"url": url}).Decode(&result)

	switch err {
	case nil:
		return &result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.New("category not found")
	default:
		return nil, err
	}
}

func (c *MongoConnection) UpdateCategoryByUrl(category *entity.Category) (err error) {
	if category.URL == "" {
		return errors.New("Input url is empty")
	}

	query := bson.M{"url": category.URL}
	update := bson.M{"$set": category}

	_, err = c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))

	switch err {
	case nil:
		return nil
	case mongo.ErrNoDocuments:
		return entity.ErrNotFound
	default:
		return err
	}
}

// распидарасиваем bson Монги в карту [string]interface, для выгрузки в CSV, т.к массив соединяет через | в одну переменную
func flatten(input bson.M, lkey string, flattened *map[string]interface{}) {
	for rkey, value := range input {
		key := lkey + rkey
		switch v := value.(type) {
		case string:
			(*flattened)[key] = v
		case float64:
			(*flattened)[key] = strconv.FormatFloat(v, 'f', 2, 64)
		case int:
			(*flattened)[key] = v
		case int32:
			(*flattened)[key] = v
		case int64:
			(*flattened)[key] = v
		case bool:
			(*flattened)[key] = v
		case primitive.DateTime:
			(*flattened)[key] = v.Time().Format("2006-01-02T15:04:05Z07:00")
		case time.Time:
			(*flattened)[key] = v.Format("2006-01-02T15:04:05Z07:00")
		case primitive.ObjectID:
			(*flattened)[key] = v.Hex()
		case primitive.A:
			sliceValues := v
			allvalues := []string{}
			for k := range sliceValues {
				if sliceValues[k] != nil {
					switch sliceValues[k].(type) {
					case string:
						allvalues = append(allvalues, sliceValues[k].(string))
					case primitive.M:
						// Handle nested primitive.M if necessary
					}
				}
				(*flattened)[key] = strings.Join(allvalues, "|")
			}
		case []interface{}:
			allvalues := []string{}
			for i := 0; i < len(v); i++ {
				val := fmt.Sprintf("%+v", v[i])
				allvalues = append(allvalues, val)
				count := len(v) - 1
				if i == count {
					(*flattened)[key] = strings.Join(allvalues, "|")
				}
				if _, ok := v[i].(bson.M); ok {
					flatten(v[i].(bson.M), key+"."+strconv.Itoa(i)+".", flattened)
				}
			}
		case bson.M:
			flatten(v, key+".", flattened)
		default:
			if value != nil {
				fmt.Println("Encountered unsupported type:", key, "-->", value)
				(*flattened)[key] = fmt.Sprintf("%v", value)
			} else {
				(*flattened)[key] = ""
			}
		}
	}
}

// получает названия полей из bson
func getHeadersFromBson(inputBson bson.M) []string {
	var flattened = make(map[string]interface{})
	var headers []string
	// Auto Detect Headerline
	flatten(inputBson, "", &flattened)
	for key := range flattened {
		headers = append(headers, key)
	}
	// Default sort the headers
	// Otherwise accessing the headers will be
	// different each time.
	sort.Strings(headers)
	return headers
}

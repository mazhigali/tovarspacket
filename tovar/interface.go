package tovar

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByArticul(articul string) (*entity.Tovar, error)
	FindByArticulMore(articul string) (*entity.Tovar, error)
	FindByName(name string) (*entity.Tovar, error)
	FindByModel(name string) (*entity.Tovar, error)
	FindByNametranslit(name string) (*entity.Tovar, error)
	FindByVnutrID(idVnutr string) (*entity.Tovar, error)
	FindByExtID(id string) (*entity.Tovar, error)
	FindByEan(ean string) (*entity.Tovar, error)
	FindByUrl(url string) (*entity.Tovar, error)
	FindByEanMore(ean string) (*entity.Tovar, error)
	FindByIdBitrix(idBitrix string) (*entity.Tovar, error)
	FindByID(id string) (*entity.Tovar, error)
	FindByArticulAndBrand(articul, brand string) (*entity.Tovar, error)
	FindByArtAndName(articul, name string) (*entity.Tovar, error)
	FindByModelAndBrand(model, brand string) (*entity.Tovar, error)
	FindByNameAndBrand(name, brand string) (*entity.Tovar, error)
	FindByArticulMoreAndBrand(articul, brand string) (*entity.Tovar, error)
	FindBySootvetstviePostavshik(namePostavshcik, nameForSearch string) (*entity.Tovar, error)
	FindTovarByQuery(query bson.M) (*entity.Tovar, error)
	FindByEanAndBrand(model, brand string) (*entity.Tovar, error)
	FindByEanMoreAndBrand(model, brand string) (*entity.Tovar, error)
	FindPartInNameAndBrand(partInName, brand string) (*entity.Tovar, error)
	FindByPartInName(partInName string) (*entity.Tovar, error)
	FindAll() ([]entity.Tovar, error)
	FindAllByBrand(brand string) ([]entity.Tovar, error)
	FindAllByBrandAndCategory(brand, category string) ([]entity.Tovar, error)
	FindAllByBrandAndCategoryUrl(brand, categoryUrl string) ([]entity.Tovar, error)
	FindAllByCollection(collectionVnutrId string) ([]entity.Tovar, error)
	FindAllByVnutrIds(vnutrIds []string) ([]entity.Tovar, error)
	FindAllWithArticul() ([]entity.Tovar, error)
	FindAllWithoutArticul() ([]entity.Tovar, error)
	FindAllWithSkladAndImageAndArticul() ([]entity.Tovar, error)
	FindAllWithSkladGTE(skladName string, kolOt int) ([]entity.Tovar, error)
	FindByQuery(query bson.M) ([]entity.Tovar, error)
	FindByQueryByPage(query bson.M, page, perPage int64) (resultArray []entity.Tovar, total int64, lastPage float64, err error)

	GetUniqBrandsAllCollection() ([]string, error)
	GetUniqBrandsWithoutArticul() ([]string, error)
	GetUniqCategoriesByBrand(brand string) ([]string, error)
	GetUniqCategoriesForSiteByBrand(brand string) ([]entity.Category, error)
	GetUniqCategoriesForSiteByQuery(query bson.M) ([]entity.Category, error)
	GetUniqValues(nameField string) ([]string, error)
	GetUniqValuesFromQuery(query interface{}, nameField string) ([]string, error)
	CountDocsFromQuery(query interface{}) (int, error)

	GetSvoystvaByQueryForSiteFilter(query bson.M) ([]entity.Svoystvo, error) // получаем свойства для сайта из svoystvaSiteFilter
	GetSvoystvaByQuery(query bson.M) ([]entity.Svoystvo, error)              // получаем из Svoystva и svoystvaMnozhestvo

	GetHeadersAll() ([]string, error)                                //устарело
	GetHeadersSvoystva() ([]string, error)                           //устарело
	GetHeadersSvoystvaFromQuery(query interface{}) ([]string, error) //устарело

	FindCategoryByUrl(url string) (*entity.Category, error)

	//GetArticulsByQuery(query bson.M) ([]string, error)
}

// Writer tovar writer
type Writer interface {
	AddTovar(t *entity.Tovar) error
	UpdateTovarByIdMongo(tovarMongoDocument *entity.Tovar) error
	UpdateTovarByArtAndName(tovarMongoDocument *entity.Tovar) error
	UpdateTovarByArtBrand(tovarMongoDocument *entity.Tovar) error
	UpdateTovarByModelBrand(tovarMongoDocument *entity.Tovar) error
	UpdateTovarByNameBrand(tovarMongoDocument *entity.Tovar) error
	UpdateTovarByVnutrId(tovarMongoDocument *entity.Tovar) error
	UpdateTovarByExtId(tovarMongoDocument *entity.Tovar) error
	RemoveTovar(tovarMongoDocument *entity.Tovar) error
	RemoveById(mongoId primitive.ObjectID) error
	RemoveByIdVnutr(vnutrId string) error
	RemoveByUrl(url string) error

	SetSkladOstatok(mongoId primitive.ObjectID, ostatok float64) error
	SetSkladPostavschik(mongoId primitive.ObjectID, skladName string, ostatok float64) error
	SetNullSkladPostavschik(mongoId primitive.ObjectID, skladName string) error

	SetPrice(mongoId primitive.ObjectID, price float64) error
	SetKursPrice(mongoId primitive.ObjectID, kurs float64) error
	SetValuta(mongoId primitive.ObjectID, valuta string) error
	SetPriceRecommend(mongoId primitive.ObjectID, price float64) error
	SetPriceByUrl(url string, price float64) error
	SetPriceOptPostavschik(mongoId primitive.ObjectID, nameOptPrice string, price float64) error
	SetPriceRoznPostavschik(mongoId primitive.ObjectID, nameRoznPrice string, price float64) error
	SetNullPricesPostavschik(mongoId primitive.ObjectID, nameRoznPrice, nameOptPrice string) error

	SetSvoystvaForSite(mongoId primitive.ObjectID, svoystvaSiteFilter, svoystvaSiteHaract bson.M) error
	SetCollections(mongoId primitive.ObjectID, collections []string) error

	SetMarzha(mongoId primitive.ObjectID, marzha float64) error
	SetMarzhaMaxAndMin(mongoId primitive.ObjectID, maxMarzha, minMarzha float64) error
	//устанавливает цену конкурентов при парсинге
	SetPriceKonkur(mongoId primitive.ObjectID, dbCollection string, cena float64) error
	SetSootvetstviePostavschik(mongoId primitive.ObjectID, namePostavshcik, sootvetstvieValue string) error
	SetSootvetstviePostavschikByVnutrId(vnutrId string, sootvetstvie entity.Sootvetstvie) error
	SetSootvetstviePostavschikByNameBrand(name, brand, namePostavshcik, sootvetstvieValue string) error
	RemoveSootvetstviePostavschikByVnutrId(idVnutr, namePostavshcik string) error //удаляет конкретные соответствия по имени поставщика
	RemoveSootvetstviePostavschik(idVnutr string) error                           //удаляет все соответствия у товара
	RemoveField(mongoId primitive.ObjectID, nameField string) error

	SetYaMarketPrices(bitrixId string, minPrice, maxPrice, avgPrice float32) error
	SetYaMarketPricesNull() error
	SetYaMarketAvailable(mongoId primitive.ObjectID, status bool) error
	SetAvailableAvito(mongoId primitive.ObjectID, status bool) error

	AddDopArticul(idMongoString, dopArticul string) error

	UpdateCategoryByUrl(category *entity.Category) (err error)

	ExportCollection2Csv(path string) error //TODO проверить можно ли их в TOOLS переместить
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// Tools tools interface
type Tools interface {
	UpdatePricesFromYaMarket(YaMarketToken, YaMarketClientId string) error
	ObnulAkciiByVnutrId() error
	ExportAllWithContent2Xml(filename string) error
	ExportAllWithContent2RetailXml() error
	//Export2XmlLorosa(query bson.M) error

	GetMongoIDsByQuery(query bson.M) ([]string, error)

	RemoveTovarFromSlice(tovars []entity.Tovar, tToDel entity.Tovar) []entity.Tovar // удаляет элемент из массива
	CompareBSONDocuments(doc1, doc2 bson.M)                                         // bool Функция для сравнения двух BSON-документов

}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
	Tools
}

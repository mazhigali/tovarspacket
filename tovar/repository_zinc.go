package tovar

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/mazhigali/tovarspacket/entity"
)

type Response struct {
	Took int
	Hits struct {
		Total struct {
			Value    int
			Relation string
		}
		Max_score float32
		Hits      []struct {
			ID         string          `json:"_id"`
			Source     entity.Tovar    `json:"_source"`
			Highlights json.RawMessage `json:"highlight"`
			Sort       []interface{}   `json:"sort"`
			Score      float32         `json:"_score"`
			Index      string          `json:"_index"`
			Type       string          `json:"_type"`
		}
	}
}

type ResponseWithId struct {
	Took int
	Hits struct {
		Total struct {
			Value    int
			Relation string
		}
		Max_score float32
		Hits      []struct {
			ID         string          `json:"_id"`
			Source     entity.Tovar    `json:"_source"`
			Highlights json.RawMessage `json:"highlight"`
			Sort       []interface{}   `json:"sort"`
			Score      float32         `json:"_score"`
			Index      string          `json:"_index"`
			Type       string          `json:"_type"`
		}
	}
}

type ZinkConnection struct {
	adminName     string
	adminPassword string
	indexName     string
}

type ConfZink struct {
	AdminName     string
	AdminPassword string
	IndexName     string
}

func NewZinkConnection(conf *ConfZink) (conn *ZinkConnection) {
	conn = new(ZinkConnection)

	conn.adminName = conf.AdminName
	conn.adminPassword = conf.AdminPassword
	conn.indexName = conf.IndexName
	return
}

func (c *ZinkConnection) FindByName(name string, minScore float32) (*entity.Tovar, error) {

	name = regexp.QuoteMeta(strings.TrimSpace(name))
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	repl := strings.NewReplacer(
		`"`, `\"`,
		`(`, `\(`,
		`)`, `\)`,
		`.`, `\.`,
		`*`, `\*`,
		`+`, `\+`,
	)
	name = repl.Replace(name)
	//fmt.Println("Sear--", name)

	query := `{
	"query" : {
		"match" : {
			"name" : {
			"query" : "` + name + `",
			"fuzziness": "AUTO",
			"prefix_length": "2",
			"max_expansions": "3"
			}
		}
	}
	}`

	// создаем новый документ
	result := entity.Tovar{}

	req, err := http.NewRequest("POST", "http://localhost:4080/es/"+c.indexName+"/_search", strings.NewReader(query))
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(c.adminName, c.adminPassword)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
		return nil, err
	}
	defer resp.Body.Close()

	//body, err := io.ReadAll(resp.Body)
	//if err != nil {
	//log.Println("Error CreateTovar reading response: %s", err)
	////return err
	//}
	//log.Println(resp.StatusCode)
	//fmt.Println(string(body))
	//if resp.StatusCode != 200 {
	//return fmt.Errorf(string(body))
	//}

	var r Response
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		log.Fatalf("Error decode while ElasticSearch: %s", err)
		return nil, err
	}

	if len(r.Hits.Hits) > 0 && r.Hits.Max_score > minScore {
		result = r.Hits.Hits[0].Source
	}

	switch err {
	case nil:
		return &result, nil
	default:
		return nil, err
	}
}

func (c *ZinkConnection) FindTovarsByName(name string) ([]entity.Tovar, error) {
	// создаем новый документ
	result := []entity.Tovar{}

	name = regexp.QuoteMeta(strings.TrimSpace(name))
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	repl := strings.NewReplacer(
		`"`, `\"`,
		`(`, `\(`,
		`)`, `\)`,
		`.`, `\.`,
		`*`, `\*`,
		`+`, `\+`,
	)
	name = repl.Replace(name)
	//fmt.Println("Sear--", name)

	query := `{
 	"size" : 100,
	"query" : {
		"match" : {
			"name" : {
			"query" : "` + name + `",
			"fuzziness": "AUTO",
			"prefix_length": "2",
			"max_expansions": "3"
			}
		}
	}
	}`

	req, err := http.NewRequest("POST", "http://localhost:4080/es/"+c.indexName+"/_search", strings.NewReader(query))
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(c.adminName, c.adminPassword)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
		return nil, err
	}
	defer resp.Body.Close()

	//body, err := io.ReadAll(resp.Body)
	//if err != nil {
	//log.Println("Error CreateTovar reading response: %s", err)
	////return err
	//}
	//log.Println(resp.StatusCode)
	//fmt.Println(string(body))
	//if resp.StatusCode != 200 {
	//return fmt.Errorf(string(body))
	//}

	var r ResponseWithId
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		log.Fatalf("Error decode while ElasticSearch: %s", err)
		return nil, err
	}

	for _, hit := range r.Hits.Hits {
		//log.Println(hit.Source.Name, hit.Sort, hit.Score, hit.Index, hit.Type)
		result = append(result, hit.Source)
	}

	switch err {
	case nil:
		return result, nil
	default:
		return nil, err
	}
}

func (c *ZinkConnection) FindTovarsByNameTranslit(name string) ([]entity.Tovar, error) {
	// создаем новый документ
	result := []entity.Tovar{}

	name = regexp.QuoteMeta(strings.TrimSpace(name))
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	repl := strings.NewReplacer(
		`"`, `\"`,
		`(`, `\(`,
		`)`, `\)`,
		`.`, `\.`,
		`*`, `\*`,
		`+`, `\+`,
	)
	name = repl.Replace(name)
	//fmt.Println("Sear--", name)

	query := `{
 	"size" : 100,
	"query" : {
		"match" : {
			"nameTranslit" : {
			"query" : "` + name + `",
			"fuzziness": "AUTO",
			"prefix_length": "2",
			"max_expansions": "3"
			}
		}
	}
	}`

	req, err := http.NewRequest("POST", "http://localhost:4080/es/"+c.indexName+"/_search", strings.NewReader(query))
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(c.adminName, c.adminPassword)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
		return nil, err
	}
	defer resp.Body.Close()

	//body, err := io.ReadAll(resp.Body)
	//if err != nil {
	//log.Println("Error CreateTovar reading response: %s", err)
	////return err
	//}
	//log.Println(resp.StatusCode)
	//fmt.Println(string(body))
	//if resp.StatusCode != 200 {
	//return fmt.Errorf(string(body))
	//}

	var r ResponseWithId
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		log.Fatalf("Error decode while ElasticSearch: %s", err)
		return nil, err
	}

	for _, hit := range r.Hits.Hits {
		//log.Println(hit.Source.Name, hit.Sort, hit.Score, hit.Index, hit.Type)
		result = append(result, hit.Source)
	}

	switch err {
	case nil:
		return result, nil
	default:
		return nil, err
	}
}

func (c *ZinkConnection) FindByNameAndBrand(name, brand string, minScore float32) (*entity.Tovar, error) {

	repl := strings.NewReplacer(
		`"`, `\"`,
		`(`, `\(`,
		`)`, `\)`,
		`.`, `\.`,
		`*`, `\*`,
		`+`, `\+`,
	)
	name = regexp.QuoteMeta(strings.TrimSpace(name))
	if name == "" {
		return nil, entity.ErrInputValueNil
	}
	//fmt.Println("Sear--", name)

	name = repl.Replace(name)

	brand = regexp.QuoteMeta(strings.TrimSpace(brand))
	//if brand == "" {
	//return nil, entity.ErrInputValueNil
	//}
	brand = repl.Replace(brand)
	//fmt.Println("Sear--", brand)

	query := fmt.Sprintf(`{
 "size" : 1000,
"query": {
     "bool": {
       "should": [
                {"match": {"name" : {
				"query" : "%s",
				"fuzziness": "AUTO",
				"prefix_length": 2
				}}},
                {"match": {"proizvoditel" : {
				"query" : "%s",
				"fuzziness": "AUTO",
				"prefix_length": 2
				}}}

       ]
     }
   }
	}`, name, brand)

	// создаем новый документ
	result := entity.Tovar{}

	req, err := http.NewRequest("POST", "http://localhost:4080/es/"+c.indexName+"/_search", strings.NewReader(query))
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(c.adminName, c.adminPassword)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
		return nil, err
	}
	defer resp.Body.Close()

	var r Response
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		log.Fatalf("Error decode while ElasticSearch: %s", err)
		return nil, err
	}

	if len(r.Hits.Hits) > 0 && r.Hits.Max_score > minScore {
		result = r.Hits.Hits[0].Source
	}

	switch err {
	case nil:
		return &result, nil
	default:
		return nil, err
	}
}

func (c *ZinkConnection) CreateTovar(tovar entity.Tovar) error {

	jsonTovar, err := json.Marshal(tovar)
	if err != nil {
		log.Fatalf("Error CreateTovar json.Marshal: %s", err)
		return err
	}

	req, err := http.NewRequest("POST", "http://localhost:4080/es/"+c.indexName+"/_create/"+tovar.Id.Hex(), bytes.NewBuffer(jsonTovar))
	if err != nil {
		log.Fatalf("Error CreateTovar NewRequest: %s", err)
		return err
	}
	req.SetBasicAuth(c.adminName, c.adminPassword)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("Error CreateTovar getting response: %s", err)
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error CreateTovar reading response: %s", err)
		return err
	}
	//fmt.Println(string(body))

	//log.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		return fmt.Errorf(string(body))
	}

	return nil
}

func (c *ZinkConnection) DeleteIndex() error {

	req, err := http.NewRequest(http.MethodDelete, "http://localhost:4080/api/index/"+c.indexName, nil)
	if err != nil {
		log.Fatalf("Error Delete Index NewRequest: %s", err)
		return err
	}
	req.SetBasicAuth(c.adminName, c.adminPassword)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("Error DeleteIndex getting response: %s", err)
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error DeleteIndex reading response: %s", err)
		return err
	}
	//fmt.Println(string(body))

	//log.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		return fmt.Errorf(string(body))
	}

	return nil
}

package tovar

import (
	"time"

	"gitlab.com/mazhigali/usefullPacket"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/mazhigali/tovarspacket/entity"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByArticul(articul string) (*entity.Tovar, error) {
	result, err := s.repo.FindByArticul(articul)

	switch err {
	case nil:
		return result, err
	case entity.ErrNotFound:
		result, err = s.repo.FindByArticulMore(articul)
		switch err {
		case nil:
			return result, err
		case entity.ErrNotFound:
			result, err = s.repo.FindByEan(articul)
			switch err {
			case nil:
				return result, err
			case entity.ErrNotFound:
				result, err = s.repo.FindByEanMore(articul)
				switch err {
				case nil:
					return result, err
				case entity.ErrNotFound:
					result, err = s.repo.FindByModel(articul)
					switch err {
					case nil:
						return result, err
					case entity.ErrNotFound:
						result, err = s.repo.FindByArticul(usefullPacket.NormalisatorRus2Eng(articul))
						switch err {
						case nil:
							return result, err
						case entity.ErrNotFound:
							result, err = s.repo.FindByArticulMore(usefullPacket.NormalisatorRus2Eng(articul))
							switch err {
							case nil:
								return result, err
							case entity.ErrNotFound:
								return s.repo.FindByModel(usefullPacket.NormalisatorRus2Eng(articul))
							default:
								return nil, err
							}
						default:
							return nil, err
						}
					default:
						return nil, err
					}
				default:
					return nil, err
				}
			default:
				return nil, err
			}
		default:
			return nil, err
		}
	default:
		return nil, err
	}
}

func (s *Service) FindByArticulMore(articul string) (*entity.Tovar, error) {
	return s.repo.FindByArticulMore(articul)
}
func (s *Service) FindByEan(ean string) (*entity.Tovar, error) {
	return s.repo.FindByEan(ean)
}
func (s *Service) FindByUrl(url string) (*entity.Tovar, error) {
	return s.repo.FindByUrl(url)
}
func (s *Service) FindByEanMore(ean string) (*entity.Tovar, error) {
	return s.repo.FindByEanMore(ean)
}
func (s *Service) FindByName(name string) (*entity.Tovar, error) {
	return s.repo.FindByName(name)
}

func (s *Service) FindByModel(name string) (*entity.Tovar, error) {
	return s.repo.FindByModel(name)
}

func (s *Service) FindByNametranslit(name string) (*entity.Tovar, error) {
	return s.repo.FindByNametranslit(name)
}

func (s *Service) FindByVnutrID(idVnutr string) (*entity.Tovar, error) {
	return s.repo.FindByVnutrID(idVnutr)
}
func (s *Service) FindByExtID(id string) (*entity.Tovar, error) {
	return s.repo.FindByExtID(id)
}
func (s *Service) FindByIdBitrix(idBitrix string) (*entity.Tovar, error) {
	return s.repo.FindByIdBitrix(idBitrix)
}
func (s *Service) FindByID(id string) (*entity.Tovar, error) {
	return s.repo.FindByID(id)
}

func (s *Service) FindBySootvetstviePostavshik(namePostavshcik, nameForSearch string) (*entity.Tovar, error) {
	return s.repo.FindBySootvetstviePostavshik(namePostavshcik, nameForSearch)
}

func (s *Service) FindTovarByQuery(query bson.M) (*entity.Tovar, error) {
	return s.repo.FindTovarByQuery(query)
}

func (s *Service) FindByModelAndBrand(model, brand string) (*entity.Tovar, error) {
	return s.repo.FindByModelAndBrand(model, brand)
}

func (s *Service) FindByNameAndBrand(name, brand string) (*entity.Tovar, error) {
	return s.repo.FindByNameAndBrand(name, brand)
}

func (s *Service) FindByEanAndBrand(model, brand string) (*entity.Tovar, error) {
	return s.repo.FindByEanAndBrand(model, brand)
}
func (s *Service) FindPartInNameAndBrand(partInName, brand string) (*entity.Tovar, error) {
	return s.repo.FindPartInNameAndBrand(partInName, brand)
}

func (s *Service) FindByPartInName(partInName string) (*entity.Tovar, error) {
	return s.repo.FindByPartInName(partInName)
}

func (s *Service) FindByEanMoreAndBrand(model, brand string) (*entity.Tovar, error) {
	return s.repo.FindByEanMoreAndBrand(model, brand)
}

func (s *Service) FindByArtAndName(articul, name string) (*entity.Tovar, error) {
	return s.repo.FindByArtAndName(articul, name)
}

func (s *Service) FindByArticulAndBrand(articul, brand string) (*entity.Tovar, error) {
	result, err := s.repo.FindByArticulAndBrand(articul, brand)
	if articul == "" || brand == "" {
		return nil, entity.ErrInputValueNil
	}

	switch err {
	case nil:
		return result, err
	case entity.ErrNotFound:
		result, err = s.repo.FindByArticulMoreAndBrand(articul, brand)
		switch err {
		case nil:
			return result, err
		case entity.ErrNotFound:
			result, err = s.repo.FindByModelAndBrand(articul, brand)
			switch err {
			case nil:
				return result, err
			case entity.ErrNotFound:
				result, err = s.repo.FindByEanAndBrand(articul, brand)
				switch err {
				case nil:
					return result, err
				case entity.ErrNotFound:
					result, err = s.repo.FindByEanMoreAndBrand(articul, brand)
					switch err {
					case nil:
						return result, err
					case entity.ErrNotFound:
						result, err = s.repo.FindByArticulAndBrand(usefullPacket.NormalisatorRus2Eng(articul), brand)
						switch err {
						case nil:
							return result, err
						case entity.ErrNotFound:
							result, err = s.repo.FindByArticulMoreAndBrand(usefullPacket.NormalisatorRus2Eng(articul), brand)
							switch err {
							case nil:
								return result, err
							case entity.ErrNotFound:
								return s.repo.FindByModelAndBrand(usefullPacket.NormalisatorRus2Eng(articul), brand)
							default:
								return nil, err
							}
						default:
							return nil, err
						}
					default:
						return nil, err
					}
				default:
					return nil, err
				}
			default:
				return nil, err
			}
		default:
			return nil, err
		}
	default:
		return nil, err
	}
}

func (s *Service) FindByArticulMoreAndBrand(articul, brand string) (*entity.Tovar, error) {
	return s.repo.FindByArticulMoreAndBrand(articul, brand)
}

func (s *Service) FindAll() ([]entity.Tovar, error) {
	return s.repo.FindAll()
}

func (s *Service) FindAllByBrand(brand string) ([]entity.Tovar, error) {
	return s.repo.FindAllByBrand(brand)
}

func (s *Service) FindAllByCollection(collectionVnutrId string) ([]entity.Tovar, error) {
	return s.repo.FindAllByCollection(collectionVnutrId)
}

func (s *Service) FindAllByVnutrIds(vnutrIds []string) ([]entity.Tovar, error) {
	return s.repo.FindAllByVnutrIds(vnutrIds)
}

func (s *Service) FindAllByBrandAndCategory(brand, category string) ([]entity.Tovar, error) {
	return s.repo.FindAllByBrandAndCategory(brand, category)
}

func (s *Service) FindAllByBrandAndCategoryUrl(brand, categoryUrl string) ([]entity.Tovar, error) {
	return s.repo.FindAllByBrandAndCategoryUrl(brand, categoryUrl)
}

func (s *Service) FindAllWithArticul() ([]entity.Tovar, error) {
	return s.repo.FindAllWithArticul()
}

func (s *Service) FindAllWithoutArticul() ([]entity.Tovar, error) {
	return s.repo.FindAllWithoutArticul()
}

func (s *Service) FindAllWithSkladAndImageAndArticul() ([]entity.Tovar, error) {
	return s.repo.FindAllWithSkladAndImageAndArticul()
}

// получает товары чьи остатки по заданому складу больше или равны kolOt
func (s *Service) FindAllWithSkladGTE(skladName string, kolOt int) ([]entity.Tovar, error) {
	return s.repo.FindAllWithSkladGTE(skladName, kolOt)
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.Tovar, error) {
	return s.repo.FindByQuery(query)
}

func (s *Service) FindByQueryByPage(query bson.M, page, perPage int64) (resultArray []entity.Tovar, total int64, lastPage float64, err error) {
	return s.repo.FindByQueryByPage(query, page, perPage)
}

func (s *Service) AddTovar(b *entity.Tovar) error {
	if b.Id == primitive.NilObjectID {
		b.Id = primitive.NewObjectID()
	}
	return s.repo.AddTovar(b)
}

func (s *Service) UpdateTovarByIdMongo(tovar *entity.Tovar) error {
	if tovar.Id == primitive.NilObjectID {
		tovar.Id = primitive.NewObjectID()
		now := time.Now()
		tovar.CreatedTime = &now
	}
	return s.repo.UpdateTovarByIdMongo(tovar)
}

func (s *Service) UpdateTovarByVnutrId(tovar *entity.Tovar) (err error) {
	if tovar.IdVnutr == "" {
		return entity.ErrInputValueNil
	}

	if tovar.Id == primitive.NilObjectID {
		findTovar, err := s.repo.FindByVnutrID(tovar.IdVnutr)
		if err != nil {
			switch err {
			case entity.ErrNotFound:
				tovar.Id = primitive.NewObjectID()
				now := time.Now()
				tovar.CreatedTime = &now
				return s.repo.UpdateTovarByVnutrId(tovar)
			default:
				return err
			}
		}
		tovar.Id = findTovar.Id
	}

	return s.repo.UpdateTovarByVnutrId(tovar)
}

func (s *Service) UpdateTovarByExtId(tovarForUpdate *entity.Tovar) (err error) {
	if tovarForUpdate.ExternalId == "" {
		return entity.ErrInputValueNil
	}

	if tovarForUpdate.Id == primitive.NilObjectID {
		findTovar, err := s.repo.FindByExtID(tovarForUpdate.ExternalId)
		if err != nil {
			switch err {
			case entity.ErrNotFound:
				tovarForUpdate.Id = primitive.NewObjectID()
				now := time.Now()
				tovarForUpdate.CreatedTime = &now
				return s.repo.UpdateTovarByExtId(tovarForUpdate)
			default:
				return err
			}
		}
		tovarForUpdate.Id = findTovar.Id
	}

	return s.repo.UpdateTovarByExtId(tovarForUpdate)
}

func (s *Service) UpdateTovarByArtAndName(tovarForUpdate *entity.Tovar) error {
	if tovarForUpdate.Articul == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Name == "" {
		return entity.ErrInputValueNil
	}

	if tovarForUpdate.Id == primitive.NilObjectID {
		findTovar, err := s.repo.FindByArtAndName(tovarForUpdate.Articul, tovarForUpdate.Name)
		if err != nil {
			switch err {
			case entity.ErrNotFound:
				tovarForUpdate.Id = primitive.NewObjectID()
				now := time.Now()
				tovarForUpdate.CreatedTime = &now
				return s.repo.UpdateTovarByArtAndName(tovarForUpdate)
			default:
				return err
			}
		}
		tovarForUpdate.Id = findTovar.Id
	}

	return s.repo.UpdateTovarByArtAndName(tovarForUpdate)
}

func (s *Service) UpdateTovarByNameBrand(tovarForUpdate *entity.Tovar) error {
	if tovarForUpdate.Name == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Proizvoditel == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Id == primitive.NilObjectID {
		findTovar, err := s.repo.FindByNameAndBrand(tovarForUpdate.Name, tovarForUpdate.Proizvoditel)
		if err != nil {
			switch err {
			case entity.ErrNotFound:
				tovarForUpdate.Id = primitive.NewObjectID()
				now := time.Now()
				tovarForUpdate.CreatedTime = &now
				return s.repo.UpdateTovarByNameBrand(tovarForUpdate)
			default:
				return err
			}
		}
		tovarForUpdate.Id = findTovar.Id
	}

	return s.repo.UpdateTovarByNameBrand(tovarForUpdate)
}

func (s *Service) UpdateTovarByArtBrand(tovarForUpdate *entity.Tovar) error {
	if tovarForUpdate.Articul == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Proizvoditel == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Id == primitive.NilObjectID {
		findTovar, err := s.repo.FindByArticulAndBrand(tovarForUpdate.Articul, tovarForUpdate.Proizvoditel)
		if err != nil {
			switch err {
			case entity.ErrNotFound:
				tovarForUpdate.Id = primitive.NewObjectID()
				now := time.Now()
				tovarForUpdate.CreatedTime = &now
				return s.repo.UpdateTovarByArtBrand(tovarForUpdate)
			default:
				return err
			}
		}
		tovarForUpdate.Id = findTovar.Id
	}

	return s.repo.UpdateTovarByArtBrand(tovarForUpdate)
}

func (s *Service) UpdateTovarByModelBrand(tovarForUpdate *entity.Tovar) error {
	if tovarForUpdate.Model == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Proizvoditel == "" {
		return entity.ErrInputValueNil
	}
	if tovarForUpdate.Id == primitive.NilObjectID {
		findTovar, err := s.repo.FindByModelAndBrand(tovarForUpdate.Model, tovarForUpdate.Proizvoditel)
		if err != nil {
			switch err {
			case entity.ErrNotFound:
				tovarForUpdate.Id = primitive.NewObjectID()
				now := time.Now()
				tovarForUpdate.CreatedTime = &now
				return s.repo.UpdateTovarByModelBrand(tovarForUpdate)
			default:
				return err
			}
		}
		tovarForUpdate.Id = findTovar.Id
	}
	return s.repo.UpdateTovarByModelBrand(tovarForUpdate)
}

func (s *Service) RemoveTovar(tovarMongoDocument *entity.Tovar) error {
	return s.repo.RemoveTovar(tovarMongoDocument)
}

func (s *Service) RemoveById(mongoId primitive.ObjectID) error {
	return s.repo.RemoveById(mongoId)
}

func (s *Service) RemoveByIdVnutr(vnutrId string) error {
	return s.repo.RemoveByIdVnutr(vnutrId)
}

func (s *Service) RemoveByUrl(url string) error {
	return s.repo.RemoveByUrl(url)
}

func (s *Service) GetUniqBrandsAllCollection() ([]string, error) {
	return s.repo.GetUniqBrandsAllCollection()
}

func (s *Service) GetUniqBrandsWithoutArticul() ([]string, error) {
	return s.repo.GetUniqBrandsWithoutArticul()
}

func (s *Service) GetUniqCategoriesByBrand(brand string) ([]string, error) {
	return s.repo.GetUniqCategoriesByBrand(brand)
}

func (s *Service) GetUniqCategoriesForSiteByBrand(brand string) ([]entity.Category, error) {
	return s.repo.GetUniqCategoriesForSiteByBrand(brand)
}

func (s *Service) GetUniqCategoriesForSiteByQuery(query bson.M) ([]entity.Category, error) {
	return s.repo.GetUniqCategoriesForSiteByQuery(query)
}

func (s *Service) GetSvoystvaByQueryForSiteFilter(query bson.M) ([]entity.Svoystvo, error) {
	return s.repo.GetSvoystvaByQueryForSiteFilter(query)
}

func (s *Service) GetSvoystvaByQuery(query bson.M) ([]entity.Svoystvo, error) {
	return s.repo.GetSvoystvaByQuery(query)
}

func (s *Service) GetUniqValues(nameField string) ([]string, error) {
	return s.repo.GetUniqValues(nameField)
}

func (s *Service) GetUniqValuesFromQuery(query interface{}, nameField string) ([]string, error) {
	return s.repo.GetUniqValuesFromQuery(query, nameField)
}

func (s *Service) CountDocsFromQuery(query interface{}) (int, error) {
	return s.repo.CountDocsFromQuery(query)
}

func (s *Service) SetPriceByUrl(url string, price float64) error {
	return s.repo.SetPriceByUrl(url, price)
}

func (s *Service) SetPriceKonkur(mongoId primitive.ObjectID, dbCollection string, cena float64) error {
	if len(mongoId) == 0 {
		return entity.ErrNoMongoId
	}
	return s.repo.SetPriceKonkur(mongoId, dbCollection, cena)
}

func (s *Service) SetSootvetstviePostavschik(mongoId primitive.ObjectID, namePostavshcik, sootvetstvieValue string) error {
	if len(mongoId) == 0 {
		return entity.ErrNoMongoId
	}
	return s.repo.SetSootvetstviePostavschik(mongoId, namePostavshcik, sootvetstvieValue)
}

func (s *Service) SetSootvetstviePostavschikByVnutrId(vnutrId string, sootvetstvie entity.Sootvetstvie) error {
	if len(vnutrId) == 0 {
		return entity.ErrNoMongoId
	}
	if sootvetstvie.NameTranslitPostav == "" {
		return entity.ErrInputValueNil
	}
	return s.repo.SetSootvetstviePostavschikByVnutrId(vnutrId, sootvetstvie)
}

func (s *Service) RemoveSootvetstviePostavschikByVnutrId(idVnutr, namePostavshcik string) error {
	if len(idVnutr) == 0 {
		return entity.ErrNoMongoId
	}
	return s.repo.RemoveSootvetstviePostavschikByVnutrId(idVnutr, namePostavshcik)
}

func (s *Service) RemoveSootvetstviePostavschik(idVnutr string) error {
	if len(idVnutr) == 0 {
		return entity.ErrNoMongoId
	}
	return s.repo.RemoveSootvetstviePostavschik(idVnutr)
}

func (s *Service) RemoveField(mongoId primitive.ObjectID, nameField string) error {
	if len(mongoId) == 0 {
		return entity.ErrNoMongoId
	}
	return s.repo.RemoveField(mongoId, nameField)
}

func (s *Service) SetSootvetstviePostavschikByNameBrand(name, brand, namePostavshcik, sootvetstvieValue string) error {
	if len(name) == 0 || len(brand) == 0 {
		return entity.ErrNoMongoId
	}
	return s.repo.SetSootvetstviePostavschikByNameBrand(name, brand, namePostavshcik, sootvetstvieValue)
}

func (s *Service) GetArticulsByQuery(query bson.M) ([]string, error) {
	articuls := []string{}

	tovary, err := s.repo.FindByQuery(query)
	if err != nil {
		return articuls, err
	}

	for _, t := range tovary {
		if t.Articul != "" {
			articuls = append(articuls, t.Articul)
		}
	}
	//fmt.Println("ARTICULS:--- ", len(articuls))
	return articuls, err
}

// выбрать товары по запросу и занести все айдишники монги в массив
func (s *Service) GetMongoIDsByQuery(query bson.M) ([]string, error) {
	mongoIds := []string{}

	tovaryWithIds, err := s.repo.FindByQuery(query)
	if err != nil {
		return mongoIds, err
	}

	for _, t := range tovaryWithIds {
		mongoIds = append(mongoIds, t.Id.Hex())
	}
	//fmt.Println("ARTICULS:--- ", len(mongoIds))
	return mongoIds, err
}

func (s *Service) GetHeadersAll() ([]string, error) {
	return s.repo.GetHeadersAll()
}
func (s *Service) GetHeadersSvoystva() ([]string, error) {
	return s.repo.GetHeadersSvoystva()
}
func (s *Service) GetHeadersSvoystvaFromQuery(query interface{}) ([]string, error) {
	return s.repo.GetHeadersSvoystvaFromQuery(query)
}

func (s *Service) SetNullSkladPostavschik(mongoId primitive.ObjectID, skladName string) error {
	return s.repo.SetNullSkladPostavschik(mongoId, skladName)
}

func (s *Service) SetSkladPostavschik(mongoId primitive.ObjectID, skladName string, ostatok float64) error {
	return s.repo.SetSkladPostavschik(mongoId, skladName, ostatok)
}

func (s *Service) SetPriceOptPostavschik(mongoId primitive.ObjectID, nameOptPrice string, price float64) error {
	return s.repo.SetPriceOptPostavschik(mongoId, nameOptPrice, price)
}

func (s *Service) SetPriceRoznPostavschik(mongoId primitive.ObjectID, nameRoznPrice string, price float64) error {
	return s.repo.SetPriceRoznPostavschik(mongoId, nameRoznPrice, price)
}

func (s *Service) SetNullPricesPostavschik(mongoId primitive.ObjectID, nameRoznPrice, nameOptPrice string) error {
	return s.repo.SetNullPricesPostavschik(mongoId, nameRoznPrice, nameOptPrice)
}

func (s *Service) SetSvoystvaForSite(mongoId primitive.ObjectID, svoystvaSiteFilter, svoystvaSiteHaract bson.M) error {
	return s.repo.SetSvoystvaForSite(mongoId, svoystvaSiteFilter, svoystvaSiteHaract)
}

func (s *Service) SetCollections(mongoId primitive.ObjectID, collections []string) error {
	return s.repo.SetCollections(mongoId, collections)
}

func (s *Service) SetMarzha(mongoId primitive.ObjectID, marzha float64) error {
	return s.repo.SetMarzha(mongoId, marzha)
}

func (s *Service) SetMarzhaMaxAndMin(mongoId primitive.ObjectID, maxMarzha, minMarzha float64) error {
	return s.repo.SetMarzhaMaxAndMin(mongoId, maxMarzha, minMarzha)
}

func (s *Service) SetPrice(mongoId primitive.ObjectID, price float64) error {
	return s.repo.SetPrice(mongoId, price)
}

func (s *Service) SetKursPrice(mongoId primitive.ObjectID, kurs float64) error {
	return s.repo.SetKursPrice(mongoId, kurs)
}

func (s *Service) SetValuta(mongoId primitive.ObjectID, valuta string) error {
	return s.repo.SetValuta(mongoId, valuta)
}

func (s *Service) SetYaMarketAvailable(mongoId primitive.ObjectID, status bool) error {
	return s.repo.SetYaMarketAvailable(mongoId, status)
}

func (s *Service) SetAvailableAvito(mongoId primitive.ObjectID, status bool) error {
	return s.repo.SetAvailableAvito(mongoId, status)
}

func (s *Service) SetPriceRecommend(mongoId primitive.ObjectID, price float64) error {
	return s.repo.SetPriceRecommend(mongoId, price)
}

func (s *Service) SetSkladOstatok(mongoId primitive.ObjectID, ostatok float64) error {
	return s.repo.SetSkladOstatok(mongoId, ostatok)
}

func (s *Service) SetYaMarketPrices(bitrixId string, minPrice, maxPrice, avgPrice float32) error {
	return s.repo.SetYaMarketPrices(bitrixId, minPrice, maxPrice, avgPrice)
}

func (s *Service) SetYaMarketPricesNull() error {
	return s.repo.SetYaMarketPricesNull()
}

func (s *Service) AddDopArticul(idMongoString, dopArticul string) error {
	t, err := s.repo.FindByID(idMongoString)
	if err != nil {
		return err
	}
	if usefullPacket.ContainsString(t.ArticulsMore, dopArticul) == true {
		return entity.ErrDublicateInSlice
	} else {
		return s.repo.AddDopArticul(idMongoString, dopArticul)
	}
}

func (s *Service) FindCategoryByUrl(url string) (*entity.Category, error) {
	return s.repo.FindCategoryByUrl(url)
}

func (s *Service) UpdateCategoryByUrl(category *entity.Category) (err error) {
	now := time.Now()
	category.UpdatedTime = &now
	return s.repo.UpdateCategoryByUrl(category)
}

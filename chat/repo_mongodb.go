package chat

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindAccountById(id string) (*entity.AccountChat, error) {
	account := new(entity.AccountChat)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(account)
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (c *MongoConnection) FindAccountByQuery(query bson.M) (*entity.AccountChat, error) {
	account := new(entity.AccountChat)
	err := c.collection.FindOne(context.TODO(), query).Decode(account)
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (c *MongoConnection) FindAccountsByQuery(query bson.M) ([]entity.AccountChat, error) {
	accounts := []entity.AccountChat{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &accounts); err != nil {
		return nil, err
	}
	return accounts, nil
}

func (c *MongoConnection) UpdateAccount(account *entity.AccountChat) error {
	query := bson.M{"_id": account.ID}
	update := bson.M{"$set": account}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveAccount(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) FindChatById(id string) (*entity.Chat, error) {
	chat := new(entity.Chat)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(chat)
	if err != nil {
		return nil, err
	}
	return chat, nil
}

func (c *MongoConnection) FindChatByQuery(query bson.M) (*entity.Chat, error) {
	chat := new(entity.Chat)
	err := c.collection.FindOne(context.TODO(), query).Decode(chat)
	if err != nil {
		return nil, err
	}
	return chat, nil
}

func (c *MongoConnection) FindChatsByQuery(query bson.M) ([]entity.Chat, error) {
	chats := []entity.Chat{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &chats); err != nil {
		return nil, err
	}
	return chats, nil
}

func (c *MongoConnection) UpdateChat(chat *entity.Chat) error {
	query := bson.M{"_id": chat.ID}
	update := bson.M{"$set": chat}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveChat(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) FindMessageById(id string) (*entity.Message, error) {
	message := new(entity.Message)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(message)
	if err != nil {
		return nil, err
	}
	return message, nil
}

func (c *MongoConnection) FindMessageByQuery(query bson.M) (*entity.Message, error) {
	message := new(entity.Message)
	err := c.collection.FindOne(context.TODO(), query).Decode(message)
	if err != nil {
		return nil, err
	}
	return message, nil
}

func (c *MongoConnection) FindMessagesByQuery(query bson.M) ([]entity.Message, error) {
	messages := []entity.Message{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &messages); err != nil {
		return nil, err
	}
	return messages, nil
}

func (c *MongoConnection) UpdateMessage(message *entity.Message) error {
	query := bson.M{"_id": message.ID}
	update := bson.M{"$set": message}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveMessage(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

package chat

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()

	FindAccountById(id string) (*entity.AccountChat, error)
	FindAccountByQuery(query bson.M) (*entity.AccountChat, error)
	FindAccountsByQuery(query bson.M) ([]entity.AccountChat, error)

	FindChatById(id string) (*entity.Chat, error)
	FindChatByQuery(query bson.M) (*entity.Chat, error)
	FindChatsByQuery(query bson.M) ([]entity.Chat, error)

	FindMessageById(id string) (*entity.Message, error)
	FindMessageByQuery(query bson.M) (*entity.Message, error)
	FindMessagesByQuery(query bson.M) ([]entity.Message, error)
}

// Writer interface
type Writer interface {
	UpdateAccount(account *entity.AccountChat) error
	UpdateChat(chat *entity.Chat) error
	UpdateMessage(message *entity.Message) error
	RemoveAccount(id string) error
	RemoveChat(id string) error
	RemoveMessage(id string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

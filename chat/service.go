package chat

import (
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindAccountById(id string) (*entity.AccountChat, error) {
	return s.repo.FindAccountById(id)
}

func (s *Service) FindAccountByQuery(query bson.M) (*entity.AccountChat, error) {
	return s.repo.FindAccountByQuery(query)
}

func (s *Service) FindAccountsByQuery(query bson.M) ([]entity.AccountChat, error) {
	return s.repo.FindAccountsByQuery(query)
}

func (s *Service) FindChatById(id string) (*entity.Chat, error) {
	return s.repo.FindChatById(id)
}

func (s *Service) FindChatByQuery(query bson.M) (*entity.Chat, error) {
	return s.repo.FindChatByQuery(query)
}

func (s *Service) FindChatsByQuery(query bson.M) ([]entity.Chat, error) {
	return s.repo.FindChatsByQuery(query)
}

func (s *Service) FindMessageById(id string) (*entity.Message, error) {
	return s.repo.FindMessageById(id)
}

func (s *Service) FindMessageByQuery(query bson.M) (*entity.Message, error) {
	return s.repo.FindMessageByQuery(query)
}

func (s *Service) FindMessagesByQuery(query bson.M) ([]entity.Message, error) {
	return s.repo.FindMessagesByQuery(query)
}

func (s *Service) UpdateAccount(account *entity.AccountChat) error {
	account.UpdatedAt = time.Now()

	if account.ID == primitive.NilObjectID {
		account.ID = primitive.NewObjectID()
		account.CreatedAt = time.Now()
	}

	return s.repo.UpdateAccount(account)
}

func (s *Service) UpdateChat(chat *entity.Chat) error {
	chat.UpdatedAt = time.Now()
	if chat.ID == primitive.NilObjectID {
		chat.ID = primitive.NewObjectID()
		chat.CreatedAt = time.Now()
	}
	return s.repo.UpdateChat(chat)
}

func (s *Service) UpdateMessage(message *entity.Message) error {
	message.UpdatedAt = time.Now()
	if message.ID == primitive.NilObjectID {
		message.ID = primitive.NewObjectID()
		message.CreatedAt = time.Now()
	}
	return s.repo.UpdateMessage(message)
}

func (s *Service) RemoveAccount(id string) error {
	return s.repo.RemoveAccount(id)
}
func (s *Service) RemoveChat(id string) error {
	return s.repo.RemoveChat(id)
}

func (s *Service) RemoveMessage(id string) error {
	return s.repo.RemoveMessage(id)
}

package gallery

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByName(name string) (*entity.Gallery, error)
	FindByNameTranslit(name string) (*entity.Gallery, error)
	FindByCategoryId(nameTranslit string) (*entity.Gallery, error)

	FindAll() ([]entity.Gallery, error)
}

// Writer bookmark writer
type Writer interface {
	UpdateByName(gallery *entity.Gallery) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

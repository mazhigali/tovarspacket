package gallery

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByName(name string) (*entity.Gallery, error) {
	return s.repo.FindByName(name)
}

func (s *Service) FindByNameTranslit(name string) (*entity.Gallery, error) {
	return s.repo.FindByNameTranslit(name)
}

func (s *Service) FindByCategoryId(nameTranslit string) (*entity.Gallery, error) {
	return s.repo.FindByCategoryId(nameTranslit)
}

func (s *Service) FindAll() ([]entity.Gallery, error) {
	return s.repo.FindAll()
}

func (s *Service) UpdateByName(gallery *entity.Gallery) error {
	if gallery.Name == "" {
		return entity.ErrInputValueNil
	}

	findGallery, err := s.repo.FindByName(gallery.Name)
	if err != nil {
		switch err {
		case entity.ErrNotFound:
			gallery.Id = primitive.NewObjectID()
			return s.repo.UpdateByName(gallery)
		default:
			return err
		}
	}
	gallery.Id = findGallery.Id

	return s.repo.UpdateByName(gallery)
}

package brand

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByName(name string) (*entity.Brand, error) {
	return s.repo.FindByName(name)
}

func (s *Service) FindByNameTranslit(name string) (*entity.Brand, error) {
	return s.repo.FindByNameTranslit(name)
}

func (s *Service) FindAll() ([]entity.Brand, error) {
	return s.repo.FindAll()
}

func (s *Service) Update(brand *entity.Brand) error {
	if brand.Name == "" {
		return entity.ErrInputValueNil
	}

	findBrand, err := s.repo.FindByName(brand.Name)
	if err != nil {
		switch err {
		case entity.ErrNotFound:
			brand.Id = primitive.NewObjectID()
			return s.repo.Update(brand)
		default:
			return err
		}
	}
	brand.Id = findBrand.Id

	return s.repo.Update(brand)
}

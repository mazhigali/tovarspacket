package brand

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByName(name string) (*entity.Brand, error)
	FindByNameTranslit(name string) (*entity.Brand, error)

	FindAll() ([]entity.Brand, error)
}

// Writer bookmark writer
type Writer interface {
	Update(brand *entity.Brand) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

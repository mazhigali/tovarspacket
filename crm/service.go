package crm

import (
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindContactById(id string) (*entity.Contact, error) {
	return s.repo.FindContactById(id)
}

func (s *Service) FindContactByQuery(query bson.M) (*entity.Contact, error) {
	return s.repo.FindContactByQuery(query)
}

func (s *Service) FindContactsByQuery(query bson.M) ([]entity.Contact, error) {
	return s.repo.FindContactsByQuery(query)
}

func (s *Service) FindCustomerById(id string) (*entity.Customer, error) {
	return s.repo.FindCustomerById(id)
}

func (s *Service) FindCustomerByQuery(query bson.M) (*entity.Customer, error) {
	return s.repo.FindCustomerByQuery(query)
}

func (s *Service) FindCustomersByQuery(query bson.M) ([]entity.Customer, error) {
	return s.repo.FindCustomersByQuery(query)
}

func (s *Service) FindTaskById(id string) (*entity.Task, error) {
	return s.repo.FindTaskById(id)
}

func (s *Service) FindTaskByQuery(query bson.M) (*entity.Task, error) {
	return s.repo.FindTaskByQuery(query)
}

func (s *Service) FindTasksByQuery(query bson.M) ([]entity.Task, error) {
	return s.repo.FindTasksByQuery(query)
}

func (s *Service) FindCallsByQuery(query bson.M) ([]entity.Call, error) {
	return s.repo.FindCallsByQuery(query)
}

func (s *Service) FindCallByQuery(query bson.M) (*entity.Call, error) {
	return s.repo.FindCallByQuery(query)
}

func (s *Service) FindCallById(id string) (*entity.Call, error) {
	return s.repo.FindCallById(id)
}

func (s *Service) UpdateContact(contact *entity.Contact) error {
	contact.UpdatedAt = time.Now()

	if contact.ID == primitive.NilObjectID {
		contact.ID = primitive.NewObjectID()
		contact.CreatedAt = time.Now()
	}

	return s.repo.UpdateContact(contact)
}

func (s *Service) UpdateCustomer(customer *entity.Customer) error {
	customer.UpdatedAt = time.Now()
	if customer.ID == primitive.NilObjectID {
		customer.ID = primitive.NewObjectID()
		customer.CreatedAt = time.Now()
	}
	return s.repo.UpdateCustomer(customer)
}

func (s *Service) UpdateTask(task *entity.Task) error {
	task.UpdatedAt = time.Now()
	if task.ID == primitive.NilObjectID {
		task.ID = primitive.NewObjectID()
		task.CreatedAt = time.Now()
	}
	return s.repo.UpdateTask(task)
}

func (s *Service) UpdateCall(call *entity.Call) error {
	call.UpdatedAt = time.Now()
	if call.ID == primitive.NilObjectID {
		call.ID = primitive.NewObjectID()
		call.CreatedAt = time.Now()
	}
	return s.repo.UpdateCall(call)
}

func (s *Service) UpdateCallByAtsId(call *entity.Call) error {
	call.UpdatedAt = time.Now()
	if call.ID == primitive.NilObjectID {
		call.ID = primitive.NewObjectID()
		call.CreatedAt = time.Now()
	}
	return s.repo.UpdateCallByAtsId(call)
}

func (s *Service) RemoveContact(id string) error {
	return s.repo.RemoveContact(id)
}
func (s *Service) RemoveCustomer(id string) error {
	return s.repo.RemoveCustomer(id)
}

func (s *Service) RemoveTask(id string) error {
	return s.repo.RemoveTask(id)
}

func (s *Service) RemoveCall(id string) error {
	return s.repo.RemoveCall(id)
}

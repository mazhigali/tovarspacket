package crm

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()

	FindContactById(id string) (*entity.Contact, error)
	FindContactByQuery(query bson.M) (*entity.Contact, error)
	FindContactsByQuery(query bson.M) ([]entity.Contact, error)

	FindCustomerById(id string) (*entity.Customer, error)
	FindCustomerByQuery(query bson.M) (*entity.Customer, error)
	FindCustomersByQuery(query bson.M) ([]entity.Customer, error)

	FindTaskById(id string) (*entity.Task, error)
	FindTaskByQuery(query bson.M) (*entity.Task, error)
	FindTasksByQuery(query bson.M) ([]entity.Task, error)

	FindCallsByQuery(query bson.M) ([]entity.Call, error)
	FindCallByQuery(query bson.M) (*entity.Call, error)
	FindCallById(id string) (*entity.Call, error)
}

// Writer interface
type Writer interface {
	UpdateContact(contact *entity.Contact) error
	UpdateCustomer(customer *entity.Customer) error
	UpdateTask(task *entity.Task) error
	UpdateCall(call *entity.Call) error
	UpdateCallByAtsId(call *entity.Call) error
	RemoveContact(id string) error
	RemoveCustomer(id string) error
	RemoveTask(id string) error
	RemoveCall(id string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

package crm

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}
type ConfMongo struct {
	ConnectionString string
	DbName           string
	CollectionName   string
	Indexes          []Index
}

type Index struct {
	Fields []string
	Unique bool
}

func NewDBConnection(conf *ConfMongo) (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

// функцию, которая будет создавать соединение с базой
func (c *MongoConnection) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: %s\n", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: %s\n", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.Fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.Unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *MongoConnection) CloseConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: %s\n", err.Error())
	}
	log.Println("Connection to MongoDB closed.--", c.collection.Name())
}

func (c *MongoConnection) FindContactById(id string) (*entity.Contact, error) {
	contact := new(entity.Contact)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(contact)
	if err != nil {
		return nil, err
	}
	return contact, nil
}

func (c *MongoConnection) FindContactByQuery(query bson.M) (*entity.Contact, error) {
	contact := new(entity.Contact)
	err := c.collection.FindOne(context.TODO(), query).Decode(contact)
	if err != nil {
		return nil, err
	}
	return contact, nil
}

func (c *MongoConnection) FindContactsByQuery(query bson.M) ([]entity.Contact, error) {
	contacts := []entity.Contact{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &contacts); err != nil {
		return nil, err
	}
	return contacts, nil
}

func (c *MongoConnection) FindCustomerById(id string) (*entity.Customer, error) {
	customer := new(entity.Customer)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(customer)
	if err != nil {
		return nil, err
	}
	return customer, nil
}

func (c *MongoConnection) FindCustomerByQuery(query bson.M) (*entity.Customer, error) {
	customer := new(entity.Customer)
	err := c.collection.FindOne(context.TODO(), query).Decode(customer)
	if err != nil {
		return nil, err
	}
	return customer, nil
}

func (c *MongoConnection) FindCustomersByQuery(query bson.M) ([]entity.Customer, error) {
	customers := []entity.Customer{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &customers); err != nil {
		return nil, err
	}
	return customers, nil
}

func (c *MongoConnection) UpdateContact(contact *entity.Contact) error {
	query := bson.M{"_id": contact.ID}
	update := bson.M{"$set": contact}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) UpdateCustomer(customer *entity.Customer) error {
	query := bson.M{"_id": customer.ID}
	update := bson.M{"$set": customer}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveContact(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveCustomer(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) FindTaskById(id string) (*entity.Task, error) {
	task := new(entity.Task)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(task)
	if err != nil {
		return nil, err
	}
	return task, nil
}

func (c *MongoConnection) FindTaskByQuery(query bson.M) (*entity.Task, error) {
	task := new(entity.Task)
	err := c.collection.FindOne(context.TODO(), query).Decode(task)
	if err != nil {
		return nil, err
	}
	return task, nil
}

func (c *MongoConnection) FindTasksByQuery(query bson.M) ([]entity.Task, error) {
	tasks := []entity.Task{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &tasks); err != nil {
		return nil, err
	}
	return tasks, nil
}

func (c *MongoConnection) UpdateTask(task *entity.Task) error {
	query := bson.M{"_id": task.ID}
	update := bson.M{"$set": task}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveTask(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) FindCallByQuery(query bson.M) (*entity.Call, error) {
	call := new(entity.Call)
	err := c.collection.FindOne(context.TODO(), query).Decode(call)
	if err != nil {
		return nil, err
	}
	return call, nil
}

func (c *MongoConnection) FindCallById(id string) (*entity.Call, error) {
	call := new(entity.Call)
	id = strings.TrimSpace(id)
	if id == "" {
		return nil, entity.ErrInputValueNil
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	err = c.collection.FindOne(context.TODO(), bson.M{"_id": objectID}).Decode(call)
	if err != nil {
		return nil, err
	}
	return call, nil
}

func (c *MongoConnection) FindCallsByQuery(query bson.M) ([]entity.Call, error) {
	calls := []entity.Call{}
	cursor, err := c.collection.Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	if err = cursor.All(context.TODO(), &calls); err != nil {
		return nil, err
	}
	return calls, nil
}

func (c *MongoConnection) UpdateCall(call *entity.Call) error {
	query := bson.M{"_id": call.ID}
	update := bson.M{"$set": call}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) UpdateCallByAtsId(call *entity.Call) error {
	query := bson.M{"atsId": call.AtsId}
	update := bson.M{"$set": call}

	_, err := c.collection.UpdateOne(context.TODO(), query, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c *MongoConnection) RemoveCall(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = c.collection.DeleteOne(context.TODO(), bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

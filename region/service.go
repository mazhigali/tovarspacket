package region

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByName(name string) (*entity.Region, error) {
	return s.repo.FindByName(name)
}

func (s *Service) FindByNameTranslit(name string) (*entity.Region, error) {
	return s.repo.FindByNameTranslit(name)
}

func (s *Service) FindById(id int) (*entity.Region, error) {
	return s.repo.FindById(id)
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.Region, error) {
	return s.repo.FindByQuery(query)
}

func (s *Service) FindAll() ([]entity.Region, error) {
	return s.repo.FindAll()
}

func (s *Service) Update(region *entity.Region) error {

	return s.repo.Update(region)
}

func (s *Service) RemoveById(id int) error {
	return s.repo.RemoveById(id)
}

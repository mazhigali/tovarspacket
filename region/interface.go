package region

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByName(name string) (*entity.Region, error)
	FindById(id int) (*entity.Region, error)
	FindByNameTranslit(name string) (*entity.Region, error)
	FindByQuery(query bson.M) ([]entity.Region, error)

	FindAll() ([]entity.Region, error)
}

// Writer
type Writer interface {
	Update(region *entity.Region) error
	RemoveById(id int) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

package partner

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
)

// Reader interface
type Reader interface {
	CloseConnection()
	FindByName(name string) (*entity.Partner, error)
	FindByIdvnutr(idvnutr string) (*entity.Partner, error)
	FindByNameTranslit(name string) (*entity.Partner, error)
	FindByQuery(query bson.M) ([]entity.Partner, error)

	FindAll() ([]entity.Partner, error)
}

// Writer
type Writer interface {
	Update(partner *entity.Partner) error
	RemoveById(id string) error
}

// Repository repository interface
type Repository interface {
	Reader
	Writer
}

// UseCase use case interface
type UseCase interface {
	Reader
	Writer
}

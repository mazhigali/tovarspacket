package partner

import (
	"gitlab.com/mazhigali/tovarspacket/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service service interface
type Service struct {
	repo Repository
}

// NewService create new service
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}
func (s *Service) CloseConnection() {
	s.repo.CloseConnection()
}

func (s *Service) FindByName(name string) (*entity.Partner, error) {
	return s.repo.FindByName(name)
}

func (s *Service) FindByIdvnutr(idvnutr string) (*entity.Partner, error) {
	return s.repo.FindByIdvnutr(idvnutr)
}

func (s *Service) FindByNameTranslit(name string) (*entity.Partner, error) {
	return s.repo.FindByNameTranslit(name)
}

// Поиск по произвольному запросу
func (s *Service) FindByQuery(query bson.M) ([]entity.Partner, error) {
	return s.repo.FindByQuery(query)
}

func (s *Service) FindAll() ([]entity.Partner, error) {
	return s.repo.FindAll()
}

func (s *Service) Update(partner *entity.Partner) error {
	if partner.Id == primitive.NilObjectID {
		partner.Id = primitive.NewObjectID()
		//partner.IDString = partner.Id.Hex()
	}

	return s.repo.Update(partner)
}

func (s *Service) RemoveById(id string) error {
	return s.repo.RemoveById(id)
}
